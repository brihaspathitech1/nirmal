﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="Ordersprofit.aspx.aspx.cs" Inherits="Nirmal.Ordersprofit_aspx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
         .cust{   text-align: right; }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
   

     <div class="row">
         <div class="box-header with-border">
          <h3 class="primery">&nbsp;&nbsp<b>Orders List</b></h3>
             </div>
        <div class="col-md-12">
            
           
            
            <br />
            <!-- /.box-header -->
           
            <div class="box-body">
                  <div class="box box-success">
               
               <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
                      
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="grdProduct_Details" runat="server" AutoGenerateColumns="false" DataKeyNames="id" class="table table-bordered table-striped" >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
                              
                 <asp:BoundField DataField="id" HeaderText="OrderId" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
              
               <asp:BoundField DataField="dname" HeaderText="Distributor Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               
               <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="shpname" HeaderText="Shopname" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
             
             <asp:TemplateField HeaderText="Action" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Button ID="btnitems" runat="server" CssClass="btn btn-success"  Text="Items" OnClick="btnitems_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        
           
               
                       
           </Columns>
       </asp:GridView>
           </div>
                      <asp:Button ID="ff" runat="server" style="display:none" />
                <cc1:ModalPopupExtender ID="modal1" runat="server" TargetControlID="ff" CancelControlID="Cancel" PopupControlID="panel1"  BackgroundCssClass="tableBackground">

                </cc1:ModalPopupExtender>
                      <asp:Panel ID="panel1" runat="server" BackColor="#d9d9d9" Style="display: none; border: 1px solid #ceceec; padding-bottom: 20px; padding:50px;margin-left:150px">


                          <asp:GridView ID="grid1" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped" >
                              <Columns>
                                 <asp:BoundField DataField="id" HeaderText="OrderId" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="dt" HeaderText="Order Date" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="name" HeaderText="Distributor Name" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="address" HeaderText="Distributor Address" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />

               <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
            
            <asp:BoundField DataField="mobile" HeaderText="Distributor Mobile" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="loc" HeaderText="Order Location" HeaderStyle-BackColor="#333300" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                 
               
                              </Columns>
                          </asp:GridView>
                           <div class="col-md-2" style="padding-top:10px;margin-right:10px">
                        <asp:Button ID="Cancel" runat="server" Text="Cancel"  class="btn btn-danger btn-block" />
                       </div>
                      </asp:Panel>

                </div></div></div>
</asp:Content>