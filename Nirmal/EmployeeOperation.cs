﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Nirmal.DataAccessLayer;
namespace Nirmal
{
    public class EmployeeOperation
    {
        public EmployeeItem getEmployee(int empID)
        {
            EmployeeItem employee = new EmployeeItem();

            DataSet ds = DataQueries.SelectCommon("select id,categoryname,image from Category  where id='"+empID+"'");

            if (ds.Tables[0].Rows.Count > 0)
            {
                
                employee.id = ds.Tables[0].Rows[0]["id"].ToString().Trim();
                employee.name = ds.Tables[0].Rows[0]["categoryname"].ToString().Trim();
                employee.image = ds.Tables[0].Rows[0]["image"].ToString().Trim();
                
            }

            return employee;
        }
        public List<EmployeeItem> getEmployeeList()
        {
            List<EmployeeItem> employeeList = new List<EmployeeItem>();

            DataSet ds = DataQueries.SelectCommon("select id from Category");
            DataTable dt = ds.Tables[0];

            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int empID = Convert.ToInt32(dt.Rows[i]["id"].ToString().Trim());
                    employeeList.Add(getEmployee(empID));
                }
            }

            return employeeList;
        }


       


    }
}