﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reg.aspx.cs" Inherits="Nirmal.Reg" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .container{
            /*background:rgba(0,0,0,0.1);*/
            box-shadow:2px 5px 10px  #808080;
        }
    </style>
    <link href="assets/bootstrap.min.css" rel="stylesheet" />

    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        
         <div class="text-center" style="padding-top: 5px">
        <a href="Default.aspx" id="btnback" type="button" onclick="goBack()"><strong style="background: #5cb85c; padding: 9px 8px; border-radius: 4px; color: #fff;">HOME</strong></a>
             </div>
        <br />
        <div class="container">
            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-4 text-center col-md-offset-4 img-responsive">
                        <asp:Image ID="imgLogo" runat="server" ImageUrl="http://gsrnirmal.com/images/nlogo.png" Width="175" />
                    </div>
                    <div class="col-md-12 ">
                        <p class="text-center"><strong>www.gsrnirmal.com</strong></p>
                        <h1 class="text-center text-danger""><b>GSR NIRMAL ONLINE MARKETING</b></h1>
                        <p class="text-center"><b>81-169-A-2, Ground floor, Vivek Nagar Kallur Mandal, Kurnool Dist-518003, Andhra Pradesh</b></p>
                        <p class="text-center"><b>Customer Care-9848505284</b></p>
                    </div>
                    <hr />
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label " for="email">Full Name :</label>
                            <asp:TextBox ID="txtfname" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label " for="email">User Name :</label>
                            <asp:TextBox ID="txtuname" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                               <label class="control-label " for="email">Password :</label>
                            <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control"></asp:TextBox>
                         
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                               <label class="control-label " for="email">Mobile  :</label>
                            <asp:TextBox ID="txtmobile" runat="server" CssClass="form-control"></asp:TextBox>
                         
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label " for="email">Email :</label>
                            <asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label " for="email">Select Role :</label>
                            <asp:RadioButtonList ID="radlist" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem>Dealer</asp:ListItem>
                                <asp:ListItem>Merchant</asp:ListItem>
                                <asp:ListItem>Distributor </asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </div>
                <br />
                <div class="row">
                   
                    <div class="col-md-2 col-md-offset-2">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Location:</label>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:TextBox ID="txtloc" runat="server" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>
                </div>

                <div class="row">
                   
                    <div class="col-md-2 col-md-offset-2">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Address :</label>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>
                </div>

                <div class="row">
                    
                    <div class="col-md-2 col-md-offset-2">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Village :</label>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:TextBox ID="txtvillage" runat="server" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>
                </div>

                <div class="row">
                   
                     <div class="col-md-2 col-md-offset-2">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Addhar :</label>

                        </div>
                    </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <asp:TextBox ID="txtaadharno" runat="server" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">                            

                        </div>
                    </div>
                     <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Account No :</label>

                        </div>
                    </div>
                      <div class="col-md-6">
                        <div class="form-group">
                            <asp:TextBox ID="txtaccountno" runat="server" CssClass="form-control"></asp:TextBox>

                        </div>
                    </div>
                </div>
                <br />
                 <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Holde Name :</label>
                            <asp:TextBox ID="txtholdername" runat="server" CssClass="form-control"></asp:TextBox>
                           
                        </div>
                    </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd"> Bank Name :</label>
                            <asp:TextBox ID="txtbankname" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd">Branch :</label>
                            <asp:TextBox ID="txtbranch" runat="server" CssClass="form-control"></asp:TextBox>
                           
                        </div>
                    </div>
                  <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label cl-md-3" for="pwd"> IFSC :</label>
                            <asp:TextBox ID="txtifsc" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-4 ">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-success btn-block" OnClick="btnSubmit_Click" />
                        <br />
                    </div>
                </div>
            </div>
        </div>
        <br />
    </form>
</body>
</html>
