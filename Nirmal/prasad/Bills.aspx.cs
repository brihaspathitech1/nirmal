﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class Bills : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                bind();
            }
        }




        protected void bind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
           
            SqlCommand cmd = new SqlCommand("select b.*,(o.updatedamount) as totamount,CAST(o.updatedamount as decimal)-CAST(b.amount as decimal) as reamount,o.id from bills b inner join tblorder2 o on b.oid=o.id", conn44);
            
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdCategory_Details.DataSource = ds;
                grdCategory_Details.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdCategory_Details.DataSource = ds;
                grdCategory_Details.DataBind();
                int columncount = grdCategory_Details.Rows[0].Cells.Count;
                grdCategory_Details.Rows[0].Cells.Clear();
                grdCategory_Details.Rows[0].Cells.Add(new TableCell());
                grdCategory_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdCategory_Details.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
       
    }

}