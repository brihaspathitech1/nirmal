﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Nirmal.DataAccessLayer;

using System.IO;

namespace Nirmal.prasad
{
    public partial class Dealerproducts : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindcat();
            }
            BindGrid();
            itemscount();
       
        }
        protected void itemscount()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select count(*)as pcount from productslist",conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblitemscount.Text = ds.Tables[0].Rows[0]["pcount"].ToString();
            }

        }
        protected void bindcat()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;

            SqlConnection sqlConn = new SqlConnection(connstrg);

            SqlCommand sqlCmd = new SqlCommand();
                   
                        sqlCmd.CommandText = "select categoryname from Category";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        dropcat.DataSource = dt;

                        dropcat.DataTextField = "categoryname";
                         //dropcat.DataValueField = "id";
                        dropcat.DataBind();
                        sqlConn.Close();
                        dropcat.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Category", "0"));
                 

        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            connstr.Open();
            string serverpath = string.Empty;
            if (FileUpload2.HasFile)

            {
                string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                FileUpload2.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
                SqlCommand cmd = new SqlCommand("Insert into productslist(catname,pname,image) values('" + dropcat.SelectedValue + "','" + txtpname.Text + "','" + serverpath + "')", connstr);

                cmd.ExecuteNonQuery();

            }
           

            connstr.Close();

            dropcat.SelectedIndex=-1;
            txtpname.Text = null;
           
            BindGrid();

        }
        protected void BindGrid()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from productslist order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                Gridproducts.DataSource = ds;
                Gridproducts.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                Gridproducts.DataSource = ds;
                Gridproducts.DataBind();
                int columncount = Gridproducts.Rows[0].Cells.Count;
                Gridproducts.Rows[0].Cells.Clear();
                Gridproducts.Rows[0].Cells.Add(new TableCell());
                Gridproducts.Rows[0].Cells[0].ColumnSpan = columncount;
                Gridproducts.Rows[0].Cells[0].Text = "No Records Found";
            }

        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Delete from productslist where id='" + id + "'", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
            connstr.Close();
            BindGrid();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Button btnsubmit = sender as Button;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lbl.Text = Gridproducts.DataKeys[gRow.RowIndex].Value.ToString();
           
            txtmpname.Text = gRow.Cells[1].Text.Replace("&nbsp;", "");
            txtmcatname.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");
            this.ModalPopupExtender2.Show();


        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            string serverpath = string.Empty;
            if (Imgprev.HasFile)
            {
                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                Imgprev.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
                String Update = "update  [productslist] set pname='" + txtmpname.Text + "',catname='" + txtmcatname.Text + "',image='" + serverpath + "'  where Id='" + lbl.Text + "'";
                SqlCommand comm = new SqlCommand(Update, conn);
                conn.Open();
                comm.ExecuteNonQuery();
                //comm1.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("Dealerproducts.aspx");
            }
            else
            {
                String Update = "update  [productslist] set pname='" + txtmpname.Text + "',catname='" + txtmcatname.Text + "' where Id='" + lbl.Text + "'";
                SqlCommand comm = new SqlCommand(Update, conn);
                conn.Open();
                comm.ExecuteNonQuery();
                //comm1.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("Dealerproducts.aspx");
            }
        }
    }
}