﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Nirmal.DataAccessLayer;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
namespace Nirmal.prasad
{
    public partial class MerchantOrders : System.Web.UI.Page
    {
        SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind_distributors();

            }
            lbldistname.Visible = false;
            lblprofittext.Visible = false;
            lblprofit.Visible = false;
            todayprofit.Visible = false;
            totalprofit.Visible = false;
            lblordercount.Visible = false;
            lbltxtorderscount.Visible = false;
            lblmerchantshopname.Visible = false;
            lblmshopname.Visible = false;
            lbltxtmname.Visible = false;
            lbltxtvillage.Visible = false;
            lblvillage.Visible = false;
        }
        protected void bind_distributors()
        {
            DataSet ds = DataQueries.SelectCommon("select id, name from [DistributorLogin] where role= 'merchant'");
            ddlmerchant.DataTextField = "name";
            ddlmerchant.DataValueField = "id";
            ddlmerchant.DataSource = ds;
            ddlmerchant.Items.Insert(0, "All");
            ddlmerchant.DataBind();
        }
        //protected void calsum()
        //{
        //    double NetAmt = 0;

        //    //lblTotal.Visible = true;

        //    foreach (GridViewRow g1 in grdcustdetails.Rows)
        //    {

        //        NetAmt += Convert.ToDouble(g1.Cells[5].Text);

        //    }
        //    this.lblprofit.Text = " " + ":Profits Value: " + "  " + NetAmt.ToString();



        //}
        protected void bindprofit()
        {
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select sum(cast(updatedamount as float)) as profit from tblorder2 where mid='" + ddlmerchant.SelectedValue + "'", conn44);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblprofit.Text = ds.Tables[0].Rows[0]["profit"].ToString();
            }


        }
        protected void bndorderscount()
        {
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select count(mid) as orderscount from tblorder2 where mid='" + ddlmerchant.SelectedValue + "'", conn44);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblordercount.Text = ds.Tables[0].Rows[0]["orderscount"].ToString();
            }


        }
        protected void todayprofit_Click(object sender, EventArgs e)
        {

        }

        protected void totalprofit_Click(object sender, EventArgs e)
        {

        }



        protected void ddlmerchant_SelectedIndexChanged(object sender, EventArgs e)
        {
            //calsum();
            bindprofit();
            bndorderscount();
            lblprofittext.Visible = true;
            lbldistname.Visible = true;
            lblprofit.Visible = true;
            lblordercount.Visible = true;
            lbltxtorderscount.Visible = true;
            lblmerchantshopname.Visible = true;
            lblmshopname.Visible = true;
            lbltxtmname.Visible = true;
            lbltxtvillage.Visible = true;
            lblvillage.Visible = true;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            //SqlCommand cmd = new SqlCommand("select (d.name) as merchantname,d.address,o.amount,o.actualamount,(convert (float,o.amount)-convert (float,o.actualamount)) as profit from DistributorLogin d inner join tblorder o on d.id=o.Distributor_id inner join tblorderitem oi on o.id=oi.order_id where o.Distributor_id='" + ddlmerchent.SelectedValue + "'", conn44);
            SqlCommand cmd = new SqlCommand("select  ROW_NUMBER() over(order by o.id) as sno, o.date,o.actualamount,o.amount,o.adminprof,o.travelcharges,o.updatedamount,d.name,d.address from tblorder2 o inner join DistributorLogin d on o.mid=d.id where o.mid='" + ddlmerchant.SelectedValue + "'", conn44);
            SqlCommand cmd1 = new SqlCommand("select name,shopname,address from distributorlogin where name='" + ddlmerchant.SelectedItem.Text + "' and role='merchant'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            da.Fill(ds);
            da1.Fill(ds1);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0 || ds1.Tables[0].Rows.Count > 0)
            {
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                lbldistname.Text = ds1.Tables[0].Rows[0]["name"].ToString();
                lblmerchantshopname.Text= ds1.Tables[0].Rows[0]["shopname"].ToString();
                lbltxtmname.Text= ds1.Tables[0].Rows[0]["name"].ToString();
                lblvillage.Text = ds1.Tables[0].Rows[0]["address"].ToString();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                int columncount = grdcustdetails.Rows[0].Cells.Count;
                grdcustdetails.Rows[0].Cells.Clear();
                grdcustdetails.Rows[0].Cells.Add(new TableCell());
                grdcustdetails.Rows[0].Cells[0].ColumnSpan = columncount;
                grdcustdetails.Rows[0].Cells[0].Text = "No Records Found";
            }


        }
    }
}
