﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Nirmal.DataAccessLayer;

namespace Nirmal.prasad
{
    public partial class Savedata : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {

                binddata();
            }
        }

        protected void btsave_Click(object sender, EventArgs e)
        {
            string date = DateTime.Now.ToString("dd-MM-yyyy");
            DataQueries.InsertCommon("insert into admindata(date,data) values('"+date+"','"+txtmessage.Text+"')");
            binddata();
            txtmessage.Text = string.Empty;

        }
        protected void binddata()
        {
           DataSet ds= DataQueries.SelectCommon("select * from admindata");
            GridLogin.DataSource = ds;
            GridLogin.DataBind();
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            DataQueries.DeleteCommon("delete admindata where id='"+id+"'");
            binddata();
        }
    }
}