﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="distributorlistloc.aspx.cs" Inherits="Nirmal.prasad.distributorlistloc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdcustdetails.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
   
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Distributor Details</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
                <div class="col-md-3" style="padding-left: 10px;">  
      </div>
                <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            
                                    <div class="col-md-3">
                                        <label>Dealer Name</label>
                                        <asp:DropDownList ID="ddlAdmins" class="form-control" runat="server" OnSelectedIndexChanged="ddlAdmins_SelectedIndexChanged" AutoPostBack="true">
                                        
                                        </asp:DropDownList>
                                    </div>
                         
                            <h3 style="padding-top:15px"><b><asp:Label ID="lbldslctname" runat="server"></asp:Label></b><asp:Label ID="lblloc" runat="server"> Location:&nbsp;</asp:Label><u><asp:Label ID="lbldlocation" runat="server"></asp:Label></u></h3>
                          
                         
                                
                                    <div class="col-md-2">
                                        
                                        <asp:Button ID="btnsubmit" class="btn btn-success" runat="server" Text="Submit" OnClientClick=" return validate()" OnClick="btnsubmit_Click" Visible="false" />
                                        

                                    </div>



                                    </div>
                               
                              </div>
                                <br />
                               
                           
                        </div>
                    </div>
                    <div class="col-md-12">
                       <%-- <div class="col-md-4">
                            <h3>
                                <asp:Label ID="lblTotal" runat="server" Visible="false" ForeColor="Blue"></asp:Label></h3>
                        </div>--%>
                       <%-- <div class="col-md-5" style="padding-top: 18px">
                            <asp:Button ID="btnExcel" runat="server" Width="25%" Visible="false" Text="Export Excel" class="btn btn-success btn-sm btn-block" />

                        </div>--%>
                    </div>

                    <div class="col-md-12">
                        <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="grdcustdetails" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive table-striped">
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:BoundField DataField="name" HeaderText="Distributor Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="mobile" HeaderText="Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Address" HeaderText="Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    
                                </Columns>
                            </asp:GridView>
                           <%-- <div id="div1" runat="server" visible="false">
                                <div class="col-md-5" style="padding-top: 16px">
                                    <div class="form-group">
                                        <asp:Button ID="btnword" runat="server" class="btn btn-success"
                                            Text="Export Ms-Word" BackColor="#33ffe3" ForeColor="#000000" />
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-top: 16px">
                                    <div class="form-group">
                                        <asp:Button ID="btnpdf" runat="server" class="btn btn-danger"
                                            Text="Export PDF" BackColor="Orange" ForeColor="#000000" />
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
