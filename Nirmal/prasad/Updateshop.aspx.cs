﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Nirmal.prasad
{
    public partial class Updateshop : System.Web.UI.Page
    {
        string did = string.Empty;
        string lc = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            did = Request.QueryString["id"].ToString();

            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select loc,shopname from Distributorlogin where id='" + did + "'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lc = ds.Tables[0].Rows[0]["loc"].ToString();
              

            }
            if (!IsPostBack)
            {
                SqlConnection conn4 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
                conn4.Open();
                SqlCommand cm = new SqlCommand("select loc,shopname from Distributorlogin where id='" + did + "'", conn4);
                SqlDataAdapter d = new SqlDataAdapter(cm);
                DataSet dss = new DataSet();
                d.Fill(dss);
                conn4.Close();
                if (dss.Tables[0].Rows.Count > 0)
                {
                    
                    shops.Text = dss.Tables[0].Rows[0]["shopname"].ToString();

                }
                listbind(lc);
                villagebind(lc);
            }

        }

        protected void btnsubmit_Click(object sender, EventArgs e)

        {
            string message = "";
            string messagev = "";
            string shpname = string.Empty;
            string vilage = string.Empty;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmdd = new SqlCommand("select shopname,village from Distributorlogin where id='" + did + "'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmdd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                shpname = ds.Tables[0].Rows[0]["shopname"].ToString();
                vilage = ds.Tables[0].Rows[0]["village"].ToString();

            }

            
            
            

            SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());

            if (list1.SelectedIndex != -1 && list2.SelectedIndex == -1)
            {
                foreach (System.Web.UI.WebControls.ListItem item in list1.Items)
                {
                    if (item.Selected)
                    {
                        message += item.Text + ",";

                    }
                }
                string sub = message.Substring(0, message.Length - 1);
                string ss = string.Concat(shpname, ",", sub);

                SqlCommand cmd = new SqlCommand("Update DistributorLogin set shopname='" + ss + "' where id='" + did + "'", connstr);
                connstr.Open();
                cmd.ExecuteNonQuery();
                connstr.Close();
            }
            else if(list2.SelectedIndex!=-1 && list1.SelectedIndex== -1)
                {
                foreach (System.Web.UI.WebControls.ListItem itemv in list2.Items)
                { 
                    
                        if (itemv.Selected)
                        {
                            messagev += itemv.Text + ",";

                        }
                    }
                string subv = messagev.Substring(0, messagev.Length - 1);
                string vv = string.Concat(vilage, ",", subv);


                SqlCommand cmd = new SqlCommand("Update DistributorLogin set village='" + vv + "' where id='" + did + "'", connstr);
                connstr.Open();
                cmd.ExecuteNonQuery();
                connstr.Close();
            }

          
           
            else
            {
                foreach (System.Web.UI.WebControls.ListItem item in list1.Items)
                {
                    if (item.Selected)
                    {
                        message += item.Text + ",";

                    }
                }
                foreach (System.Web.UI.WebControls.ListItem itemv in list2.Items)
                { 
                        if (itemv.Selected)
                        {
                            messagev += itemv.Text + ",";

                        }
                    }
                string sub = message.Substring(0, message.Length - 1);
                string ss = string.Concat(shpname, ",", sub);

                string subv = messagev.Substring(0, messagev.Length - 1);
                string vv = string.Concat(vilage, ",", subv);
                SqlCommand cmd = new SqlCommand("Update DistributorLogin set shopname='"+ss+"', village='" + vv + "' where id='" + did + "'", connstr);
                connstr.Open();
                cmd.ExecuteNonQuery();
                connstr.Close();


            }
            Response.Redirect("Distributorslist.aspx");
        }


        public void listbind(string loc)
        {
            string strcon = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection connection = new SqlConnection(strcon);
            connection.Open();
            SqlCommand cmdd = new SqlCommand("select * from Distributorlogin where loc='" + loc + "' and shopname is not null and role='merchant' ", connection);
            SqlDataReader drr = cmdd.ExecuteReader();
            list1.DataSource = drr;
            list1.DataValueField = "id";
            list1.DataTextField = "shopname";
            list1.DataBind();
            connection.Close();
            list1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "0"));
        }
        public void villagebind(string loc)
        {
            string strcon = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection connection = new SqlConnection(strcon);
            connection.Open();
            SqlCommand cmdd = new SqlCommand("select * from Distributorlogin where loc='" + loc + "'", connection);
            SqlDataReader drr = cmdd.ExecuteReader();
            list2.DataSource = drr;
            list2.DataValueField = "id";
            list2.DataTextField = "village";
            list2.DataBind();
            connection.Close();
            list2.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "0"));

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("Update DistributorLogin set shopname='" + shops.Text + "' where id='" + did + "'", conn44);
           
            cmd.ExecuteNonQuery();
            conn44.Close();
            Response.Redirect("Distributorslist.aspx");

        }
    }
}