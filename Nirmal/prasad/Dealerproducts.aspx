﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Dealerproducts.aspx.cs" Inherits="Nirmal.prasad.Dealerproducts" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
       function showimagepreview(input) {
           if (input.files && input.files[0]) {
               var filerdr = new FileReader();
               filerdr.onload = function (e) {
                   $('#FileUpload1').attr('src', e.target.result);
               }
               filerdr.readAsDataURL(input.files[0]);
           }
       }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=Gridproducts.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
    <div class="box">
        <!-- Content Header (Page header) -->
        <div class="box-header">
            <h1>Admin Products
        
      </h1>

        </div>
        <!-- Main content -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        
                        <div class="box-body">
                             
                             <div class="col-sm-8 padtop">
                                 <div class="col-xs-3"><b>select Image</b></div>
                       <asp:FileUpload ID="FileUpload2" runat="server" onchange="showimagepreview(this)" Width="214px" />
                       
                   </div>
                           
                             <div class="col-md-3">
                                <div class="form-group">
                            <b>Select Category</b> <asp:DropDownlist ID="dropcat" CssClass="form-control" runat="server" Font-Bold="true"></asp:DropDownlist>
                                     <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtemp"  ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                    </div>
                                 </div>
                            <div class="col-md-3">
                                <div class="form-group">
                            <b>productname Name</b> <asp:TextBox ID="txtpname" CssClass="form-control" runat="server"></asp:TextBox>
                                     
                                    </div>
                                 </div>
                              
                             <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                                    </div>
                                 </div>
                            <h4><b><asp:Label ID="lblitems" runat="server"> ItemsSubTotal:&nbsp;</asp:Label></b><asp:Label ID="lblitemscount" runat="server"></asp:Label></h4>
                            <asp:Label ID="lbl" runat="server" Visible="false"></asp:Label>
                             <div class="col-md-12">
                           <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                      
                    </div>
                            <div class="col-md-12">
                                   
                                <div class="table table-responsive" style="overflow: scroll; height: 420px">
                            <asp:GridView ID="Gridproducts" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server"  Font-Bold="true" DataKeyNames="Id" >
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                     <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  >
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("image")%>' ControlStyle-Height="100" ControlStyle-Width="100" />
        </ItemTemplate>
    </asp:TemplateField>
                                    <asp:BoundField DataField="pname" HeaderText="Product Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Font-Names="Center" />
                                    <asp:BoundField DataField="catname" HeaderText="Category Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    

                                    <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btn-xs" Text="Edit" CommandArgument='<%# Eval("Id") %>'  CausesValidation="false" OnClick="btnEdit_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btndelete"  runat="server" CssClass="btn btn-danger btn-xs" Text="Delete"  OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>' OnClick="btndelete_Click"  CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                </div></div>

                            <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup"
                        PopupControlID="updatePanel" CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="updatePanel" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="350px" Width="650px" ForeColor="#ffffff" style="background-color: rgba(0,0,0,0.8)" >

                                    <h3 style="text-align:center">Edit Category</h3>
                                <hr />
                       

                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                
                        
                             <div class="col-md-4">
                            <div class="form-group">
                                Name
                                <asp:TextBox ID="txtmpname" runat="server" class="form-control" />
                                   
                                </div>
                               </div>
                        
                             
                           
                           
                             <div class="col-md-4">
                            <div class="form-group">
                            Category Name
                                    <asp:TextBox ID="txtmcatname" runat="server" class="form-control" />
                                </div>
                                 </div>
                       <div class="col-sm-3">
                        <label>Insert Image</label>
                   <div class="col-sm-4 padbot" style="margin-right: 1px">
                       <img id="imgprvw" height="100px" width="100px" /><br />
                   </div>
                    <div class="col-sm-8 padtop">
                       <asp:FileUpload ID="Imgprev" runat="server" onchange="showimagepreview(this)" Width="214px" />
                       
                   </div>
                     </div>
                        
                         <div class="clearfix"></div>
                            
                   
                         
                             
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                               
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" OnClick="btnUpdate_Click"
                                        Text="Update Data" class="btn btn-success" />
                                </div>

                                 </div>
                       
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                               </div>
                               
                            </div>
                    </asp:Panel>
                            <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                    </div>
                        </div>
                    </div>
                
            </div>
        </div></div>
</asp:Content>
