﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Suggesstionslist.aspx.cs" Inherits="Nirmal.prasad.Suggesstionslist" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=gridsuggestions.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
    <div class="row">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b style="margin-left:50px">Suggestions</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
             
                <!-- /.box-header -->
               
                    </div>
        <div class="box box-body">
                    <div class="col-md-12">
                           <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                    </div>
                    <div class="col-md-12">
                       
                            <asp:GridView ID="gridsuggestions" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive table-striped" ShowHeaderWhenEmpty="true">
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:TemplateField HeaderText="S NO" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="sname" HeaderText="Customer Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                     <asp:BoundField DataField="sugg" HeaderText="Suggition" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="smob" HeaderText="Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                     <asp:BoundField DataField="sadd" HeaderText="Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                 </Columns>
                                <EmptyDataTemplate>
                                    <div>
                                    No Records Found
                                        </div>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
</asp:Content>


