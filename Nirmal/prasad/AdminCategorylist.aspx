﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="AdminCategorylist.aspx.cs" Inherits="Nirmal.prasad.AdminCategorylist" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdCategory_Details.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
     <div class="row">
        <div class="col-md-12">
          <div class="box">
              
            <div class="box-header  with-border height-border">
              <h3 class="box-title"><b>Category List</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
                
            </div>
              <div class="box-header  with-border height-border">
              <div class="col-md-3" style="padding-left: 0px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox>
     </div>
                  </div>
            <!-- /.box-header -->
            <div class="box-body">
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="grdCategory_Details" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped"  >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
               <asp:TemplateField HeaderText="SNO" ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                   <ItemTemplate>
                       <%# Container.DataItemIndex+1 %>
                   </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  >
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("image")%>'  ControlStyle-Height="100" ControlStyle-Width="100" />
        </ItemTemplate>
    </asp:TemplateField>

               <asp:BoundField DataField="id" HeaderText="id" visible="false"/>
                 <asp:BoundField DataField="categoryname" HeaderText="CategoryName" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
                
                   <%--<asp:BoundField DataField="categoryid" HeaderText="id" visible="false"/>--%>
            <asp:BoundField DataField="description" HeaderText="Description" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               
                  <asp:TemplateField HeaderText="Update Category " HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" Class="fa fa-pencil" 
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
               <asp:TemplateField  HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="50">
                <ItemTemplate>
              <asp:LinkButton ID="lnkDelete" OnClick="lnkDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this Category?')) return false;"  runat="server" CssClass="fa fa-trash" ></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>
                       
           </Columns>
       </asp:GridView>
           </div>
    <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup"
                        PopupControlID="updatePanel" CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="updatePanel" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="350px" Width="650px" ForeColor="#ffffff" style="background-color: rgba(0,0,0,0.8)" >

                                    <h3 style="text-align:center">Edit Category</h3>
                                <hr />
                       

                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                
                        
                             <div class="col-md-4">
                            <div class="form-group">
                                Name
                                <asp:TextBox ID="txtcatname" runat="server" class="form-control" />
                                   
                                </div>
                               </div>
                        
                             
                           
                           
                             <div class="col-md-4">
                            <div class="form-group">
                            Description
                                    <asp:TextBox ID="txtdescription" runat="server" class="form-control" />
                                </div>
                                 </div>
                       <div class="col-sm-3">
                        <label>Insert Image</label>
                   <div class="col-sm-4 padbot" style="margin-right: 1px">
                       <img id="imgprvw" height="100px" width="100px" /><br />
                   </div>
                    <div class="col-sm-8 padtop">
                       <asp:FileUpload ID="Imgprev" runat="server" onchange="showimagepreview(this)" Width="214px" />
                       
                   </div>
                     </div>
                        
                         <div class="clearfix"></div>
                            
                   
                         
                             
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                               
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" OnClick="btnUpdate_Click"
                                        Text="Update Data" class="btn btn-success" />
                                </div>

                                 </div>
                       
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                               </div>
                               
                            </div>
                    </asp:Panel>
                </div></div></div></div>
</asp:Content>


