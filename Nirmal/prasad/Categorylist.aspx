﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Categorylist.aspx.cs" Inherits="Nirmal.prasad.Categorylist" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <div class="row">
        <div class="col-md-12">
          <div class="box">
              
            <div class="box-header  with-border height-border">
              <h3 class="box-title">Category List</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="grdProduct_Details" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped" >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
                <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" >
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("image")%>'  ControlStyle-Height="100" ControlStyle-Width="100" />
        </ItemTemplate>
    </asp:TemplateField>

               <asp:BoundField DataField="id" HeaderText="id" visible="false"/>
                 <asp:BoundField DataField="Categoryname" HeaderText="Category" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                

            <asp:BoundField DataField="description" HeaderText="Description" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               
                       
           </Columns>
       </asp:GridView>
           </div>
    
                    
                    
                </div></div></div></div>
</asp:Content>