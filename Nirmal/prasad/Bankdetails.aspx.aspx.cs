﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Nirmal.prasad
{
    public partial class Bankdetails_aspx : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            bindbankdetails();
        }
       

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("insert into bankdetails(bankname,holdername,accountnumber,branch,ifsc)values('" + txtbankname.Text+ "','" + txtholdername.Text + "','" + txtaccountnbr.Text + "','" + txtbranch.Text + "','" + txtifsc.Text + "')", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
            connstr.Close();
            bindbankdetails();
            txtbankname.Text = string.Empty;
            txtholdername.Text = string.Empty;
            txtaccountnbr.Text = string.Empty;
            txtbranch.Text = string.Empty;
            txtifsc.Text = string.Empty;
        }
        protected void bindbankdetails()
        {
            SqlCommand cmd = new SqlCommand("select id,bankname,holdername,accountnumber,branch,ifsc from bankdetails", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            connstr.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridBank.DataSource = ds;

                GridBank.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridBank.DataSource = ds;
                GridBank.DataBind();
                int columncount = GridBank.Rows[0].Cells.Count;
                GridBank.Rows[0].Cells.Clear();
                GridBank.Rows[0].Cells.Add(new TableCell());
                GridBank.Rows[0].Cells[0].ColumnSpan = columncount;
                GridBank.Rows[0].Cells[0].Text = "No Records Found";
            }

        }
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Select * from bankdetails where id='" + lblid.Text + "'", connstr);
            connstr.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txtebankname.Text = dr["bankname"].ToString();
                txteholder.Text = dr["holdername"].ToString();
                txteaccountnbr.Text = dr["accountnumber"].ToString();
                txtebranch.Text = dr["branch"].ToString();
                txteifsccode.Text = dr["ifsc"].ToString();
              

            }
            connstr.Close();
            ModalPopupExtender1.Show();

        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Update bankdetails set bankname='" + txtebankname.Text + "',holdername='" + txteholder.Text + "',accountnumber='" + txteaccountnbr.Text + "',branch='" + txtebranch.Text + "', ifsc = '" + txteifsccode.Text + "' where id='" + lblid.Text + "'", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
           connstr.Close();
            bindbankdetails();
    }

       

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Delete from bankdetails where id='" + id + "'", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
            connstr.Close();
            bindbankdetails();

        }
    }
}