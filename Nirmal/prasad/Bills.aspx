﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Bills.aspx.cs" Inherits="Nirmal.prasad.Bills" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdCategory_Details.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
     <div class="row">
        <div class="col-md-12">
          <div class="box">
              
            <div class="box-header  with-border height-border">
              <h3 class="box-title"><b>Bills</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
                
            </div>
               
              <div class="box-header  with-border height-border">
              <div class="col-md-3" style="padding-left: 0px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox>
     </div>
                  </div>
            <!-- /.box-header -->
            <div class="box-body">
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="grdCategory_Details" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped"  >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
                <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  >
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("image")%>'  ControlStyle-Height="100" ControlStyle-Width="100" />
        </ItemTemplate>
    </asp:TemplateField>

               <asp:BoundField DataField="oid" HeaderText="Order Id" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
                 <asp:BoundField DataField="mname" HeaderText="Merchant Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
               <asp:BoundField DataField="shopname" HeaderText="Shop Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
               <asp:BoundField DataField="totamount" HeaderText="Order Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
               <asp:BoundField DataField="amount" HeaderText="Bill Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
               <asp:BoundField DataField="reamount" HeaderText="Remaining Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
               <%--<asp:BoundField DataField="shopname" HeaderText="Shop Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />--%>
               <asp:BoundField DataField="loc" HeaderText="Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="date" HeaderText="Order Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="text" HeaderText="Text" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="mnumber" HeaderText="Merchant Number" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                         
                  <%--<asp:TemplateField HeaderText="Update Category " HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" OnClick="lnkEdit_Click" Class="fa fa-pencil" 
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
               <asp:TemplateField  HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="50">
                <ItemTemplate>
              <asp:LinkButton ID="lnkDelete" OnClick="lnkDelete_Click" OnClientClick="if ( !confirm('Are you sure you want to delete this Category?')) return false;"  runat="server" CssClass="fa fa-trash" ></asp:LinkButton><br />
                     </ItemTemplate>
            </asp:TemplateField>--%>
                       
           </Columns>
       </asp:GridView>
           </div>
                     
                </div></div></div></div>
</asp:Content>

