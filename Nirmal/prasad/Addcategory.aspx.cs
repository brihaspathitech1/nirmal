﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class Addcategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            string serverpath = string.Empty;
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg);
            if (Imgprev.HasFile)
            {
                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                Imgprev.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
            }
            String insert = "insert into Category(categoryname,description,image)values('" + txtcategoryname.Text + "','" + txtdiescription.Text + "','" + serverpath+"'  )";
            SqlCommand comm1 = new SqlCommand(insert, conn1);

            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            txtcategoryname.Text = string.Empty;
            txtdiescription.Text = string.Empty;


            //Response.Write("<script>alert('Category inserted successfully')</script>");
            Response.Redirect("AdminCategorylist.aspx");
        }
    }

}