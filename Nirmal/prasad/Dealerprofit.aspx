﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Dealerprofit.aspx.cs" Inherits="Nirmal.prasad.Dealerprofit" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
   
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Dealer Profits</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
               
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label>Dealer Name</label>
                                        <asp:DropDownList ID="ddldistributor" class="form-control" runat="server" OnSelectedIndexChanged="ddldistributor_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    </div>
                              </div>
                    <br />
                    <div class="row">
                         <div class="col-md-3">
                    <b>Pending Payment</b>
                    <asp:Label ID="totprofit" runat="server"></asp:Label></div></div>
                                <br />
                               
                    <div class="col-md-12">
                        <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="grdcustdetails" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive table-striped" DataKeyNames="id" OnRowDataBound="grdcustdetails_RowDataBound" OnSelectedIndexChanged="grdcustdetails_SelectedIndexChanged">
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="Order Id" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />                           
                                   
                                    <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />                           
                                    <asp:BoundField DataField="location" HeaderText="Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
                                   <asp:BoundField DataField="distsub" HeaderText="Dist Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                   
                                </Columns>
                            </asp:GridView>
                          
                        </div>
                    </div>
                </div>
            </div></div></div>
    <asp:Label ID="lblid" runat="server" Visible="false"></asp:Label>
     <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" CancelControlID="btncancel" TargetControlID="HiddenField1"></cc1:ModalPopupExtender>
                        <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px" Height="300px" Width="350px" ForeColor="#ffffff" Style="background-color: rgba(0,0,0,0.8)">
                            <h3 class="text-center">Pay Amount</h3>
                            <hr />
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Pay Amount</b>
                                    <asp:TextBox ID="amt" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-top: 20px">
                                <div class="form-group">
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                    <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="btnupdate_Click" />
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-top: 20px">
                                <div class="form-group">
                                    <asp:Button ID="btncancel" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                                </div>
                            </div>
                        </asp:Panel>
</asp:Content>

