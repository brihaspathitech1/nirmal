﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Adminlogins.aspx.cs" Inherits="Nirmal.prasad.Adminlogins" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .GridViewHeaderStyle th {
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#FileUpload1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    


    <!-- Select2 -->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" />

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #000 !important
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
        function Search_Gridview(strKey) {
            var strData = strKey.value.toLowerCase().split(" ");
            var tblData = document.getElementById("<%=GridLogin.ClientID %>");
            var rowData;
            for (var i = 1; i < tblData.rows.length; i++) {
                rowData = tblData.rows[i].innerHTML;
                var styleDisplay = 'none';
                for (var j = 0; j < strData.length; j++) {
                    if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                        styleDisplay = '';
                    else {
                        styleDisplay = 'none';
                        break;
                    }
                }
                tblData.rows[i].style.display = styleDisplay;
            }
        }
    </script>
    <div class="box">
        <!-- Content Header (Page header) -->
        <div class="box-header">
            <h1>Admin Logins </h1>

        </div>
        <!-- Main content -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Admin Name</b>
                                    <asp:TextBox ID="txtadminname" CssClass="form-control" runat="server"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="txtemp"  ForeColor="Red"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>User Name</b>
                                    <asp:TextBox ID="txtusername" CssClass="form-control" runat="server"></asp:TextBox>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Password</b>
                                    <asp:TextBox ID="txtpwd" CssClass="form-control" runat="server"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Mobile</b><asp:TextBox ID="txtmobile" CssClass="form-control" runat="server"></asp:TextBox>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Email</b>
                                        <asp:TextBox ID="txtemail" CssClass="form-control" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Select Role</b>
                                        <asp:DropDownList ID="droprole" CssClass="form-control" runat="server" Font-Bold="true" OnSelectedIndexChanged="droprole_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Value="0">Admin</asp:ListItem>
                                            <asp:ListItem Value="1">distributor</asp:ListItem>
                                            <asp:ListItem Value="2">dealer</asp:ListItem>
                                            
                                            <asp:ListItem Value="3">Default</asp:ListItem>


                                        </asp:DropDownList>

                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Location</b>
                                        <asp:TextBox ID="loc" CssClass="form-control" runat="server" OnTextChanged="loc_TextChanged" AutoPostBack="true"></asp:TextBox>
                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="loc"
                                            MinimumPrefixLength="1" EnableCaching="true" CompletionSetCount="1"
                                            CompletionInterval="1" ServiceMethod="getlocation" UseContextKey="True">
                                        </cc1:AutoCompleteExtender>

                                    </div>
                                </div>
                                <div class="col-md-1">
                    <div class="form-group">
                        <br />
                        
                        <asp:LinkButton runat="server" ID="LinkButton1" data-toggle="modal" data-target="#myModal" Height="50"><i class="fa fa-plus-circle" style="font-size:35px"></i></asp:LinkButton>
                    </div>
                     </div>
                                

                                </div></div>
                        <div class="row">
                             <div class="col-md-12">
                                 <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Address</b>
                                        <asp:TextBox ID="txtadress" CssClass="form-control" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Village</b>
                                        <asp:TextBox ID="village" CssClass="form-control" runat="server"></asp:TextBox>

                                    </div>
                                </div>

                           
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Distributor Expiry Date</b>
                                        <asp:TextBox ID="txtdexpiry" CssClass="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server"
                                            TargetControlID="txtdexpiry" Format="yyyy-MM-dd" />

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Aadhar No:</b>
                                        <asp:TextBox ID="aadhar" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div></div> </div>
                            <div class="clearfix"></div>
                        
                                <div id="sh" runat="server" visible="false">
                                 <div class="col-md-6">
                      <div class="form-group">
                          <b>ShopNames</b>
                          <asp:ListBox ID="list1" runat="server" class="form-control select2" SelectionMode="multiple" data-placeholder="Select " Style="width: 100%;" ></asp:ListBox>
                      </div>
                  </div></div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Account Number:</b>
                                        <asp:TextBox ID="acctno" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Bank Name:</b>
                                        <asp:TextBox ID="bank" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div>
                      
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Branch</b>
                                        <asp:TextBox ID="branch" CssClass="form-control" runat="server"></asp:TextBox>

                                    </div>
                                </div>
                           
                        
                       
                            
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <b>IFSC </b>
                                        <asp:TextBox ID="ifsc" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div>
                                
                                <div class="col-sm-4 padtop">
                                    <label>Select Image</label>
                                    <asp:FileUpload ID="FileUpload1" runat="server" onchange="showimagepreview(this)" Width="214px" />

                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />
                                    </div>
                                </div>
                           
                       

                        <div class="clearfix"></div>
                        <div class="col-md-3" style="padding-left: 10px;">
                            <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
                        </div>
                        <asp:Label ID="lb" runat="server" Visible="false"></asp:Label>
                        <div class="col-md-12">
                            <div class="table table-responsive" style="overflow: scroll; height: 420px" >
                                <asp:GridView ID="GridLogin" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" Font-Bold="true">
                                    <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Image" ItemStyle-Height="50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("image")%>' ControlStyle-Height="100" ControlStyle-Width="100" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="username" HeaderText="User Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="password" HeaderText="Password" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="mobile" HeaderText="Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="aadhar" HeaderText="Aadhar" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="address" HeaderText="Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />


                                        <asp:BoundField DataField="loc" HeaderText="Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="role" HeaderText="Role" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="bankname" HeaderText="Bank Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="acctno" HeaderText="Account No" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="branch" HeaderText="Branch" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="ifsc" HeaderText="IFSC" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />

                                        <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btn-xs" Text="Edit" CommandArgument='<%# Eval("Id") %>' OnClick="btnEdit_Click" CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button ID="btndelete" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>' OnClick="btndelete_Click" CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" CancelControlID="btncancel" TargetControlID="HiddenField1"></cc1:ModalPopupExtender>
                        <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px" Height="620px" Width="650px" ForeColor="#ffffff" Style="background-color: rgba(0,0,0,0.8)">
                            <h3 class="text-center">Update Login Details</h3>
                            <hr />
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Admin Name</b>
                                    <asp:TextBox ID="txtname" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <b>Email</b>
                                    <asp:TextBox ID="txtmail" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>User Name</b>
                                    <asp:TextBox ID="txtuid" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Password </b>
                                    <asp:TextBox ID="txtpaswd" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>


                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Mobile</b><asp:TextBox ID="txtmbile" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Expiry Date</b>
                                    <asp:TextBox ID="txtexpiry" runat="server" class="form-control" AutoComplete="off" />
                                    <cc1:CalendarExtender ID="CalendarExtender1" runat="server"
                                        BehaviorID="txtDate_CalendarExtender" TargetControlID="txtexpiry" Format="yyyy-MM-dd" />

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Account Number:</b>
                                    <asp:TextBox ID="updateacctno" CssClass="form-control" runat="server"></asp:TextBox>


                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Bank Name:</b>
                                    <asp:TextBox ID="updatebank" CssClass="form-control" runat="server"></asp:TextBox>


                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>Branch</b>
                                    <asp:TextBox ID="updatebranch" CssClass="form-control" runat="server"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <b>IFSC </b>
                                    <asp:TextBox ID="updateifsc" CssClass="form-control" runat="server"></asp:TextBox>


                                </div>
                            </div>


                            <div class="col-md-3">
                                    <div class="form-group">
                                        <b>Aadhar No:</b>
                                        <asp:TextBox ID="TextBox1" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div>
                            <div class="col-md-6">
                                    <div class="form-group">
                                        <b>Shopname:</b>
                                        <asp:TextBox ID="shpname" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div>
                            <div class="col-md-9">
                                    <div class="form-group">
                                        <b>Address:</b>
                                        <asp:TextBox ID="address" CssClass="form-control" runat="server"></asp:TextBox>


                                    </div>
                                </div>
                            <div class="col-xs-3"><b>select Image</b></div>
                            <div class="col-sm-8 padtop">
                                <asp:FileUpload ID="Imgprev" runat="server" Width="214px" />

                            </div>


                            <div class="col-md-3" style="padding-top: 20px">
                                <div class="form-group">
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                    <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="btnupdate_Click" />
                                </div>
                            </div>
                            <div class="col-md-3" style="padding-top: 20px">
                                <div class="form-group">
                                    <asp:Button ID="btncancel" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title text-center" id="myModalLabel">Add Location</h4>
                                    </div>
                        <div class="col-md-offset-3">
                             
                         </div>
                         <div class="modal-body">
                             <p>
                                 <asp:TextBox ID="locate" runat="server" class="form-control" />
                             </p>
                         </div>
                         <div class="modal-footer col-md-offset-5">
                             <asp:Button ID="Button1" CommandName="Update" runat="server"  CausesValidation="false" Text="Add" class="btn btn-primary" OnClick="Button1_Click" />

                         </div>
                     </div>
                                </div>
                            </div>
</asp:Content>
