﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="AddBanners.aspx.cs" Inherits="Nirmal.prasad.AddBanners" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }

        body {
            color: #000 !important;
            font-weight: bold !important;
        }
    </style>
 <script type="text/javascript">
       function showimagepreview(input) {
           if (input.files && input.files[0]) {
               var filerdr = new FileReader();
               filerdr.onload = function (e) {
                   $('#imgprvw').attr('src', e.target.result);
               }
               filerdr.readAsDataURL(input.files[0]);
           }
       }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <section class="content-header">
                <h4>Add Banners</h4>
                <hr /></section>

    <section class="content">
       
	
	
	<div class="row">

         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
             <h3>&nbsp</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            
              
              </div>
            </div>



              
                <div class="box-body">
              <div class="row">
                 
                
                      
                       
                 
                  <div class="col-sm-3">
                   <div class="col-sm-4 padbot" style="margin-right: 1px">
                       <img id="imgprvw" height="100px" width="100px" /><br />
                   </div>
                  <div class="col-sm-8 padtop">
                       <asp:FileUpload ID="Imgprev" runat="server" onchange="showimagepreview(this)" Width="214px" />
                       
                   </div></div>
                                      <div class="col-sm-2" style="margin-left: 11px; margin-top: 14px">
                            <asp:Button ID="Add" runat="server" Text="Add" class="btn btn-success btn-block" OnClick="Add_Click" />
                        </div>
                    
              
                  
                  </div>
                    <div class="clearfix"></div>
                        
                        <asp:Label ID="lb" runat="server" Visible="false"></asp:Label>
                        <div class="col-md-12">
                            <div class="table table-responsive" style="overflow: scroll; height: 420px">
                                <asp:GridView ID="GridLogin" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" Font-Bold="true">
                                    <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Image" ItemStyle-Height="50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("content")%>' ControlStyle-Height="100" ControlStyle-Width="100" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
          
                                        <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button ID="btndelete" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>' OnClick="btndelete_Click" CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                   
                        </div>

                        </div></div>
        </div>

<script>
               

                var placeSearch, autocomplete;
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    // Create the autocomplete object, restricting the search to geographical
                    // location types.
                    autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        { types: ['geocode'] });

                    // When the user selects an address from the dropdown, populate the address
                    // fields in the form.
                    autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                    // Get the place details from the autocomplete object.
                    var place = autocomplete.getPlace();

                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                        document.getElementById(component).disabled = false;
                    }

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }

                // Bias the autocomplete object to the user's geographical location,
                // as supplied by the browser's 'navigator.geolocation' object.
                function geolocate() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOSxb5hyc4b2X7YF9WZJVDVjsKhrEd1lI&libraries=places&callback=initAutocomplete"
                async defer></script>

        </section>
    </section>
</asp:Content>

