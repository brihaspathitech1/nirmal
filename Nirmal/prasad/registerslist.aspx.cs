﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nirmal.DataAccessLayer;
using iTextSharp.text.html.simpleparser;
using System.Configuration;

namespace Nirmal.prasad
{
    public partial class registerslist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

          if (!IsPostBack)
            {
                Response.Cookies["currentpage"].Value = System.IO.Path.GetFileName(Request.Url.AbsolutePath);

                list();
            }

        }
        public void list()
        {
            DataSet ds = DataQueries.SelectCommon("select id,date,fname,uname,mobile,pwd,email,role,( loc+'-'+address+'-'+village) as address,aadhar,accountno,holdername,bankname,branch,ifsc from siteregister order by id desc");

            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";

            }

        }
        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
         DataQueries.DeleteCommon("Delete from siteregister where id='" + id + "'");
            list();
        }

    }
}