﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="ordersconfirm.aspx.cs" Inherits="Nirmal.prasad.ordersconfirm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
     <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=GridView1.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
    <section class="content-header">
        <h1>Approved</h1>
    </section>
    <section>
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                          <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                        <div class="box-body">
                               <div class="col-md-12">
                           
                      
                    </div>
                           
                            <div class="row">

                                <div class="col-lg-12">
                                    <asp:GridView ID="GridView1" DataKeyNames="Id1" class="table table-bordered table-striped" runat="server" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" AutoGenerateColumns="False">
                                        <Columns>
                                             <asp:TemplateField HeaderText="SNO" ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#12a7ff" HeaderStyle-ForeColor="white" HeaderStyle-Font-Size="16px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                   <ItemTemplate>
                       <%# Container.DataItemIndex+1 %>
                   </ItemTemplate>
               </asp:TemplateField>
                                            <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="name" HeaderText="Distributor Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="loc" HeaderText="Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height="50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                           
                                    
                                            <asp:TemplateField HeaderText="Approve" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lb1" runat="server" CssClass="form-control" Text="completed"></asp:Label>
                                            </ItemTemplate>
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="20" ItemStyle-Font-Size="Small" ItemStyle-ForeColor="Red">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="linkdelete" runat="server" CssClass="form-control" Text="Delete" OnClick="linkdelete_Click" ForeColor="Red"></asp:LinkButton>
                                            </ItemTemplate>
                                    </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <asp:Label ID="lblId" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
           
            </div>
        </div>
    </section>
</asp:Content>
