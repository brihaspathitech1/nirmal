﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Nirmal.DataAccessLayer;
using iTextSharp.text.html.simpleparser;
using System.Configuration;
namespace Nirmal.prasad
{
    public partial class Dealerprofit : System.Web.UI.Page
    {
        SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind_distributors();

            }

        }
        protected void bind_distributors()
        {
            DataSet ds = DataQueries.SelectCommon("select id, name from [DistributorLogin] where role= 'dealer'");
            ddldistributor.DataTextField = "name";
            ddldistributor.DataValueField = "id";
            ddldistributor.DataSource = ds;
            ddldistributor.Items.Insert(0, "All");
            ddldistributor.DataBind();
        }

        protected void ddldistributor_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("with cte as(select o.id,o.date,o.amount,o.location,(cast(a.dealamount as float)*i.count) as distsub from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id where a.did='" + ddldistributor.SelectedValue + "' and o.id not in (select oid from payamount where role='dealer')) select id,date,amount,sum(distsub) as distsub,location from cte group by id,date,amount,location", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                profit();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                int columncount = grdcustdetails.Rows[0].Cells.Count;
                grdcustdetails.Rows[0].Cells.Clear();
                grdcustdetails.Rows[0].Cells.Add(new TableCell());
                grdcustdetails.Rows[0].Cells[0].ColumnSpan = columncount;
                grdcustdetails.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void profit()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select sum((cast(a.dealamount as float)*i.count))-(select isnull(sum(cast(damount as float)),0) from payamount where did='" + ddldistributor.SelectedValue + "' and role='dealer') as distsub from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id  where a.did='" + ddldistributor.SelectedValue + "' ", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                totprofit.Text = ds.Tables[0].Rows[0]["distsub"].ToString();

            }
        }

        protected void grdcustdetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(grdcustdetails, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
            }
        }

        protected void grdcustdetails_SelectedIndexChanged(object sender, EventArgs e)
        {
            int dataKey = Convert.ToInt32(grdcustdetails.DataKeys[grdcustdetails.SelectedIndex].Value);
            lblid.Text = Convert.ToString(dataKey);
            this.ModalPopupExtender1.Show();
        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("insert into payamount(did,damount,oid,role) values('" + ddldistributor.SelectedValue + "','" + amt.Text + "','" + lblid.Text + "','dealer')", conn44);
            cmd.ExecuteNonQuery();
            conn44.Close();
            SqlConnection conn444 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn444.Open();
            //SqlCommand cmdd = new SqlCommand("select o.*,(cast(a.distamount as float)*i.count) as distsub from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id where d.id='" + ddldistributor.SelectedValue + "' and o.id not in (select oid from payamount)", conn444);
            SqlCommand cmdd = new SqlCommand("with cte as(select o.id,o.date,o.amount,o.location,(cast(a.dealamount as float)*i.count) as distsub from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id where a.did='" + ddldistributor.SelectedValue + "' and o.id not in (select oid from payamount where role='dealer')) select id,date,amount,sum(distsub) as distsub,location from cte group by id,date,amount,location", conn444);
            SqlDataAdapter da = new SqlDataAdapter(cmdd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn444.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                profit();

            }

        }
    }
}