﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;

namespace Nirmal.prasad
{
    public partial class Bulkmerchantorders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            gridbind();
        }
        protected void gridbind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd1 = new SqlCommand("select o.id,d.name,o.date,o.amount,o.updatedamount,o.actualamount,o.adminprof,o.travelcharges,d.shopname from tblorder2 o inner join distributorlogin d on o.mid=d.id order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";
            }

        }
        

        protected void btnitems_Click(object sender, EventArgs e)
        {
  
            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = grdProduct_Details.DataKeys[grow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select o.id, o.oid, o.date,d.name,d.address,o.amount,o.status,d.mobile,d.loc,(c.name) as CustomerName from  tblorderitem2 o inner join DistributorLogin d on o.Distributor_id=d.id inner join customer c on c.id=o.customer_id where o.status = 'Approved' and o.ordid='" + lblId.Text + "')  select *, substring(cte.date, 0, 11) as dt from cte");
            grid1.DataSource = ds;
            grid1.DataBind();
            modal1.Show();
            
        }

        
    }
}