﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nirmal.DataAccessLayer;
using System.Configuration;

namespace Nirmal.prasad
{
    public partial class Profits : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                adminprofbind();
                thismothadminprofbind();
                totordersamount();
                thismothordersamount();
                totdealersamount();
                monthdealersamount();
                totdistamount();
                monthdistamount();
                merchantsaleamount();
                monthmerchantsaleamount();
                totaltravelcharges();
                thismonthtravelcharges();

            }

        }
        protected void adminprofbind()
        {
            DataSet ds = DataQueries.SelectCommon("select sum((cast(isnull(o2.updatedamount,0) as float)- cast(IsNull(o2.travelcharges,0)  as float)) )as oamount from tblorder2 o2 ");
            DataSet ds1 = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)+cast(a.distamount as float)) as empamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id");
            if (ds.Tables[0].Rows.Count > 0)
            {
                o2total.Text = ds.Tables[0].Rows[0]["oamount"].ToString();
            }
            if (ds1.Tables[0].Rows.Count > 0)
            {
                atotal.Text = ds1.Tables[0].Rows[0]["empamount"].ToString();
            }

            double d = Convert.ToDouble(o2total.Text) - Convert.ToDouble(atotal.Text);
            lbladminprof.Text = d.ToString();
        }
        protected void thismothadminprofbind()
        {
            DataSet ds = DataQueries.SelectCommon("select sum((cast(isnull(o2.updatedamount,0) as float)- cast(IsNull(o2.travelcharges,0)  as float)) )as omonthamount from tblorder2 o2 where month(o2.date) <= month(getdate()) and year(o2.date)=year(getdate())");
            DataSet ds1 = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)+cast(a.distamount as float)) as empmonthamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id  where month(o2.date) <= month(getdate()) and year(o2.date)=year(getdate())");
            if (ds.Tables[0].Rows.Count > 0)
            {
                o2thismonthtotal.Text = ds.Tables[0].Rows[0]["omonthamount"].ToString();
            }
            if (ds1.Tables[0].Rows.Count > 0)
            {
                athismonthtotal.Text = ds1.Tables[0].Rows[0]["empmonthamount"].ToString();
            }

            double d = Convert.ToDouble(o2thismonthtotal.Text) - Convert.ToDouble(athismonthtotal.Text);
            lblmonthadminprof.Text = d.ToString();

        }
        protected void totordersamount()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(amount as float)) as orderamount from tblorder2");
            lbltotordersamount.Text = ds.Tables[0].Rows[0]["orderamount"].ToString();
        }
        protected void thismothordersamount()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(amount as float)),0) as monthorderamount from tblorder2 where  month(date)=month(getdate()) and year(date)=year(getdate())");
            lblmonthordersamount.Text = ds.Tables[0].Rows[0]["monthorderamount"].ToString();
        }
        protected void totdealersamount()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)) as dealamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id");
            totdealprofitamount.Text = ds.Tables[0].Rows[0]["dealamount"].ToString();
        }
        protected void monthdealersamount()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)) as dealamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id where month(o2.date) <= month(getdate()) and year(o2.date)=year(getdate())");
            thismonthdealprofitamount.Text = ds.Tables[0].Rows[0]["dealamount"].ToString();

        }
        protected void totdistamount()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as totdistamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id");
            totdistprofitamount.Text = ds.Tables[0].Rows[0]["totdistamount"].ToString();
        }
        protected void monthdistamount()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as monthdistamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id where month(o2.date) <= month(getdate()) and year(o2.date)=year(getdate())");
            thismonthdistprofitamount.Text = ds.Tables[0].Rows[0]["monthdistamount"].ToString();
        }
        protected void merchantsaleamount()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(actualamount as float)),0 ) as merchtotamount from tblorder2");
            totmerchamount.Text = ds.Tables[0].Rows[0]["merchtotamount"].ToString();
        }
        protected void monthmerchantsaleamount()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(actualamount as float)),0 ) as monthmerchamount from tblorder2 where month(date)=month(getdate()) and year(date)=year(getdate())");
            tismonthmerchamount.Text = ds.Tables[0].Rows[0]["monthmerchamount"].ToString();
        }
        protected void totaltravelcharges()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(travelcharges as float)),0 ) as tottravelcharges from tblorder2 ");
            tottravelcharges.Text = ds.Tables[0].Rows[0]["tottravelcharges"].ToString();

        }
        protected void thismonthtravelcharges()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(travelcharges as float)),0 ) as thismonthtravelcharges from tblorder2 where month(date)=month(getdate()) and year(date)=year(getdate())");
            thismonthtravelcharge.Text = ds.Tables[0].Rows[0]["thismonthtravelcharges"].ToString();
        }
        protected void adminprofitdatetodate()
        {
            DataSet ds = DataQueries.SelectCommon("select sum((cast(isnull(o2.updatedamount,0) as float)- cast(IsNull(o2.travelcharges,0)  as float)) )as omonthamount from tblorder2 o2 where o2.date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            DataSet ds1 = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)+cast(a.distamount as float)) as empmonthamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id  where o2.date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            if (ds.Tables[0].Rows.Count > 0)
            {
                o2datetotal.Text = ds.Tables[0].Rows[0]["omonthamount"].ToString();
            }
            if (ds1.Tables[0].Rows.Count > 0)
            {
                adatetotal.Text = ds1.Tables[0].Rows[0]["empmonthamount"].ToString();
            }

            double d = Convert.ToDouble(o2datetotal.Text) - Convert.ToDouble(adatetotal.Text);
            lbldateadminprofit.Text = d.ToString();
        }
        protected void ordersamountdatetodate()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(amount as float)),0) as monthorderamount from tblorder2 where date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            lbldateordersamount.Text = ds.Tables[0].Rows[0]["monthorderamount"].ToString();

        }
        protected void dealerprofitdatetodate()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)) as dealamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id where o2.date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            lbldatedealerprofit.Text = ds.Tables[0].Rows[0]["dealamount"].ToString();

        }
        protected void distprofitdatetodate()
        {
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as monthdistamount  from tblorder2 o2 inner join tblorderitem2 i2 on o2.id=i2.ordid inner join tblorderitem i on i2.oid=i.order_id inner join Addproduct a on i.product_id=a.id where o2.date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            lbldatedistprofit.Text = ds.Tables[0].Rows[0]["monthdistamount"].ToString();

        }
        protected void merchprofitdatetodate()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(actualamount as float)),0 ) as monthmerchamount from tblorder2 where date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            lbldatemerchantsale.Text = ds.Tables[0].Rows[0]["monthmerchamount"].ToString();
        }
        protected void travelchargesdatetodate()
        {
            DataSet ds = DataQueries.SelectCommon("select isnull(sum(cast(travelcharges as float)),0 ) as thismonthtravelcharges from tblorder2 where date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110)");
            lbldatetravelcharges.Text = ds.Tables[0].Rows[0]["thismonthtravelcharges"].ToString();
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            admindiv.Visible = false; ordersdiv.Visible = false; ; dealersdiv.Visible = false;
            distdiv.Visible = false; merchantsdiv.Visible = false; traveldiv.Visible = false;
            lbldateadminprofittext.Visible = true; lbldateadminprofit.Visible = true;
            lbldateordersamounttext.Visible = true; lbldateordersamount.Visible = true;
            lbldatedealerprofit.Visible = true; lbldatedealerprofittext.Visible = true;
            lbldatedistprofittext.Visible = true; lbldatedistprofit.Visible = true;
            lbldatemerchantsaletext.Visible = true; lbldatemerchantsale.Visible = true;
            lbldatetravelchargestext.Visible = true; lbldatetravelcharges.Visible = true;
            adminprofitdatetodate();
            ordersamountdatetodate();
            dealerprofitdatetodate();
            distprofitdatetodate();
            merchprofitdatetodate();
            travelchargesdatetodate();


        }
    }
}