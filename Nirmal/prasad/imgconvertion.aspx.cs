﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Nirmal.DataAccessLayer;
using System.IO;

namespace Nirmal.prasad
{
    public partial class imgconvertion : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                BindGrid();
            }

           
        }
    
      

     
        protected void BindGrid()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from productslist where catname='RAMRAJ' order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                Gridproducts.DataSource = ds;
                Gridproducts.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                Gridproducts.DataSource = ds;
                Gridproducts.DataBind();
                int columncount = Gridproducts.Rows[0].Cells.Count;
                Gridproducts.Rows[0].Cells.Clear();
                Gridproducts.Rows[0].Cells.Add(new TableCell());
                Gridproducts.Rows[0].Cells[0].ColumnSpan = columncount;
                Gridproducts.Rows[0].Cells[0].Text = "No Records Found";
            }

        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Delete from productslist where id='" + id + "'", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
            connstr.Close();
            BindGrid();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Button btnsubmit = sender as Button;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lbl.Text = Gridproducts.DataKeys[gRow.RowIndex].Value.ToString();

           
            this.ModalPopupExtender2.Show();


        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            string serverpath = string.Empty;
            if (Imgprev.HasFile)
            {
                string lid = lbl.Text;
               
                String Update = "update  [productslist] set image1=@IMG  where Id='" + lid + "'";
                SqlCommand comm = new SqlCommand(Update, conn);
                int img1 = Imgprev.PostedFile.ContentLength;
                byte[] msdata1 = new byte[img1];
                string b64 = Convert.ToBase64String(msdata1);
                Imgprev.PostedFile.InputStream.Read(msdata1, 0, img1);
                comm.Parameters.AddWithValue("@IMG", msdata1);
                conn.Open();
                comm.ExecuteNonQuery();
                //comm1.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("imgconvertion.aspx");
            }
            
        }
    }
}