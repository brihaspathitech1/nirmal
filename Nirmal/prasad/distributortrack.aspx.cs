﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nirmal.DataAccessLayer;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;

namespace Nirmal.prasad
{
    public partial class distributortrack : System.Web.UI.Page
    {
        string constr = "Data Source=103.233.79.22;Initial Catalog=Nirmal;User ID=Nirmal;Password=Brihaspathi@123";
        protected void Page_Load(object sender, EventArgs e)

        {
            if (Request.HttpMethod == "POST")
            {
                try
                {
                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {
                        case "checkin_state":
                            state();
                            break;
                        case "check_in":
                            checkin();
                            break;
                        case "check_out":
                            checkout();
                            break;
                        case "coordinate":
                            co_ordinates();
                            break;
                        case "list_coordinates":
                            list_coordinates();
                            break;
                        case "admin_checkin":
                            admin_checkin();
                            break;
                        case "list_chechin":
                            list_checkin();
                            break;


                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
            }
        }
        protected void state()
        {
            string value = string.Empty;
            string dist_id = Request.Params["distributorid"].ToString();
            DataSet ds = DataQueries.SelectCommon("Select * from tblcheckin where (status=1 or status=0) and convert(date,in_time)=convert(date,getdate()) and distributor_id='" + dist_id+ "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                value = ds.Tables[0].Rows[0]["status"].ToString();
            }
            if (value == "")
            {
                Response.Write("{\"error\": false,\"state\":" + 0 + "}");
            }
            else if (value == "1")
            {
                Response.Write("{\"error\": false,\"state\":" + 1 + "}");
            }
            else if (value == "0")
            {
                Response.Write("{\"error\": false,\"state\":" + 2 + "}");
            }
        }

        protected void checkin()
        {
          string in_lat = Request.Params["in_latitude"].ToString();
          string in_lon = Request.Params["in_longitude"].ToString();
          string dist_id = Request.Params["distributorid"].ToString();
          string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
          string in_time = dt.ToString();
                   

          DataSet ds = DataQueries.SelectCommon("select * from tblcheckin where distributor_id='" + dist_id + "' and convert(date,in_time)=convert(date,getdate())");

          if (ds.Tables[0].Rows.Count > 0)
          {
                        Response.Write("{\"error\": true,\"message\":\" You have Checkedin\"}");
          }
          else
          {
                        DataQueries.InsertCommon("insert into [tblcheckin](in_latitude,in_longitude,in_time,distributor_id,Status) values('" + in_lat + "','" + in_lon + "','" + in_time + "','" + dist_id + "','1')");
                        Response.Write("{\"error\": false,\"message\":\"Checkin Successsfully\"}");
          }
        }
        protected void checkout()
        {
            string out_lat = Request.Params["out_latitude"].ToString();
            string out_long = Request.Params["out_longitude"].ToString();
            string dist_id = Request.Params["distributorid"].ToString();
            string dt = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string out_time = dt.ToString();

            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("Update tblcheckin set out_latitude='" + out_lat + "',out_longitude='" + out_long + "',out_time='" + out_time + "',status='0' where distributor_id='" + dist_id + "' and convert(date,in_time)=convert(date,getdate()) and status='1'", conn);
            conn.Open();
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            if (i == 1)
            {
                Response.Write("{\"error\": false,\"message\":\"Checkout Successsfully\"}");
            }
            else
            {
                Response.Write("{\"error\": true,\"message\":\"Please Check in first\"}");
            }

        }
        
        protected void co_ordinates()
        {

            string data = Request.Params["data"].ToString();
            string did = Request.Params["distributorid"].ToString();


            List<Coordinate> myDeserializedObjList = (List<Coordinate>)Newtonsoft.Json.JsonConvert.DeserializeObject(data, typeof(List<Coordinate>));


            foreach (Coordinate p in myDeserializedObjList)
            {
                float latitude = p.latitude;
                float longitude = p.longitude;
                string time = p.time;
                DataQueries.InsertCommon("insert into tblcoordinates(distributor_id,latitude,longitude,time) values('" + did + "','" + latitude + "','" + longitude + "','" + time + "')");

            }

            Response.Write("{\"error\": false,\"message\":\"Added Successsfully\"}");
        }
        protected void list_coordinates()
        {
            string did = Request.Params["distributorid"].ToString();
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon("select  id,in_latitude,in_longitude,in_time from tblcheckin where distributor_id='" + did + "' and convert(date,in_time)='" + date + "' order by in_time desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        public class Coordinate
        {
            public string time;
            public float latitude;
            public float longitude;
        }
        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }
        protected void admin_checkin()
        {
            string eid = Request.Params["distributorid"].ToString();
            string dt = DateTime.Now.ToString("yyyy-MM-dd");
            
            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = new SqlCommand("update tblcheckin set status='1',out_time='',out_latitude='',out_longitude='' where distributor_id='" + eid + "' and status='0' and convert(date,in_time)='" + dt + "'", conn);
            conn.Open();
            int i = cmd.ExecuteNonQuery();
            conn.Close();
            if (i == 1)
            {
                Response.Write("{\"error\": false,\"message\":\"Updated Successsfully\"}");
            }
            else
            {
                Response.Write("{\"error\": false,\"message\":\"Please Checkin First\"}");
            }
           
        }
        protected void list_checkin()
        {
            string date = Request.Params["date"].ToString();
            DataSet ds = DataQueries.SelectCommon("select c.*,d.name from tblcheckin c inner join distributorlogin d on d.id=c.distributor_id where convert(date,c.in_time)='" + date + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
    }


}







        
    
