﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Nirmal.DataAccessLayer;
using iTextSharp.text.html.simpleparser;


namespace Nirmal.prasad
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind_Distributors();
            }
        }

        protected void bind_Distributors()
        {
            DataSet ds = DataQueries.SelectCommon("select id,name from DistributorLogin where role='distributor'");
            ddlAdmins.DataTextField = "name";
            ddlAdmins.DataValueField = "id";
            ddlAdmins.DataSource = ds;
            ddlAdmins.DataBind();
            ddlAdmins.Items.Insert(0,"-Select Distributor-");
        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            //DataTable dt = new DataTable();
            DataTable dt = new DataTable();
            String strConnString = "Data Source=103.233.79.22;Initial Catalog=Nirmal;User ID=Nirmal;Password=Brihaspathi@123";

            SqlCommand comm1;
            SqlConnection conn1 = new SqlConnection(strConnString);
            SqlDataAdapter sda = new SqlDataAdapter();
            if (ddlType.SelectedItem.Text == "All")
            {
                comm1 = new SqlCommand(" with cte as(select o.id,o.date,o.status,o.amount,(select name from [Nirmal].[DistributorLogin]  where id=o.Distributor_id)as dname from [Nirmal].[tblorder] o  where date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110) and o.Distributor_id='" + ddlAdmins.SelectedValue + "' ) select *, substring(cte.date, 0, 11) as dt from cte ");
            }
          
            else
            {
                comm1 = new SqlCommand("with cte as (select o.id,o.date,o.status,o.amount,(select name from [Nirmal].[DistributorLogin]  where id=o.Distributor_id)as dname from [Nirmal].[tblorder] o  where status='" + ddlType.SelectedItem.Text + "' and date between convert(varchar(10),'" + txtFrom.Text + "',110) and convert(varchar(10),'" + txtTo.Text + "',110) and o.Distributor_id='" + ddlAdmins.SelectedValue + "' ) select *, substring(cte.date, 0, 11) as dt from cte");

            }

            comm1.Connection = conn1;
            div1.Visible = true;

            try
            {
                conn1.Open();
                grdReports.EmptyDataText = "No Records Found";

                grdReports.DataSource = comm1.ExecuteReader();
                grdReports.DataBind();

                calsum();

            }
            catch 
            {
               // throw ex1;
            }
            finally
            {

                conn1.Close();
                conn1.Dispose();

            }

        }
        protected void calsum()
        {
            double NetAmt = 0;

            lblTotal.Visible = true;
            //if (e.Row.RowType == DataControlRowType.DataRow)
            {

                foreach (GridViewRow g1 in grdReports.Rows)
                {

                    NetAmt += Convert.ToDouble(g1.Cells[3].Text);

                }
                this.lblTotal.Text = ddlType.SelectedItem.Text + " " + " orders Value: " + "  " + NetAmt.ToString();


            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition",
            "attachment;filename='" + ddlType.SelectedItem.Text + " " + DateTime.Now + "'.xls");
            //"attachment;filename='" + ddlType.SelectedItem.Text + " " + DateTime.Now + "'.pdf");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            //Response.ContentType = "application/vnd.pdf";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            grdReports.AllowPaging = false;


            //Change the Header Row back to white color
            grdReports.HeaderRow.Style.Add("background-color", "#FFFFFF");



            for (int i = 0; i < grdReports.Rows.Count; i++)
            {
                GridViewRow row = grdReports.Rows[i];

                //Change Color back to white
                row.BackColor = System.Drawing.Color.White;

                //Apply text style to each Row
                row.Attributes.Add("class", "textmode");


            }
            grdReports.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void btnword_Click(object sender, EventArgs e)
        {

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
            "attachment;filename='Reports.doc");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-word ";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdReports.AllowPaging = false;

            grdReports.RenderControl(hw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected void btnpdf_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=OrdersReport.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdReports.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
            grdReports.AllowPaging = true;
            grdReports.DataBind();

        }
    }
}