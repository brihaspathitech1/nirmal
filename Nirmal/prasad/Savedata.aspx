﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Savedata.aspx.cs" Inherits="Nirmal.prasad.Savedata" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .GridViewHeaderStyle th {
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#FileUpload1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    


    <!-- Select2 -->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" />

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #000 !important
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
        function Search_Gridview(strKey) {
            var strData = strKey.value.toLowerCase().split(" ");
            var tblData = document.getElementById("<%=GridLogin.ClientID %>");
            var rowData;
            for (var i = 1; i < tblData.rows.length; i++) {
                rowData = tblData.rows[i].innerHTML;
                var styleDisplay = 'none';
                for (var j = 0; j < strData.length; j++) {
                    if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                        styleDisplay = '';
                    else {
                        styleDisplay = 'none';
                        break;
                    }
                }
                tblData.rows[i].style.display = styleDisplay;
            }
        }
    </script>
    <div class="box">
        <!-- Content Header (Page header) -->
        <div class="box-header">
            <h1>Admin Data </h1>

        </div>
        <!-- Main content -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-md-12">
                              <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                  <b>Enter Data</b>
                            <span class="style1">*</span>
                                    <asp:TextBox ID="txtmessage" TextMode="MultiLine" MaxLength="10" Height="100px" Width="590px" class="form-control" runat="server"></asp:TextBox>


                                </div>
                            </div>
                                     <div class="col-sm-3" style="padding-left:150px;padding-top:50px">
                            <div class="form-group">
                                <asp:Button ID="btsave" runat="server" Text="save" class="btn btn-success btn-block" OnClick="btsave_Click" />
                            </div>
                        </div>
                        </div>
                                </div>


                     
                           
                    
                                
                         
                              

                     
                         
                               
                               

                                   
                     
                              
                           
                       
                            <div class="row">
                        
                        <div class="col-md-3" style="padding-left: 10px;">
                            <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
                        </div>
                                </div>
                        <asp:Label ID="lb" runat="server" Visible="false"></asp:Label>
                        <div class="col-md-12">
                            <div class="table table-responsive" style="overflow: scroll; height: 420px">
                                <asp:GridView ID="GridLogin" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" Font-Bold="true">
                                    <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="SNO" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Data" HeaderText="Data" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                        

                                     
                                        <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Button ID="btndelete" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>' OnClick="btndelete_Click" CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

              
                        <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

