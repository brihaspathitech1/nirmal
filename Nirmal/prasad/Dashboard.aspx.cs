﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using Nirmal.DataAccessLayer;

namespace Nirmal.prasad
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Distributor_count();
                pendingorders();
                dealers_count();
                merchants_count();
                customers_count();
            }
            if (!IsPostBack)
            {
                categories_count();
            }
            if (!IsPostBack)
            {
                products();
            }
            if (!IsPostBack)
            {
                orders();
               // shwdt();
            }
           
          
        }

        protected void Distributor_count()
        {

            DataSet ds = DataQueries.SelectCommon("select count(*)as d_count from DistributorLogin where role='distributor' and status=1");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lbldistributor.Text = ds.Tables[0].Rows[0]["d_count"].ToString();
            }
           

        }

        protected void categories_count()
        {
            DataSet ds = DataQueries.SelectCommon("select count(*) as count from Category ");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblcategory.Text = ds.Tables[0].Rows[0]["count"].ToString();
            }
           
        }

        protected void products()
        {
            DataSet ds = DataQueries.SelectCommon("select count(*) as count from Addproduct");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblproduc.Text = ds.Tables[0].Rows[0]["count"].ToString();
            }
           
        }
        protected void orders()
        {
            DataSet ds = DataQueries.SelectCommon("select count(*)as d_count from tblorder");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblorders.Text = ds.Tables[0].Rows[0]["d_count"].ToString();
            }
           


        }
        protected void pendingorders()
        {
            DataSet ds = DataQueries.SelectCommon("select count(*)as d_count from tblorder where status='Pending'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblpendingorders.Text = ds.Tables[0].Rows[0]["d_count"].ToString();
            }



        }
        protected void dealers_count()
        {

            DataSet ds = DataQueries.SelectCommon("select count(*)as d_count from DistributorLogin where role='dealer' and status=1");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lbldealers.Text = ds.Tables[0].Rows[0]["d_count"].ToString();
            }


        }
        protected void merchants_count()
        {

            DataSet ds = DataQueries.SelectCommon("select count(*)as d_count from DistributorLogin where role='merchant' and status=1");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblmerchants.Text = ds.Tables[0].Rows[0]["d_count"].ToString();
            }


        }
        protected void customers_count()
        {

            DataSet ds = DataQueries.SelectCommon("select  count(distinct(mobile)) as custcount from customer");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblcustomers.Text = ds.Tables[0].Rows[0]["custcount"].ToString();
            }


        }



        private void shwdt()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String query = " select d.name,count(o.Distributor_id) as count from DistributorLogin d inner join tblorder o on d.id=o.Distributor_id where o.status='Approved' group by name,Distributor_id ";
            //String query = "Select Brand,Amount from Inward  ";
            SqlCommand cmd = new SqlCommand(query, conn);
            DataTable tb = new DataTable();
            try
            {
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                tb.Load(dr, LoadOption.OverwriteChanges);
                conn.Close();
            }
            catch { }

            if (tb != null)
            {
                String chart = "";
                // You can change your chart height by modify height value
                chart = "<canvas id=\"line-chartt\" width =\"100%\" height=\"40\"></canvas>";
                chart += "<script>";
                chart += "new Chart(document.getElementById(\"line-chartt\"),    { type: 'line', data:  { labels: [";

                // more details in x-axis
                //for (int i = 0; i < 5; i++)
                //    chart += i.ToString() + ",";
                //chart = chart.Substring(0, chart.Length - 1);

                //chart += "],datasets: [{ data: [";




                for (int i = 0; i < tb.Rows.Count; i++)
                    chart += "'" + tb.Rows[i]["name"].ToString() + "'" + ",";
                chart = chart.Substring(0, chart.Length -1);

                chart += "],datasets: [{ data: [";


                // put data from database to chart
                String value = "";
                for (int i = 0; i < tb.Rows.Count; i++)
                    value += tb.Rows[i]["count"].ToString() + ",";
                value = value.Substring(0, value.Length -1);

                chart += value;

                chart += "],label: \"Orders\", borderColor: \"#3e95cd\",fill: true}"; // Chart color
                chart += "]},options: { title: { display: true,text:  'Orders'}      }  "; // Chart title
                chart += "});";
                chart += "</script>";

              //  ltch.Text = chart;
            }
        }



      


    }
}
