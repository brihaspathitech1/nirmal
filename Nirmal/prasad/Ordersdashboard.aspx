﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Ordersdashboard.aspx.cs" Inherits="Nirmal.prasad.Ordersdashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container-fluid">
        <div class="box box-header">
            <h4 style="color: brown; font-weight: bold; margin-left: 40%">
                <strong>Distributor Performance</strong></h4>
        </div>
        <div class="box box-body">
            <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();">
    <asp:DataList ID="DataList1" runat="server" RepeatColumns="5000">

        <ItemTemplate>
            <div class="col-md-12">
                <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom: 55px">
                    <div class="content-panel pn" style="height:175px !important; width: 200px !important;">
                        <div id="spotify ">
                           
                        <asp:Image ID="Image1" class="img-circle" Width="150px" Height="150px" ImageUrl='<%#  Eval("image")%>'
                            runat="server" />
                            <div class="sp-title" style="padding-top: 1px">
                                <h4 style="color:#2b2a2a; font-weight: bold; margin-top: 5px; margin-bottom: 3px;">
                                <strong> <asp:Label ID="lblBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "name") %>'></asp:Label></strong></h4>
                                <h4 style="color: #2b2a2a; font-weight: bold; margin-top: 5px; margin-bottom: 3px;">
                                    <strong>
                                        <asp:LinkButton runat="server" BackColor="#ccffff" ID="btn1" OnClick="btn1_Click" CommandArgument='<%#Eval("name") %>' style="border-radius:8px" Width="50" CssClass="form-control" Text='<%# DataBinder.Eval(Container.DataItem, "count") %>'></asp:LinkButton>
                                    </strong></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </ItemTemplate>

    </asp:DataList>
         </marquee>
        </div>
        <div class="box box-body">
            <h4 style="color: brown; font-weight: bold; margin-left: 40%">
                <strong>Within 24 hrs</strong></h4>
            <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();">
    <asp:DataList ID="DataList2" runat="server" RepeatColumns="5000">

        <ItemTemplate>
            <div class="col-md-12">
                <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom: 55px">
                    <div class="content-panel pn" style="height: 175px !important; width: 200px !important;">
                        <div id="spotify ">
                           <asp:Image ID="Image1" class="img-circle" Width="150px" Height="150px" ImageUrl='<%#  Eval("image")%>'
                            runat="server" />
                           
                            <div class="sp-title" style="padding-top: 1px">
                               <h4 style="color:#2b2a2a; font-weight: bold; margin-top: 5px; margin-bottom: 3px;">
                                <strong> <asp:Label ID="lblBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "name") %>'></asp:Label></strong></h4>
                                <h4 style="color: #2b2a2a; font-weight: bold; margin-top: 5px; margin-bottom: 3px;">
                                    <strong>
                                        <asp:LinkButton runat="server" BackColor="#ccffff" ID="btn2" OnClick="btn2_Click" CommandArgument='<%#Eval("name") %>' style="border-radius:8px" Width="50" CssClass="form-control" Text='<%# DataBinder.Eval(Container.DataItem, "count") %>'></asp:LinkButton>
                                    </strong></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </ItemTemplate>
        
    </asp:DataList>
         </marquee>
        </div>
        <div class="box box-body">
            <h4 style="color: brown; font-weight: bold; margin-left: 40%">
                <strong>Within 48 hrs</strong></h4>
            <marquee class="animated slideInUp" onmouseover="this.stop();" onmouseout="this.start();">
    <asp:DataList ID="DataList3" runat="server" RepeatColumns="5000">

        <ItemTemplate>
            <div class="col-md-12">
                <div class="col-lg-6 col-md-6   mb text-center" style="margin-bottom: 55px">
                    <div class="content-panel pn" style="height:175px !important; width: 200px !important;">
                        <div id="spotify ">
                            <asp:Image ID="Image1" class="img-circle" Width="150px" Height="150px" ImageUrl='<%#  Eval("image")%>'
                            runat="server" />
                           
                            <div class="sp-title" style="padding-top: 1px">
                               <h4 style="color:#2b2a2a; font-weight: bold; margin-top: 5px; margin-bottom: 3px;">
                                <strong> <asp:Label ID="lblBrandName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "name") %>'></asp:Label></strong></h4>
                                <h4 style="color: #2b2a2a; font-weight: bold; margin-top: 5px; margin-bottom: 3px;">
                                    <strong>
                                        <asp:LinkButton runat="server" BackColor="#ccffff" ID="btn3" OnClick="btn3_Click" CommandArgument='<%#Eval("name") %>' style="border-radius:8px" Width="50" CssClass="form-control" Text='<%# DataBinder.Eval(Container.DataItem, "count") %>'></asp:LinkButton>
                                    </strong></h4> </div>
                        </div>
                    </div>
                </div>

            </div>
        </ItemTemplate>

    </asp:DataList>
         </marquee>
        </div>
  <cc1:ModalPopupExtender runat="server" ID="modelpopup1" CancelControlID="btncancel" TargetControlID="h1" PopupControlID="panel1"></cc1:ModalPopupExtender>
     <asp:Button ID="h1" runat="server" style="display:none" />
    <asp:Panel runat="server" ID="panel1" CssClass="animated bounceIn" Style="border: solid 1px; background-color: white" Width="700px" Height="600px">
  
            <div class="col-md-12">
                <div class="row">
                 <div class="col-md-4">
                 <b><asp:Label runat="server" ID="lblname" ForeColor="Blue" Font-Size="18"></asp:Label></b>
                </div>
                <div class="col-md-1 col-md-offset-7">
                    <asp:ImageButton ImageUrl="https://cdn4.iconfinder.com/data/icons/media-controls-4/100/close-512.png" runat="server" ID="btncancel" Height="30" Width="30" />
                </div>
                    </div>
            </div>
            <div class="col-md-7" runat="server">
                  <br />
                <div class="box-body table-responsive no-padding" style="overflow: scroll; height: 510px;width:680px">
                    <asp:GridView ID="GridView2" class="table table-bordered table-striped table-hover" runat="server" AutoGenerateColumns="False">
                      
                        <Columns>
                            <asp:BoundField DataField="sno" HeaderText="S.No" HeaderStyle-BackColor="#EEFF99" />
                            <asp:BoundField DataField="orderid" HeaderText="Order Id" HeaderStyle-BackColor="#EEFF99"  />
                            <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#EEFF99" />
                            <asp:BoundField DataField="address" HeaderText="Address" HeaderStyle-BackColor="#EEFF99" />
                            <asp:BoundField DataField="orderamount" HeaderText="amount" HeaderStyle-BackColor="#EEFF99" />

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
         
    </asp:Panel>
    </div>
</asp:Content>
