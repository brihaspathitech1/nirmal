﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="Nirmal.prasad.Orders" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
         .cust{   text-align: right; }
   </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script language="javascript" type="text/javascript">
   function CallPrint(strid) {
       var prtContent = document.getElementById(strid);
       var WinPrint = window.open('', '', 'width=1000,height=800');
       WinPrint.document.write(prtContent.innerHTML);
       WinPrint.document.close();
       WinPrint.focus();
       WinPrint.print();
       WinPrint.close();
       prtContent.innerHTML = strOldOne;
   }
</script>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdProduct_Details.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>

     <div class="row">
         <div class="box-header with-border">
          <h3 class="primery">&nbsp;&nbsp;<b>Orders List</b></h3>
             </div>
        <div class="col-md-12">
            <div class="box box-success">
            
          <div class="box">
              
            <div class="box-header  with-border height-border">
             

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
               <div class="box-body">
              <div class="row">
                 
                  <div class="col-md-3">
                <div class="form-group">
                  <label>Start Date</label><span class="style1">*</span>
            
             
                    <asp:TextBox ID="TAN"   class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
          <cc1:CalendarExtender ID="CalendarExtender1" runat="server"  TargetControlID="TAN" Enabled="True" Format="MM/dd/yyyy"/>
    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TAN"
                              ErrorMessage="Enter Start Date" Style="color: #FF0000"></asp:RequiredFieldValidator>--%>
                     
                </div>
                </div>
                   

                  <div class="col-md-3">
                <div class="form-group">
                  <label>End Date</label><span class="style1">*</span>
            
             
                 <asp:TextBox ID="TextBox1"   class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
    <cc1:CalendarExtender ID="CalendarExtender2" runat="server"  TargetControlID="TextBox1" Enabled="True" Format="MM/dd/yyyy"/>
    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                              ErrorMessage="Enter Start Date" Style="color: #FF0000"></asp:RequiredFieldValidator>--%>
                </div>
                </div>
                  <div class="col-md-2" style=" padding-top:19px">
                    <asp:Button ID="Button2" class="btn btn-success btn-block" runat="server" Text="Search" OnClick="Button2_Click"/>
       </div>

              </div>
              </div>
              </div>
              <!-- /.row -->
            </div>
           
              <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
            <br />
            <!-- /.box-header -->
           
            <div class="box-body">
                  <div class="box box-success">
               
               <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
                     <%-- <div class="col-md-12">
                      <div class="col-md-4">
                            <h3>
                                Total Profit Amount:<asp:Label ID="lblTotal" runat="server" Text="0" ForeColor="Blue"></asp:Label></h3>
                        </div>
                          <div class="col-md-4">
                            <h3>
                                Bill Amount:<asp:Label ID="lblbillamount" runat="server" Text="0" ForeColor="Blue"></asp:Label></h3>
                        </div>
                            <div class="col-md-4">
                            <h3>
                              Pending Amount:<asp:Label ID="lblpending" runat="server" Text="0" ForeColor="Blue"></asp:Label></h3>
                        </div>
                          </div>--%>
                   
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="grdProduct_Details" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped" >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
                              
                 <asp:BoundField DataField="id" HeaderText="OrderId" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="dt" HeaderText="Order Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="approvedate" HeaderText="Approve Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="name" HeaderText="Distributor Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               
               <asp:BoundField DataField="address" HeaderText="Distributor Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="amount" HeaderText="Order Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="actualamount" HeaderText="Admin Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="mobile" HeaderText="Distributor Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="loc" HeaderText="Order Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                <asp:TemplateField HeaderText="Action" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Button ID="btnitems" runat="server" CssClass="btn btn-success"  Text="Items" OnClick="btnitems_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Details" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Button ID="btndetails" runat="server" CssClass="btn btn-success"  Text="Details"  CausesValidation="false" OnClick="btndetails_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
           
               
                       
           </Columns>
       </asp:GridView>
           </div>
                 <asp:Label ID="Label6" runat="server" Text="" Visible="false"></asp:Label>

       <asp:Button ID="Button1" runat="server" Style="display: none" />
                <div id="div22">
       <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="Button1" PopupControlID="Panel1" CancelControlID="Cancel"
            BackgroundCssClass="tableBackground">
       </cc1:ModalPopupExtender>
        <asp:Panel ID="Panel1" runat="server" BackColor="White" Style="display: none; border: 1px solid #ceceec; padding-bottom: 20px; padding:50px">
       
               <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
             <asp:Label ID="lblUser" runat="server" Visible="false"></asp:Label>


           <table style="width:450px">
               <tr>
                   <td><b>Distributor Details:</b></td>
                   <td ><b>Customer Details:</b></td>
               </tr>
               <tr>
                   <td><asp:Label ID="lbname" runat="server" >Date:&nbsp;</asp:Label><asp:Label ID="lblpname" runat="server"></asp:Label><br /></td>
                   <td ><asp:Label ID="lblcname" runat="server" >Customer Name:&nbsp;</asp:Label><asp:Label ID="lblcustname" runat="server"></asp:Label><br /></td>
               </tr>
               <tr>
                   <td><asp:Label ID="lbunits" runat="server">Name :&nbsp;</asp:Label><label></label><asp:Label ID="lblunits" runat="server"></asp:Label><br /></td>
                   <td ><asp:Label ID="lblcmobile" runat="server">Customer Mobile:&nbsp;</asp:Label><label></label><asp:Label ID="lblcustmobile" runat="server"></asp:Label><br /></td>
                   
               </tr>
               <tr>
                   <td><asp:Label ID="mobi" runat="server">Mobile :&nbsp;</asp:Label><asp:Label ID="moble" runat="server"></asp:Label><br /></td>
                   <td ><asp:Label ID="lblcaddress" runat="server">Customer Address:&nbsp;</asp:Label><asp:Label ID="lblcustaddress" runat="server"></asp:Label><br /></td>
                   
               </tr>
               <tr>
                   <td><asp:Label ID="lbprice" runat="server">Status :&nbsp;</asp:Label><asp:Label ID="lblprice" runat="server"></asp:Label><br /></td>
                   <td ><asp:Label ID="lbpincode" runat="server">Pincode:&nbsp;</asp:Label><asp:Label ID="lblpincode" runat="server"></asp:Label><br /></td>
                   

               </tr>
               <tr>
                   <td></td>
                   <td ><asp:Label ID="lblorder" runat="server">OrderId:&nbsp;</asp:Label><asp:Label ID="lblorderid" runat="server"></asp:Label><br /></td>
                   

               </tr>
           </table>
           <br />
           <h3 class="box-title" style="text-align:center">Ordered Items List</h3>
                            
          
          
      <div  style="width:900px; height: 300px; overflow: scroll">      
     <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found" class="table table-responsive table-striped" >
          <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
               <asp:BoundField DataField="productname" HeaderText="ProductName" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  />
               <asp:BoundField DataField="shopname" HeaderText="Shop Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  />
              <asp:BoundField DataField="units" HeaderText="Units" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  />
              <asp:BoundField DataField="mrp" HeaderText="Price" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="count" HeaderText="Item Count" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="totalcost" HeaderText="Total Cost" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
              <asp:BoundField DataField="dealamount" HeaderText="Dealer Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
              <asp:BoundField DataField="distamt" HeaderText="Dist Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
             
               
           </Columns>

       </asp:GridView>
          <div class="col-md-12">
          <div class="row">
         
          <div class="col-md-6" style="margin-top:15px"> <h3><asp:Label ID="lbcount" runat="server"><b>Total Amount :</b>&nbsp;</asp:Label><asp:Label ID="lblcount" runat="server"></asp:Label></h3></div><br />
          <div class="col-md-6"> <h3><asp:Label ID="lbldisttext" runat="server"><b>Distributor Amount :</b>&nbsp;</asp:Label><asp:Label ID="lbldistamount" runat="server"></asp:Label></h3></div></div></div><br />
          <div class="col-md-12">
          <div class="row">
          <div class="col-md-6"> <h3><asp:Label ID="lbldealtext" runat="server"><b>Dealer Amount :</b>&nbsp;</asp:Label><asp:Label ID="lbldealamount" runat="server"></asp:Label></h3></div>
          <div class="col-md-6"> <h3><asp:Label ID="lbladmintext" runat="server"><b>Admin Amount :</b>&nbsp;</asp:Label><asp:Label ID="lbladminprofit" runat="server"></asp:Label></h3></div>
              </div>
              </div>
         </div>

                   <div class="col-md-4" style="padding-top:10px">
                        <asp:Button ID="Cancel" runat="server" Text="Cancel"  class="btn btn-danger btn-block" />
                       </div>
            <div class="col-md-4" style="padding-top: 10px">
                <div class="form-group">
                                    <asp:Button ID="pdf_click" runat="server" class="btn btn-success"
                                        Text="Export PDF" OnClick="pdf_click_Click"   />
                                </div>
                                
                            </div>
            <div class="col-md-5" style="padding-top: 16px">
                <div class="form-group">
                                    <%--<input type="button" id="btnprint" onclick="window.print();" value="Print Page" class=" text-center btn btn-success" />--%>
                    <asp:button id="BtnPrint" runat="server" text="Print" onclientclick="javascript:CallPrint('div22');" xmlns:asp="#unknown" visible="false" />
                                </div>
                                
                            </div>
            

        </asp:Panel>

                    </div>
                      <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label>

       <asp:Button ID="Button3" runat="server" Style="display: none" />
                <div id="div22">
       <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button3" PopupControlID="Panel2" CancelControlID="Cancel"
            BackgroundCssClass="tableBackground">
       </cc1:ModalPopupExtender>
        <asp:Panel ID="Panel2" runat="server" BackColor="White" Style="display: none; border: 1px solid #ceceec; padding-bottom: 20px; padding:50px">
       
               <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
             

         
           <br />
           <h3 class="box-title" style="text-align:center">Profit Details</h3>
                            
          
          
      <div  style="width:500px; height: 300px; overflow: scroll">      
     <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false" EmptyDataText="No Records Found" class="table table-responsive table-striped" >
          <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
               <asp:BoundField DataField="actualamount" HeaderText="Merchant Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  />
               <asp:BoundField DataField="amount" HeaderText="Order Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  />
              <asp:BoundField DataField="adminprof" HeaderText="Order Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"  />
             <%-- <asp:BoundField DataField="travelcharges" HeaderText="Travel Charges" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="updatedamount" HeaderText="Remaining Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               --%>
               
           </Columns>

       </asp:GridView>

         </div>
            
             <div class="col-md-4" style="padding-top:10px">
                        <asp:Button ID="Button4" runat="server" Text="Cancel"  class="btn btn-danger btn-block" />
                       </div>
            

        </asp:Panel>
                </div></div></div></div>
</asp:Content>