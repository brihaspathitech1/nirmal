﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="MerchantOrders.aspx.cs" Inherits="Nirmal.prasad.MerchantOrders" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
   
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Merchant Profits</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
               
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">



                                
                                    <div class="col-md-3">
                                        <label>Merchant Name</label>
                                        <asp:DropDownList ID="ddlmerchant" class="form-control" runat="server" OnSelectedIndexChanged="ddlmerchant_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                              <div class="col-md-6">
                            <h3 style="padding-top:10px"><b><asp:Label ID="lbltxtmname" runat="server"></asp:Label></b><asp:Label ID="lblmshopname" runat="server"> Shopname:&nbsp;</asp:Label><b><u><asp:Label ID="lblmerchantshopname" runat="server"></asp:Label></u></b></h3>

                                
                                </div>
                             <div class="col-md-3">
                            <h3 style="padding-top:10px"><asp:Label ID="lbltxtvillage" runat="server"> Village:&nbsp;</asp:Label><b><u><asp:Label ID="lblvillage" runat="server"></asp:Label></u></b></h3>

                                
                                </div>



                                    </div>
                               
                              </div>
                                <br />
                               
                           
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                                        
                                        <asp:Button ID="todayprofit" class="btn btn-danger" runat="server" Text="Today Profits" OnClientClick=" return validate()" OnClick="todayprofit_Click" />
                                        

                                    </div>
                        <div class="col-md-2">
                                        
                                        <asp:Button ID="totalprofit" class="btn btn-success" runat="server" Text="Total Profits" OnClientClick=" return validate()" OnClick="totalprofit_Click" />
                                        

                                    </div>

                    </div>
            <div class="row">
                  <div class="col-md-12">
                        <div class="col-md-6">
            <h3><b><asp:Label ID="lbldistname" runat="server"></asp:Label><b><asp:Label ID="lblprofittext" runat="server"> Profit Value:</asp:Label></b><asp:Label ID="lblprofit" runat="server"></asp:Label></h3>
                            </div>
                       <div class="col-md-6">
               <h3><b><asp:Label ID="lbltxtorderscount" runat="server">Orders Count:</asp:Label></b><asp:Label ID="lblordercount" runat="server"></asp:Label></h3>
                </div>
                      </div>
                </div>

                    <div class="col-md-12">
                        <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="grdcustdetails" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive table-striped">
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:BoundField DataField="sno" HeaderText="SNO" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />                           
                                    <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />                           
                                    <%--<asp:BoundField DataField="mname" HeaderText="Merchant Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" Visible="false" />--%>
                                    <%--<asp:BoundField DataField="loc" HeaderText="Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />--%>
                                    <asp:BoundField DataField="actualamount" HeaderText="Actual Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="adminprof" HeaderText="Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="travelcharges" HeaderText="Travel Charges" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="updatedamount" HeaderText="Final Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="name" HeaderText="Merchant Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="address" HeaderText="Merchant Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    
                                </Columns>
                            </asp:GridView>
                           <%-- <div id="div1" runat="server" visible="false">
                                <div class="col-md-5" style="padding-top: 16px">
                                    <div class="form-group">
                                        <asp:Button ID="btnword" runat="server" class="btn btn-success"
                                            Text="Export Ms-Word" BackColor="#33ffe3" ForeColor="#000000" />
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-top: 16px">
                                    <div class="form-group">
                                        <asp:Button ID="btnpdf" runat="server" class="btn btn-danger"
                                            Text="Export PDF" BackColor="Orange" ForeColor="#000000" />
                                    </div>
                                </div>
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
