﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Reports.aspx.cs" Inherits="Nirmal.prasad.Reports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        function validate() {
            if (document.getElementById("<%=ddlType.ClientID%>").value == "") {
                alert("Please Select Type");
                document.getElementById("<%=ddlType.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%=txtFrom.ClientID%>").value == "") {
                alert("Please Select From Date");
                document.getElementById("<%=txtFrom.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=txtTo.ClientID%>").value == "") {
                alert("Please select Select To Date");
                //document.getElementById("<%=txtTo.ClientID%>").focus();
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Reports</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">



                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Distributor Name</label>
                                        <asp:DropDownList ID="ddlAdmins" class="form-control" runat="server">
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <label>Type</label>
                                        <asp:DropDownList ID="ddlType" class="form-control" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>Approved</asp:ListItem>
                                            <asp:ListItem>Pending</asp:ListItem>
                                            <asp:ListItem>Rejected</asp:ListItem>

                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <label>From</label>
                                        <asp:TextBox ID="txtFrom" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server"
                                             TargetControlID="txtFrom" Format="yyyy-MM-dd" />


                                    </div>

                                    <div class="col-md-3">
                                        <label>To</label>
                                        <asp:TextBox ID="txtTo" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        <cc2:CalendarExtender ID="CalendarExtender1" runat="server"
                                             TargetControlID="txtTo" Format="yyyy-MM-dd" />

                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3">
                                        <asp:Button ID="btnsave" class="btn btn-success btn-block" runat="server" Text="Submit" OnClientClick=" return validate()" OnClick="btnsave_Click" />
                                    </div>
                                    <br />
                                    <div>
                                        <asp:Label ID="lblmgs" runat="server" ForeColor="Blue" Width="20px"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <h3>
                                <asp:Label ID="lblTotal" runat="server" Visible="false" ForeColor="Blue"></asp:Label></h3>
                        </div>
                        <div class="col-md-5" style="padding-top: 18px">
                            <asp:Button ID="btnExcel" runat="server" Width="25%" Visible="false" OnClick="btnExcel_Click" Text="Export Excel" class="btn btn-success btn-sm btn-block" />

                        </div>
                    </div>

                    <div class="col-md-12">
                        <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="grdReports" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive table-striped">
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="OrderId" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" Visible="false" />
                                    <asp:BoundField DataField="dt" HeaderText="OrderDate" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="status" HeaderText="Status" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="dname" HeaderText="Distributor Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                    
                                </Columns>
                            </asp:GridView>
                            <div id="div1" runat="server" visible="false">
                                <div class="col-md-5" style="padding-top: 16px">
                                    <div class="form-group">
                                        <asp:Button ID="btnword" runat="server" class="btn btn-success"
                                            Text="Export Ms-Word" OnClick="btnword_Click" BackColor="#33ffe3" ForeColor="#000000" />
                                    </div>
                                </div>
                                <div class="col-md-5" style="padding-top: 16px">
                                    <div class="form-group">
                                        <asp:Button ID="btnpdf" runat="server" class="btn btn-danger"
                                            Text="Export PDF" OnClick="btnpdf_Click" BackColor="Orange" ForeColor="#000000" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
