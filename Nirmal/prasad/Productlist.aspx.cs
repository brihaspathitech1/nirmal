﻿
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls; 

namespace Nirmal.prasad
{
    public partial class Productlist : System.Web.UI.Page
    {
        double total;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind1();
            }
            
        }

        protected void bind1()
        {

            SqlConnection conw = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            //SqlCommand cmd = new SqlCommand("select *,cast(a.mrp as float)-cast(a.price as float)-cast(a.distamount as float)-cast(a.dealamount as float) as amt,(d.name) as mname,(d.address) as maddress,(d.mobile) as mmobile from Addproduct a inner join DistributorLogin d on a.shopname=d.shopname where a.status='1' and d.role='merchant' order by a.id desc", conw);
            SqlCommand cmd = new SqlCommand("select distinct a.id,a.image,a.productname,a.units,a.price,a.mrp,a.shopname,a.distamount,a.dealamount,cast(a.mrp as float)-cast(a.price as float)-cast(a.distamount as float)-cast(a.dealamount as float) as amt,a.dealerlocation,(d.name) as mname,(d.address) as maddress,(d.mobile) as mmobile from Addproduct a inner join DistributorLogin d on a.shopname=d.shopname where a.status='1' and d.role='merchant' order by a.id desc", conw);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conw.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdProduct_Details.DataSource = ds;

                grdProduct_Details.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";

            }
       
            
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string serverpath = string.Empty;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
                SqlCommand cmd = new SqlCommand("update Addproduct set price='" + price.Text + "',mrp='" + mrp.Text + "',distamount='"+txtdistamount.Text+"',dealamount='"+txtdealamount.Text+"' where id='" + lblstor_id.Text + "'", conn44);
                cmd.ExecuteNonQuery();
                conn44.Close();
                bind1();
            }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblstor_id.Text = grdProduct_Details.DataKeys[gRow.RowIndex].Value.ToString();
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select substring(expirydate,0,11) as expirydate from Addproduct where id='"+lblstor_id.Text+"'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
               // txtexpiry.Text = ds.Tables[0].Rows[0]["expirydate"].ToString();
                
            }
            
            price.Text = gRow.Cells[5].Text.Replace("&nbsp;", "");
            mrp.Text = gRow.Cells[6].Text.Replace("&nbsp;", "");
            txtdistamount.Text = gRow.Cells[12].Text.Replace("&nbsp;", "");
            txtdealamount.Text = gRow.Cells[14].Text.Replace("&nbsp;", "");
            // avail.Text = gRow.Cells[8].Text.Replace("&nbsp;", "");
            //txtpmaintenance.Text = gRow.Cells[9].Text.Replace("&nbsp;", "");
            //txtexpiry.Text = gRow.Cells[7].Text.Replace("&nbsp;", "");
            // qnt.Text = "0";


            // txtstate7.Text = gRow.Cells[15].Text;
            this.ModalPopupExtender2.Show();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblId.Text = grdProduct_Details.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("select productname from Addproduct where Id='" + lblId.Text + "'");
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Delete = "delete from [Addproduct]  where Id='" + lblId.Text + "'";
            SqlCommand comm = new SqlCommand(Delete, conn);
            //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            //SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            //comm1.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("Productlist.aspx");

        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }

        protected void dealprice_Click(object sender, EventArgs e)
        {
            grdProduct_Details.Columns[0].Visible = false;
            grdProduct_Details.Columns[1].Visible = false;
            grdProduct_Details.Columns[2].Visible = false;
            grdProduct_Details.Columns[4].Visible = false;
            grdProduct_Details.Columns[5].Visible = false;
            grdProduct_Details.Columns[6].Visible = false;
            grdProduct_Details.Columns[7].Visible = false;
            grdProduct_Details.Columns[8].Visible = false;
            grdProduct_Details.Columns[9].Visible = false;
            grdProduct_Details.Columns[10].Visible = false;
            grdProduct_Details.Columns[11].Visible = false;
            grdProduct_Details.Columns[12].Visible = false;
            grdProduct_Details.Columns[13].Visible = false;
            grdProduct_Details.Columns[14].Visible = false;
     
            grdProduct_Details.Columns[16].Visible = false;
            grdProduct_Details.Columns[17].Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=dealersprice.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            grdProduct_Details.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
            grdProduct_Details.AllowPaging = true;
            grdProduct_Details.DataBind();

        }

        protected void distprice_Click(object sender, EventArgs e)
        {
            grdProduct_Details.Columns[0].Visible = false;
            grdProduct_Details.Columns[1].Visible = false;
            grdProduct_Details.Columns[2].Visible = false;
            grdProduct_Details.Columns[4].Visible = false;
            grdProduct_Details.Columns[5].Visible = false;
            grdProduct_Details.Columns[6].Visible = false;
            grdProduct_Details.Columns[7].Visible = false;
            grdProduct_Details.Columns[8].Visible = false;
            grdProduct_Details.Columns[9].Visible = false;
            grdProduct_Details.Columns[10].Visible = false;
            grdProduct_Details.Columns[11].Visible = false;
            grdProduct_Details.Columns[12].Visible = false;
            grdProduct_Details.Columns[15].Visible = false;
            grdProduct_Details.Columns[14].Visible = false;

            grdProduct_Details.Columns[16].Visible = false;
            grdProduct_Details.Columns[17].Visible = false;
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=distributorsprice.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            grdProduct_Details.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
            grdProduct_Details.AllowPaging = true;
            grdProduct_Details.DataBind();
        }
    }
}

