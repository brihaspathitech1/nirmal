﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Ordersapprove.aspx.cs" Inherits="Nirmal.prasad.Ordersapprove" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grddistributors.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
    <div class="row">
        
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Waiting Orders List</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
             
                <!-- /.box-header -->
               >
                    </div>
                    <div class="col-md-12">
                           <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                      
                    </div>

                    <div class="col-md-12">
                        <div style="width: 100%; height: 400px; overflow: scroll">
                            <asp:GridView ID="grddistributors" runat="server" AutoGenerateColumns="false" CssClass="table table-responsive table-striped">
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="OrderId" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="dt" HeaderText="Order Date" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="name" HeaderText="Distributor Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="address" HeaderText="Distributor Address" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="amount" HeaderText="Amount" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="updatedamount" HeaderText="Admin Profit" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="mobile" HeaderText="Distributor Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="loc" HeaderText="Order Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center"  ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
