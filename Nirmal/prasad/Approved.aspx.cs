﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class Approved : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindgrid();
            }
        }

        SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
        protected void Bindgrid()
        {           
            conn44.Open();
            SqlCommand cmd1 = new SqlCommand("with cte as (select o.id, o.date,d.name,d.address,o.amount,o.status,isnull(o.updatedamount,0)as updatedamount,d.mobile, d.loc, (c.name) as CustomerName from tblorder o inner join DistributorLogin d on o.Distributor_id = d.id inner join customer c on c.id = o.customer_id where o.status = 'Pending')  select *, substring(cte.date, 0, 11) as dt from cte", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void btnapproved_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = GridView1.DataKeys[grow.RowIndex].Value.ToString();
            SqlCommand cmd = new SqlCommand("update tblorder set status = 'Rejected' where id = '" + lblId.Text + "' ", conn44);
            conn44.Open();
            cmd.ExecuteNonQuery();
            conn44.Close();

            Bindgrid();
        }

        protected void approve_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = GridView1.DataKeys[grow.RowIndex].Value.ToString();
            SqlCommand cmd = new SqlCommand("update tblorder set status = 'Approved',status1='1',approvedate='" + DateTime.Now.ToString("yyyy-MM-dd") + "' where id = '" + lblId.Text + "' ", conn44);
            conn44.Open();
            cmd.ExecuteNonQuery();
            conn44.Close();

            Bindgrid();


        }

        protected void approveall_Click(object sender, EventArgs e)
        { 
            SqlCommand cmd = new SqlCommand("update tblorder set status = 'Approved',status1='1',approvedate='"+DateTime.Now.ToString("yyyy-MM-dd")+"' where status='Pending'  ", conn44);
            conn44.Open();
            cmd.ExecuteNonQuery();
            conn44.Close();

            Bindgrid();
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = GridView1.DataKeys[grow.RowIndex].Value.ToString();
            SqlCommand cmd = new SqlCommand("delete tblorder where id = '" + lblId.Text + "' ", conn44);
            SqlCommand cmd1 = new SqlCommand("delete tblorderitem where order_id = '" + lblId.Text + "' ", conn44);
            conn44.Open();
            cmd.ExecuteNonQuery();
            cmd1.ExecuteNonQuery();
            conn44.Close();

            Bindgrid();
        }
    }
}