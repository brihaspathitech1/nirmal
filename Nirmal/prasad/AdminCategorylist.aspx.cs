﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class AdminCategorylist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind();
            }
        }

        protected void bind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Category order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdCategory_Details.DataSource = ds;
                grdCategory_Details.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdCategory_Details.DataSource = ds;
                grdCategory_Details.DataBind();
                int columncount = grdCategory_Details.Rows[0].Cells.Count;
                grdCategory_Details.Rows[0].Cells.Clear();
                grdCategory_Details.Rows[0].Cells.Add(new TableCell());
                grdCategory_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdCategory_Details.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblId.Text = grdCategory_Details.DataKeys[gRow.RowIndex].Value.ToString();

            //dropcomapany.SelectedItem.Text = gRow.Cells[3].Text.Replace("&nbsp;", "");

            txtcatname.Text = gRow.Cells[2].Text.Replace("&nbsp;", "");



            txtdescription.Text = gRow.Cells[3].Text.Replace("&nbsp;", "");
           



            // txtstate7.Text = gRow.Cells[15].Text;
            this.ModalPopupExtender2.Show();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            string serverpath = string.Empty;
            if (Imgprev.HasFile)
            {
                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                Imgprev.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
                String Update = "update  [category] set categoryname='" + txtcatname.Text + "',description='" + txtdescription.Text + "',image='" + serverpath + "'  where Id='" + lblId.Text + "'";
                SqlCommand comm = new SqlCommand(Update, conn);
                conn.Open();
                comm.ExecuteNonQuery();
                //comm1.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("AdminCategorylist.aspx");
            }
            else
            {

                DataSet ds = DataQueries.SelectCommon("select categoryname from Category where Id='" + lblId.Text + "'");

                String Update = "update  [category] set categoryname='" + txtcatname.Text + "',description='" + txtdescription.Text + "' where Id='" + lblId.Text + "'";
                SqlCommand comm = new SqlCommand(Update, conn);
                //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
                //SqlCommand comm1 = new SqlCommand(Update1, conn);
                conn.Open();
                comm.ExecuteNonQuery();
                //comm1.ExecuteNonQuery();
                conn.Close();
                Response.Redirect("AdminCategorylist.aspx");
            }



            //if (FileUploadNews.HasFile)
            //{
            //    string filename = Path.GetFileName(FileUploadNews.PostedFile.FileName);
            //    string path = Server.MapPath("~/images/") + filename;
            //    FileUploadNews.PostedFile.SaveAs(path);
            //    string serverpath = @"http://Aqua.aocfarm.com/images/" + filename;
            //    DataQueries.UpdateCommon("update ictcenters_login set name='" + txtuName.Text + "',mail = '" + txtuEmail.Text + "',password = '" + txtupwd.Text + "',ictcenter = '" + ddluictcenter.SelectedItem.Text + "',ictcenterid = '" + TextIctId.Text + "',mobile = '" + txtUMobile.Text + "',image='" + serverpath + "' where id='" + lblstor_id.Text + "'");
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Updated Successfully')", true);
            //    BindAdmins();
            //}
            //else
            //{
            //    DataQueries.UpdateCommon("update ictcenters_login set name='" + txtuName.Text + "',mail = '" + txtuEmail.Text + "',password = '" + txtupwd.Text + "',ictcenter = '" + ddluictcenter.SelectedItem.Text + "',ictcenterid = '" + TextIctId.Text + "',mobile = '" + txtUMobile.Text + "' where id='" + lblstor_id.Text + "'");
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Updated Successfully')", true);
            //    BindAdmins();
            //}
        }




        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblId.Text = grdCategory_Details.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("select categoryname from Category where Id='" + lblId.Text + "'");
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Delete = "delete from [Category]  where Id='" + lblId.Text + "'";
            SqlCommand comm = new SqlCommand(Delete, conn);
            //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            //SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            //comm1.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("AdminCategorylist.aspx");
        }
    }
}
