﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="productsdisplay.aspx.cs" Inherits="Nirmal.prasad.productsdisplay" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=grdProduct_Details.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
    
     <div class="row">
        <div class="col-md-12">
          <div class="box">
              
            <div class="box-header  with-border height-border">
              <h3 class="box-title"><b>Product List</b></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
                
            </div>
      
            <!-- /.box-header -->
            <div class="box-body">
                 <div class="col-md-12">
                           <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                      
                    </div>
             <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lb" runat="server" Visible="false"></asp:Label>
    <div style="width: 100%; height: 600px; overflow: scroll;">
     <asp:GridView ID="grdProduct_Details" runat="server" AutoGenerateColumns="false" DataKeyNames="Id" class="table table-bordered table-striped"  >
         <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
           <Columns>
              
               <asp:BoundField DataField="id" HeaderText="id" visible="false"/>
               <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  >
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("image")%>'  ControlStyle-Height="100" ControlStyle-Width="100" />
        </ItemTemplate>
    </asp:TemplateField>
                 <asp:BoundField DataField="productname" HeaderText="ProductName" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center"/>
                
                   <asp:BoundField DataField="categoryid" HeaderText="id" visible="false"/>
            <asp:BoundField DataField="units" HeaderText="Units" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="price" HeaderText="Price " HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="mrp" HeaderText="MRP" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="shopname" HeaderText="Shop name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               <asp:BoundField DataField="dealerlocation" HeaderText="Dealer Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center" />
               
              <asp:TemplateField HeaderText="Approve product" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit"   OnClick="lnkEdit_Click" Class="fa fa-pencil" 
                                        runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
               <asp:TemplateField  HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-Height = "50" ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" HeaderStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkdelete" OnClick="lnkdelete_Click" Class="fa fa-trash" 
                                        runat="server" ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                       


           </Columns>
       </asp:GridView>
           </div>
                <asp:Button ID="modelPopup" runat="server" Style="display: none" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="modelPopup"
                        PopupControlID="updatePanel" CancelControlID="btnCancel" BackgroundCssClass="tableBackground">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="updatePanel" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="350px" Width="650px" ForeColor="#ffffff" style="background-color: rgba(0,0,0,0.8)">

                                    <h3 style="text-align:center">Edit Products</h3>
                                <hr />
                       

                                    <asp:Label ID="lblstor_id" runat="server" Visible="false"></asp:Label>
                                
                        
                      <div class="col-md-3">
                            <div class="form-group">
                              <b>Dealer Price</b>
                                
                                    <asp:TextBox ID="dealamount" runat="server" class="form-control" Text="0" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers, Custom" ValidChars="."
    TargetControlID="dealamount" />
                                </div></div>
                             <div class="col-md-3">
                            <div class="form-group">
                              <b>Distributor Price</b>
                                
                                    <asp:TextBox ID="distamount" runat="server" class="form-control" Text="0" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers, Custom" ValidChars="."
    TargetControlID="distamount" />
                                </div></div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <b>Price</b>
                                
                                    <asp:TextBox ID="price" runat="server" class="form-control" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers, Custom" ValidChars="."
    TargetControlID="price" />
                                </div></div>
                        <div class="col-md-3">
                            <div class="form-group">
                              <b>MRP</b>
                                
                                    <asp:TextBox ID="mrp" runat="server" class="form-control" />
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers, Custom" ValidChars="."
    TargetControlID="mrp" />
                                </div></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                     <div class="form-group">
                              <b>Quantiy</b>
                                     <asp:TextBox ID="txtmquantity" runat="server" class="form-control" />
                                </div>
                            </div>
                                 <div class="col-md-4" id="falseshop" runat="server" visible="false">
                                     <div class="form-group">
                              <b>Select Shop</b>
                                  <asp:DropDownList ID="ddlmshop" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                                </div>
                        </div>
                       
                             
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                               
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" OnClick="btnUpdate_Click"
                                        Text="Approveproduct" class="btn btn-success" />
                                </div>
                                 </div>
                       
                             <div class="col-md-3" style="padding-top:18px">
                            <div class="form-group">
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-danger" />
                               </div>
                               
                            </div>
                        
                    </asp:Panel>
    <asp:Label ID ="lblshop" runat="server" Visible="false"></asp:Label>
        
    
                </div></div></div></div>
</asp:Content>
