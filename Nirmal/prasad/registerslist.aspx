﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="registerslist.aspx.cs" Inherits="Nirmal.prasad.registerslist" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.css" rel="stylesheet" type="text/css" />
    <link href="AdminMaster/assets/css/animate.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
    <div class="container-fluid">
        <div class="box box-header">
            <%--<h3>Registers List</h3>--%>
        </div>
        
        <div class="box box-body">          
                    
                    <div class="col-md-12">
                        <div class="box-body table-responsive no-padding" style="overflow: scroll; height: 450px">
                            <asp:Label ID="Label1" runat="server" Text="" Visible="false"></asp:Label>
                            <asp:GridView ID="GridView1" class="table table-bordered table-striped table-hover" runat="server" AutoGenerateColumns="False" >
                                <Columns>
                                    <asp:TemplateField  HeaderText="SNO" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex+1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="date" HeaderText="Date" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="fname" HeaderText="Full Name" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="uname" HeaderText="User Name" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="mobile" HeaderText="Mobile" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="pwd" HeaderText="Password" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="email" HeaderText="Email" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="role" HeaderText="Role" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="address" HeaderText="Address" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="aadhar" HeaderText="aadhar" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="accountno" HeaderText="Account No" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="holdername" HeaderText="Holder Name" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="bankname" HeaderText="Bank Name" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="branch" HeaderText="Branch" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                    <asp:BoundField DataField="ifsc" HeaderText="IFSC" HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212" />
                                      <asp:TemplateField HeaderStyle-BackColor="#ffffff" HeaderStyle-ForeColor="#131212">
                                            <ItemTemplate>
                                                <asp:Button ID="btndelete" runat="server" CssClass="btn btn-danger btn-xs" Text="Delete" OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>' OnClick="btndelete_Click" CausesValidation="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:Label Visible="false" runat="server" ID="lblid"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
</asp:Content>

