﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

        }


        protected void login1_Click(object sender, EventArgs e)
        {
            {
                int RowCount;
                String Name, Password, fname, Role, loc, id;

                String connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
                SqlConnection conn = new SqlConnection(connstrg);
                conn.Open();
                String Select = "select * from DistributorLogin where username='" + txtuname.Text + "'and password='" + txtpass.Text + "'";
                SqlCommand comm = new SqlCommand(Select, conn);
                SqlDataAdapter sqlda = new SqlDataAdapter(comm);
                DataTable dt = new DataTable();
                sqlda.Fill(dt);
                RowCount = dt.Rows.Count;
                for (int i = 0; i < RowCount; i++)
                {
                    Name = dt.Rows[i]["username"].ToString();
                    Password = dt.Rows[i]["password"].ToString();
                    fname = dt.Rows[i]["name"].ToString();
                    Role = dt.Rows[i]["role"].ToString();
                    loc = dt.Rows[i]["loc"].ToString();
                    id = dt.Rows[i]["id"].ToString();

                    if (Role == "Admin")
                    {
                        if (Name == txtuname.Text && Password == txtpass.Text)
                        {
                            HttpCookie nameCookie = new HttpCookie("Name");
                            nameCookie.Values["Name"] = Name;
                            HttpCookie nameCookie1 = new HttpCookie("Password");
                            nameCookie1.Values["Password"] = Password;
                            HttpCookie nameCookie2 = new HttpCookie("fname");
                            nameCookie2.Values["fname"] = fname;
                            HttpCookie nameCookie3 = new HttpCookie("Role");
                            nameCookie3.Values["Role"] = Role;
                            nameCookie.Expires = DateTime.Now.AddDays(1);
                            nameCookie1.Expires = DateTime.Now.AddDays(1);
                            nameCookie1.Expires = DateTime.Now.AddDays(1);
                            nameCookie3.Expires = DateTime.Now.AddDays(1);

                            //Add the Cookie to Browser.
                            Response.Cookies.Add(nameCookie);
                            Response.Cookies.Add(nameCookie1);
                            Response.Cookies.Add(nameCookie2);
                            Response.Cookies.Add(nameCookie3);
                            Response.Redirect("Dashboard.aspx");

                        }
                        else
                        {
                            Response.Write("<script>alert('Invalid User name and password')</script>");
                        }
                    }
                    else if (Role == "Default")
                    {
                        if (Name == txtuname.Text && Password == txtpass.Text)
                        {
                            HttpCookie nameCookie = new HttpCookie("Name");
                            nameCookie.Values["Name"] = Name;
                            HttpCookie nameCookie1 = new HttpCookie("Password");
                            nameCookie1.Values["Password"] = Password;
                            HttpCookie nameCookie2 = new HttpCookie("fname");
                            nameCookie2.Values["fname"] = fname;
                            HttpCookie nameCookie3 = new HttpCookie("Role");
                            nameCookie3.Values["Role"] = Role;
                            HttpCookie nameCookie4 = new HttpCookie("loc");
                            nameCookie4.Values["loc"] = loc;
                            nameCookie.Expires = DateTime.Now.AddDays(1);
                            nameCookie1.Expires = DateTime.Now.AddDays(1);
                            nameCookie1.Expires = DateTime.Now.AddDays(1);
                            nameCookie3.Expires = DateTime.Now.AddDays(1);
                            nameCookie4.Expires = DateTime.Now.AddDays(1);

                            //Add the Cookie to Browser.
                            Response.Cookies.Add(nameCookie);
                            Response.Cookies.Add(nameCookie1);
                            Response.Cookies.Add(nameCookie2);
                            Response.Cookies.Add(nameCookie3);
                            Response.Cookies.Add(nameCookie4);
                            Response.Redirect("Dashboard.aspx");


                        }

                        else
                        {

                            Response.Write("<script>alert('Invalid User name and password')</script>");
                        }
                    }
                    //else if (Role == "dealer")
                    //{
                    //    if (Name == txtuname.Text && Password == txtpass.Text)
                    //    {
                    //        HttpCookie nameCookie = new HttpCookie("Name");
                    //        nameCookie.Values["Name"] = Name;
                    //        HttpCookie nameCookie1 = new HttpCookie("Password");
                    //        nameCookie1.Values["Password"] = Password;
                    //        HttpCookie nameCookie2 = new HttpCookie("fname");
                    //        nameCookie2.Values["fname"] = fname;
                    //        HttpCookie nameCookie3 = new HttpCookie("Role");
                    //        nameCookie3.Values["Role"] = Role;
                    //        HttpCookie nameCookie4 = new HttpCookie("loc");
                    //        nameCookie4.Values["loc"] = loc;
                    //        HttpCookie nameCookie5 = new HttpCookie("id");
                    //        nameCookie5.Values["id"] = id;
                    //        nameCookie.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie1.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie1.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie3.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie4.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie5.Expires = DateTime.Now.AddDays(1);

                    //        //Add the Cookie to Browser.
                    //        Response.Cookies.Add(nameCookie);
                    //        Response.Cookies.Add(nameCookie1);
                    //        Response.Cookies.Add(nameCookie2);
                    //        Response.Cookies.Add(nameCookie3);
                    //        Response.Cookies.Add(nameCookie4);
                    //        Response.Cookies.Add(nameCookie5);
                    //        Response.Redirect("Ordersdashboard.aspx");

                    //    }

                    //}
                    //else if(Role=="merchant")
                    //{
                    //    if (Name == txtuname.Text && Password == txtpass.Text)
                    //    {
                    //        HttpCookie nameCookie = new HttpCookie("Name");
                    //        nameCookie.Values["Name"] = Name;
                    //        HttpCookie nameCookie1 = new HttpCookie("Password");
                    //        nameCookie1.Values["Password"] = Password;
                    //        HttpCookie nameCookie2 = new HttpCookie("fname");
                    //        nameCookie2.Values["fname"] = fname;
                    //        HttpCookie nameCookie3 = new HttpCookie("Role");
                    //        nameCookie3.Values["Role"] = Role;
                    //        HttpCookie nameCookie4 = new HttpCookie("loc");
                    //        nameCookie4.Values["loc"] = loc;
                    //        HttpCookie nameCookie5 = new HttpCookie("id");
                    //        nameCookie5.Values["id"] = id;
                    //        nameCookie.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie1.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie1.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie3.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie4.Expires = DateTime.Now.AddDays(1);
                    //        nameCookie5.Expires = DateTime.Now.AddDays(1);

                    //        //Add the Cookie to Browser.
                    //        Response.Cookies.Add(nameCookie);
                    //        Response.Cookies.Add(nameCookie1);
                    //        Response.Cookies.Add(nameCookie2);
                    //        Response.Cookies.Add(nameCookie3);
                    //        Response.Cookies.Add(nameCookie4);
                    //        Response.Cookies.Add(nameCookie5);
                    //        Response.Redirect("Ordersdashboard.aspx");

                    //    }

                }

            
        
                conn.Close();

            }
           

        }
    }
}
