﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class productsdisplay : System.Web.UI.Page
    {
        string cookiee = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind();
            }

            HttpCookie nameCookie = Request.Cookies["Role"];
            cookiee = nameCookie != null ? nameCookie.Value.Split('=')[1] : "undefined";
        }
        protected void bind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from addproduct where status='0' order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (cookiee == "Default")
                {
                    this.grdProduct_Details.Columns[5].Visible = false;
                    grdProduct_Details.DataSource = ds;
                    grdProduct_Details.DataBind();
                }
                else
                {
                    grdProduct_Details.DataSource = ds;
                    grdProduct_Details.DataBind();
                }

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";

            }
        }



        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            string deid = string.Empty;

            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lb.Text = grdProduct_Details.DataKeys[gRow.RowIndex].Value.ToString();
            price.Text = gRow.Cells[5].Text.Replace("&nbsp;", "");
            mrp.Text = gRow.Cells[6].Text.Replace("&nbsp;", "");
            txtmquantity.Text = gRow.Cells[4].Text.Replace("&nbsp;", "");
            SqlCommand cmd = new SqlCommand("select shopname,did from Addproduct where id='" + lb.Text + "'", conn44);
            conn44.Open();
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {

                deid = dr["did"].ToString();
                lblshop.Text = dr["shopname"].ToString();
            }
            conn44.Close();
            if (lblshop.Text == "Select Kirana Center")
            {
                falseshop.Visible = true;
                DataSet ds = DataQueries.SelectCommon("select distinct (d.shopname) as shopname from DistributorLogin d inner join Addproduct a on d.did=a.did where d.did='" + deid + "'");
                ddlmshop.DataSource = ds;
                ddlmshop.DataTextField = "shopname";
                ddlmshop.DataBind();
                this.ModalPopupExtender2.Show();
            }
            else
            {

                falseshop.Visible = false;
                this.ModalPopupExtender2.Show();
            }



        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            string serverpath = string.Empty;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            if (lblshop.Text == "Select Kirana Center")
            {
                conn44.Open();
                SqlCommand cmd = new SqlCommand("update Addproduct set distamount='" + distamount.Text + "',dealamount='" + dealamount.Text + "',price='" + price.Text + "',mrp='" + mrp.Text + "',units='" + txtmquantity.Text + "',shopname='" + ddlmshop.SelectedItem.Text + "',status='1' where id='" + lb.Text + "'", conn44);
                cmd.ExecuteNonQuery();
                conn44.Close();
                bind();
            }
            else
            {
                conn44.Open();
                SqlCommand cmd = new SqlCommand("update Addproduct set distamount='" + distamount.Text + "',dealamount='" + dealamount.Text + "',price='" + price.Text + "',mrp='" + mrp.Text + "',units='" + txtmquantity.Text + "',status='1' where id='" + lb.Text + "'", conn44);
                cmd.ExecuteNonQuery();
                conn44.Close();
                bind();

            }


        }


        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lb.Text = grdProduct_Details.DataKeys[gRow.RowIndex].Value.ToString();
            DataQueries.DeleteCommon("Delete Addproduct where id='" + lb.Text + "'");
            Response.Write("<script>alert('Item Deleted')</script>");
            bind();
        }
    }
}