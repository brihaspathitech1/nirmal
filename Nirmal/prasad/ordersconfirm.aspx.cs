﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class ordersconfirm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Bindgrid();
            }
        }

        SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
        protected void Bindgrid()
        {
            conn44.Open();
            SqlCommand cmd1 = new SqlCommand("select o.id as id1,o.date,d.* from distributorlogin  d inner join tblorder o on d.id=o.Distributor_id where o.appstatus='1'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridView1.DataSource = ds;
                GridView1.DataBind();
                int columncount = GridView1.Rows[0].Cells.Count;
                GridView1.Rows[0].Cells.Clear();
                GridView1.Rows[0].Cells.Add(new TableCell());
                GridView1.Rows[0].Cells[0].ColumnSpan = columncount;
                GridView1.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void linkdelete_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = GridView1.DataKeys[grow.RowIndex].Value.ToString();
            SqlCommand cmd = new SqlCommand("update tblorder set appstatus = '2' where id = '" + lblId.Text + "' ", conn44);
            conn44.Open();
            cmd.ExecuteNonQuery();
            conn44.Close();

            Bindgrid();
        }
    }
}