﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace Nirmal.prasad
{
    public partial class Merchentlist : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                distbind();
            }

        }
        protected void distbind()
        {

            SqlCommand cmd = new SqlCommand("select * from DistributorLogin where status='1' and role='merchant' order by id desc ", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            connstr.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grddistributors.DataSource = ds;

                grddistributors.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grddistributors.DataSource = ds;
                grddistributors.DataBind();
                int columncount = grddistributors.Rows[0].Cells.Count;
                grddistributors.Rows[0].Cells.Clear();
                grddistributors.Rows[0].Cells.Add(new TableCell());
                grddistributors.Rows[0].Cells[0].ColumnSpan = columncount;
                grddistributors.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}