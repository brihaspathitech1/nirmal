﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Updateshop.aspx.cs" Inherits="Nirmal.prasad.Updateshop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" />

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #000 !important
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  
   
    <div class="box">
        <!-- Content Header (Page header) -->
        <div class="box-header">
        

        </div>
        <!-- Main content -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
    
    <div class="col-md-6">
                      <div class="form-group">
                          <b>ShopNames</b>
                          <asp:ListBox ID="list1" runat="server" class="form-control select2" SelectionMode="multiple" data-placeholder="Select " Style="width: 100%;" ></asp:ListBox>
                      </div>
                  </div>
                      <div class="col-md-6">
                      <div class="form-group">
                          <b>Village</b>
                          <asp:ListBox ID="list2" runat="server" class="form-control select2" SelectionMode="multiple" data-placeholder="Select " Style="width: 100%;" ></asp:ListBox>
                      </div>
                  </div>
                </div>
    <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Add Shops" OnClick="btnsubmit_Click" />
                                    </div>
                                </div></div>
            <div class="row">
            <div class="col-md-3">
                                    <div class="form-group">
                                       <asp:TextBox ID="shops" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5" ></asp:TextBox>
                                    </div>
                                </div>

            <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" CssClass="btn btn-success" runat="server" Text="Remove shops" OnClick="Button1_Click" />
                                    </div>
                                </div>

                </div>
        </div></div>
</asp:Content>
