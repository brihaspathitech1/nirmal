﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Nirmal.prasad
{
    public partial class Ordersapprove : System.Web.UI.Page

    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                distbind();
            }

        }
        protected void distbind()
        {

            SqlCommand cmd = new SqlCommand("with cte as (select o.id, o.date,d.name,d.address,o.amount,o.status,o.updatedamount,d.mobile,d.loc,(c.name) as CustomerName from  tblorder o inner join DistributorLogin d on o.Distributor_id=d.id inner join customer c on c.Did=d.id where o.status='pending')  select *, substring(cte.date, 0, 11) as dt from cte", connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            connstr.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grddistributors.DataSource = ds;

                grddistributors.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grddistributors.DataSource = ds;
                grddistributors.DataBind();
                int columncount = grddistributors.Rows[0].Cells.Count;
                grddistributors.Rows[0].Cells.Clear();
                grddistributors.Rows[0].Cells.Add(new TableCell());
                grddistributors.Rows[0].Cells[0].ColumnSpan = columncount;
                grddistributors.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}