﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace Nirmal.prasad
{
    public partial class Suggesstionslist : System.Web.UI.Page
    {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                suggestionsbind();
            }

        }
        protected void suggestionsbind()
        {
            SqlCommand cmd = new SqlCommand("select date,sname,sugg,smob,sadd from suggestions",connstr);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            connstr.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                gridsuggestions.DataSource = ds;
                gridsuggestions.DataBind();

            }
        }
    }
}