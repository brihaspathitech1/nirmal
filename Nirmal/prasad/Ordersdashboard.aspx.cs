﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class Ordersdashboard : System.Web.UI.Page
    {
        string connection = @"Data Source=103.233.79.22;Initial Catalog=Nirmal;User ID=Nirmal;Password=Brihaspathi@123";
        string cookieeloc = string.Empty;
        string cookiee = string.Empty;
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HttpCookie nameCookie4 = Request.Cookies["loc"];
                cookieeloc = nameCookie4 != null ? nameCookie4.Value.Split('=')[1] : "undefined";

                HttpCookie nameCookie3 = Request.Cookies["Role"];
                cookiee = nameCookie3 != null ? nameCookie3.Value.Split('=')[1] : "undefined";

                datalist1();
                datalist2();
                datalist3();
            }

        
            
        }
        protected void datalist1()
        {
           
            SqlConnection conn = new SqlConnection(connection);
            conn.Open();
            if(cookiee=="dealer")
            { 
            SqlCommand cmd = new SqlCommand("select d.name,d.image,count(o.id) as count from DistributorLogin d inner join tblorder o  on d.id=o.Distributor_id where d.role='distributor' and o.status='Approved' and d.loc='"+cookieeloc+"'  group by name,image", conn);
            DataList1.DataSource = cmd.ExecuteReader();
            DataList1.DataBind();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select d.name,d.image,count(o.id) as count from DistributorLogin d inner join tblorder o  on d.id=o.Distributor_id where d.role='distributor' and o.status='Approved'  group by name,image", conn);
                DataList1.DataSource = cmd.ExecuteReader();
                DataList1.DataBind();
            }
            conn.Close();


        }

        protected void datalist2()
        {
            SqlConnection conn = new SqlConnection(connection);
            conn.Open();
            //SqlCommand cmd = new SqlCommand("with cte as(select id, case when CHARINDEX(',',assign)>0 then SUBSTRING(assign,1,CHARINDEX(',',assign)-1) else assign end Tech1, CASE WHEN CHARINDEX(',',assign)>0 THEN SUBSTRING(assign,CHARINDEX(',',assign)+1,len(assign))  ELSE NULL END as Tech2,closedtime from RaiseNewTicket  where closed=1)select count(val) as count, c.val,t.techimage from cte a inner join cte b  on a.id = b.id cross apply (values(a.id, a.Tech1),   (b.id, b.Tech2)) as c(id, val) inner join tech t on t.name=a.Tech1 where c.val is not null and DATEDIFF(second, cast(a.closedtime as datetime), GETDATE())<86400 group by val,techimage order by count(val) desc", conn);
            if(cookiee=="dealer")
            { 
            SqlCommand cmd = new SqlCommand("select d.name,d.image,count(o.id) as count from DistributorLogin d inner join tblorder o  on d.id=o.Distributor_id where d.role='distributor' and o.status='Approved' and DATEDIFF(second, cast(o.date as datetime), GETDATE())<86400 and d.loc='"+cookieeloc+"'  group by name,image", conn);
            DataList2.DataSource = cmd.ExecuteReader();
            DataList2.DataBind();
           
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select d.name,d.image,count(o.id) as count from DistributorLogin d inner join tblorder o  on d.id=o.Distributor_id where d.role='distributor' and o.status='Approved' and DATEDIFF(second, cast(o.date as datetime), GETDATE())<86400  group by name,image", conn);
                DataList2.DataSource = cmd.ExecuteReader();
                DataList2.DataBind();
            }
            conn.Close();

        }
        protected void datalist3()
        {
            SqlConnection conn = new SqlConnection(connection);
            conn.Open();
            if (cookiee == "dealer")
            {
                SqlCommand cmd = new SqlCommand("select d.name,d.image,count(o.id) as count from DistributorLogin d inner join tblorder o  on d.id=o.Distributor_id where d.role='distributor' and o.status='Approved' and DATEDIFF(second, cast(o.date as datetime), GETDATE())<172800 and d.loc='"+cookieeloc+"' group by name,image ", conn);
                DataList3.DataSource = cmd.ExecuteReader();
                DataList3.DataBind();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select d.name,d.image,count(o.id) as count from DistributorLogin d inner join tblorder o  on d.id=o.Distributor_id where d.role='distributor' and o.status='Approved' and DATEDIFF(second, cast(o.date as datetime), GETDATE())<172800  group by name,image ", conn);
                DataList3.DataSource = cmd.ExecuteReader();
                DataList3.DataBind();

            }
            conn.Close();

        }
        protected void bindgrid1()
        {
            SqlConnection conn = new SqlConnection(connection);
            conn.Open();
            SqlCommand cmd = new SqlCommand("select ROW_NUMBER()over(order by o.id) as sno,(o.id) as orderid,(o.amount) as orderamount,convert(varchar,cast(o.date as date),105) as date,d.address from Distributorlogin d inner join tblorder o on d.id= o.Distributor_id where d.role='distributor' and o.status='Approved' and d.name like '%" + lblname.Text + "%'", conn);
            GridView2.DataSource = cmd.ExecuteReader();
            GridView2.DataBind();
            conn.Close();


        }
        protected void bindgrid2()
        {
            SqlConnection conn = new SqlConnection(connection);
            conn.Open();
            SqlCommand cmd = new SqlCommand("select ROW_NUMBER()over(order by o.id) as sno,(o.id) as orderid,(o.amount) as orderamount,convert(varchar,cast(o.date as date),105) as date,d.address from Distributorlogin d inner join tblorder o on d.id= o.Distributor_id where d.role='distributor' and o.status='Approved' and DATEDIFF(second, cast(o.date as datetime), GETDATE())<86400 and d.name like '%" + lblname.Text + "%'", conn);
            GridView2.DataSource = cmd.ExecuteReader();
            GridView2.DataBind();
            conn.Close();


        }
        protected void bindgrid3()
        {
            SqlConnection conn = new SqlConnection(connection);
            conn.Open();
            SqlCommand cmd = new SqlCommand("select ROW_NUMBER()over(order by o.id) as sno,(o.id) as orderid,(o.amount) as orderamount,convert(varchar,cast(o.date as date),105) as date,d.address from Distributorlogin d inner join tblorder o on d.id= o.Distributor_id where d.role='distributor' and o.status='Approved' and DATEDIFF(second, cast(o.date as datetime), GETDATE())<172800 and d.name like '%" + lblname.Text + "%'", conn);
            GridView2.DataSource = cmd.ExecuteReader();
            GridView2.DataBind();
            conn.Close();


        }

        protected void btn1_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            lblname.Text = btn.CommandArgument;
            bindgrid1();
            this.modelpopup1.Show();
        }

        protected void btn2_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            lblname.Text = btn.CommandArgument;
            bindgrid2();
            this.modelpopup1.Show();
        }

        protected void btn3_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            lblname.Text = btn.CommandArgument;
            bindgrid3();
            this.modelpopup1.Show();
        }
    }
}