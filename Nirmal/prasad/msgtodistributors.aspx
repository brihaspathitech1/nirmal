﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="msgtodistributors.aspx.cs" Inherits="Nirmal.prasad.msgtodistributors" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .style1 {
            color: #FF0000;
        }

        body {
            color: #000 !important;
            font-weight: bold !important;
        }
    </style>
    <%--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />--%>


    <!-- Select2 -->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" />

    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            color: #000 !important
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>
    <section class="content-header">
        <h4>Send SMS to Distributors </h4>
        <hr />
    </section>

    <section class="content">



        <div class="row">

            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3>&nbsp</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>


                        </div>
                    </div>


                    <div class="box-body">
                        select distributor
              <div class="row">

                  <asp:Label ID="txtmobile" runat="server" Visible="false"></asp:Label>
                  <%-- <div class="col-md-3">
                <div class="form-group">
                         Select Distributor 
                           
                            <asp:DropDownList ID="dist" CssClass="form-control" runat="server"></asp:DropDownList>
                        
                        </div>
                      </div>--%>

                  <div class="col-md-6">
                      <div class="form-group">
                          <asp:ListBox ID="list1" runat="server" class="form-control select2" SelectionMode="multiple" data-placeholder="Select " Style="width: 100%;" OnSelectedIndexChanged="list1_SelectedIndexChanged" AutoPostBack="true" ></asp:ListBox>
                      </div>
                  </div>
              </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    message
                            <span class="style1">*</span>
                                    <asp:TextBox ID="txtmessage" TextMode="MultiLine" MaxLength="10" Height="200px" Width="290px" class="form-control" runat="server"></asp:TextBox>


                                </div>
                            </div>
                        </div>


                        <div class="col-sm-2">
                            <div class="form-group">
                                <asp:Button ID="btnsendone" runat="server" Text="Send To Selected" class="btn btn-success btn-block" OnClick="btnsendone_Click" />
                            </div>
                        </div>


                        <div class="col-sm-2">
                            <div class="form-group">
                                <asp:Button ID="btnsendall" runat="server" Text="Send To All" class="btn btn-success" OnClick="btnsendall_Click" />
                            </div>
                        </div>

                    </div>
                     <div class="col-md-12">
                            <div class="table table-responsive" style="overflow: scroll; height: 420px">
                                <asp:GridView ID="GridLogin" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server" Font-Bold="true">
                                    <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="SNO" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex+1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="name" HeaderText="Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"  HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="role" HeaderText="Role" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"  HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="loc" HeaderText="Location" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"  HeaderStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Mobile" HeaderText="Mobile" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white"  HeaderStyle-HorizontalAlign="Center" />
                                      
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                </div>
            </div>
        </div>
  
        <script>
            // This example displays an address form, using the autocomplete feature
            // of the Google Places API to help users fill in the information.

            // This example requires the Places library. Include the libraries=places
            // parameter when you first load the API. For example:
            // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

            var placeSearch, autocomplete;
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
            };

            function initAutocomplete() {
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                    { types: ['geocode'] });

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                autocomplete.addListener('place_changed', fillInAddress);
            }

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();

                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }

                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }
                }
            }

            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOSxb5hyc4b2X7YF9WZJVDVjsKhrEd1lI&libraries=places&callback=initAutocomplete"
            async defer></script>
        <!-- jQuery 2.2.3 -->
        <script src="vendor/jQuery/jquery-2.2.3.min.js"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Select2 -->

        <script src="vendor/select2/select2.min.js"></script>



        <!-- Page script -->
        <script>
            $(function () {
                //Initialize Select2 Elements
                $(".select2").select2();


            });

        </script>
    </section>
 
</asp:Content>

