﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Nirmal.prasad
{
    public partial class Addproduct : System.Web.UI.Page
    {
        double total;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bindcecategory();

            }

        }
        protected void Bindcecategory()
        {
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;

            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstrg))
                {
                    using (SqlCommand sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandText = "select distinct categoryname from Category";
                        sqlCmd.Connection = sqlConn;
                        sqlConn.Open();
                        SqlDataAdapter da = new SqlDataAdapter(sqlCmd);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        dropcategory.DataSource = dt;

                        dropcategory.DataTextField = "categoryname";
                        //dropdept.DataValueField = "id";
                        dropcategory.DataBind();
                        sqlConn.Close();
                        dropcategory.Items.Insert(0, new System.Web.UI.WebControls.ListItem("Select Category", "0"));
                    }
                }
            }
            catch { }

        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string serverpath = string.Empty;
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg);
            if (Imgprev.HasFile)
            {
                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                Imgprev.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
            }

            DataSet ds = DataAccessLayer.DataQueries.SelectCommon("select id from Category where categoryname='" + dropcategory.SelectedValue + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblid.Text = ds.Tables[0].Rows[0]["id"].ToString();
            }


            if (droptax.SelectedItem.Text == "GST 5%")
            {
                double stotal = Convert.ToDouble(txtPrice.Text);
                double tamt = stotal * 0.05;
                total = stotal + tamt;
            }
            else if (droptax.SelectedItem.Text == "GST 12%")
            {
                double stotal = Convert.ToDouble(txtPrice.Text);
                double tamt = stotal * 0.12;
                total = stotal + tamt;
            }
            else if (droptax.SelectedItem.Text == "GST 18%")
            {
                double stotal = Convert.ToDouble(txtPrice.Text);
                double tamt = stotal * 0.18;
                total = stotal + tamt;
            }
            else if (droptax.SelectedItem.Text == "GST 28%")
            {
                double stotal = Convert.ToDouble(txtPrice.Text);
                double tamt = stotal * 0.28;
                total = stotal + tamt;
            }
            else if (droptax.SelectedItem.Text == "GST 0%")
            {
                total = Convert.ToDouble(txtPrice.Text);
            }

            String insert = "insert into Addproduct(productname,categoryid,expirydate,maintenance,units,price,available,image,status,GSTprice,GSTtype,supname,supproductcost,supmobile,supaddress)values('" + Name.Text + "','" + lblid.Text + "','" + txtexpiry.Text + "','" + txtmaintain.Text + "','" + txtQuantity.Text + "','" + txtPrice.Text + "','" + txtAvalibility.Text + "','" + serverpath + "','1','" + total + "','" + droptax.SelectedItem.Text + "','" + txtsupname.Text + "','" + txtsupproductcost.Text + "','" + txtsupmobile.Text + "','" + txtsupaddress.Text + "' )";
            SqlCommand comm1 = new SqlCommand(insert, conn1);

            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            Name.Text = "";
            //category.Text = "";

            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtAvalibility.Text = "";

            //Response.Write("<script>alert('Product Added Successfully')</script>");
            Response.Redirect("Productlist.aspx");

        }
    }
}
