﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Addproduct.aspx.cs" Inherits="Nirmal.prasad.Addproduct" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/angular.min.js" type="text/javascript"></script>
    <script type="text/javascript">
       function showimagepreview(input) {
           if (input.files && input.files[0]) {
               var filerdr = new FileReader();
               filerdr.onload = function (e) {
                   $('#imgprvw').attr('src', e.target.result);
               }
               filerdr.readAsDataURL(input.files[0]);
           }
       }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Add Product</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <asp:Label ID="lblid" runat="server"></asp:Label>
			  <div class="col-md-12">
            <div class="col-md-12">
                <div class="row">
                                  
                   
                     <div class="col-md-3">
                <div class="form-group">
                            <b>Category</b>
                            <span class="style1">*</span>
                            <asp:DropDownList ID="dropcategory" runat="server" class="form-control"></asp:DropDownList>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="dropdept"
                                ErrorMessage="Select Department" Style="color: #FF0000"></asp:RequiredFieldValidator>--%>
                            
                        </div>
                        </div>
                    <div class="col-md-3">
                        <label>Product Name</label>
                        <asp:TextBox ID="Name" runat="server" class="form-control "></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                      <label>Quantity</label>
                       <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" ></asp:TextBox>
                       
                     </div>
                      <div class="col-md-3">
                      <label>Price</label>
                      <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control" ></asp:TextBox>
                      
                       
                      </div> 
                     
                     
                    </div>
                <div class="row">
                    <div class="col-md-3">
                <div class="form-group">
                            <b>GstType</b>
                            <span class="style1">*</span>
                            <asp:DropDownList ID="droptax" runat="server" class="form-control">
                                <asp:ListItem>Select GST Type</asp:ListItem>
                                <asp:ListItem>GST 0%</asp:ListItem>
                                <asp:ListItem>GST 5%</asp:ListItem>
                                <asp:ListItem>GST 12%</asp:ListItem>
                                <asp:ListItem>GST 18%</asp:ListItem>
                                <asp:ListItem>GST 28%</asp:ListItem> 
                            </asp:DropDownList>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="dropdept"
                                ErrorMessage="Select Department" Style="color: #FF0000"></asp:RequiredFieldValidator>--%>
                            
                        </div>
                        </div>
                     <div class="col-md-3">
                                        <label>Expiry Date</label>
                                        <asp:TextBox ID="txtexpiry" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server"
                                            BehaviorID="txtDate_CalendarExtender" TargetControlID="txtexpiry" Format="yyyy-MM-dd" />


                                    </div>
                    <div class="col-md-3">
                                        <label>Product Maintenance Charge</label>
                                        <asp:TextBox ID="txtmaintain" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        


                                    </div>
                    <div class="col-sm-3">
                        
                   <div class="col-sm-4 padbot" style="margin-right: 1px">
                       <img id="imgprvw" height="100px" width="100px" /><br />
                   </div>
                    <div class="col-sm-8 padtop">
                       <asp:FileUpload ID="Imgprev" runat="server" onchange="showimagepreview(this)" Width="214px" />
                       
                   </div>
                     </div>
                        </div>
                <div class="row">
                                  
                   
                     <div class="col-md-3">
                <div class="form-group">
                            <b>Suplier Name</b>
                            <span class="style1">*</span>
                    <asp:TextBox ID="txtsupname" runat="server" class="form-control "></asp:TextBox>
                            
                            
                        </div>
                        </div>
                    <div class="col-md-3">
                        <label>SuplierProduct Cost</label>
                        <asp:TextBox ID="txtsupproductcost" runat="server" class="form-control "></asp:TextBox>
                    </div>
                    <div class="col-md-3">
                      <label>Suplier Mobile</label>
                       <asp:TextBox ID="txtsupmobile" runat="server" CssClass="form-control" ></asp:TextBox>
                       
                     </div>
                      <div class="col-md-3">
                      <label>Suplier Address</label>
                      <asp:TextBox ID="txtsupaddress" runat="server" CssClass="form-control" ></asp:TextBox>
                      
                       
                      </div> 
                     
                     
                    </div>
                
                   
          <div="row">
    <div class="col-md-3">
                      <label>Availability</label>
                      <asp:TextBox ID="txtAvalibility" runat="server" CssClass="form-control" ></asp:TextBox>
                           
                     </div>
                  
                        </div>
                      

                 
                  
   <div class="col-md-3" style="padding-top:26px">   
   <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CausesValidation="false" class="btn btn-success"/>
       </div>

</div>

</div>







        </div>
   
</div>
              </div>
            </div>
    
    
</asp:Content>
