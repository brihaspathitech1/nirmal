﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Bankdetails.aspx.aspx.cs" Inherits="Nirmal.prasad.Bankdetails_aspx" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
       function showimagepreview(input) {
           if (input.files && input.files[0]) {
               var filerdr = new FileReader();
               filerdr.onload = function (e) {
                   $('#FileUpload1').attr('src', e.target.result);
               }
               filerdr.readAsDataURL(input.files[0]);
           }
       }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script type="text/javascript">
            function Search_Gridview(strKey) {
                var strData = strKey.value.toLowerCase().split(" ");
                var tblData = document.getElementById("<%=GridBank.ClientID %>");
                var rowData;
                for (var i = 1; i < tblData.rows.length; i++) {
                    rowData = tblData.rows[i].innerHTML;
                    var styleDisplay = 'none';
                    for (var j = 0; j < strData.length; j++) {
                        if (rowData.toLowerCase().indexOf(strData[j]) >= 0)
                            styleDisplay = '';
                        else {
                            styleDisplay = 'none';
                            break;
                        }
                    }
                    tblData.rows[i].style.display = styleDisplay;
                }
            }  
        </script>
    <div class="box">
        <!-- Content Header (Page header) -->
        <div class="box-header">
            <h1>Bank Details
        
      </h1>

        </div>
        <!-- Main content -->
        <div class="box-body">

            <div class="row">
                <div class="col-md-12">
                    <!-- AREA CHART -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <!--<h3 class="box-title">New Estimate</h3>-->

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        
                             <div class="col-md-3">
                                <div class="form-group">
                            <b>Bank Name</b> <asp:TextBox ID="txtbankname" CssClass="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                     
                                    
                                    </div>
                                    </div>
                          
                             <div class="col-md-3">
                                            <div class="form-group">
                                             <b>AccountHolder Name:</b>
                                            <asp:TextBox ID="txtholdername" CssClass="form-control" runat="server"></asp:TextBox>


                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                              <b>Account Number:</b>
                                            <asp:TextBox ID="txtaccountnbr" CssClass="form-control" runat="server"></asp:TextBox>


                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                              <b>Branch</b>
                                            <asp:TextBox ID="txtbranch" CssClass="form-control" runat="server"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                              <b>IFSC </b>
                                            <asp:TextBox ID="txtifsc" CssClass="form-control" runat="server"></asp:TextBox>

                                              
                                            </div>
                                            </div>
                                       
                         <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:Button ID="btnsubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click"/>
                                    </div>
                                 </div>
                                             
                                <div class ="clearfix"></div>
                      
                                 <div class="col-md-3" style="padding-left: 10px;">  
     <asp:TextBox ID="txtSearch" runat="server" Font-Size="15px" placeholder="Search....." CssClass="form-control" onkeyup="Search_Gridview(this)"></asp:TextBox><br />
     </div>
                            
                            <asp:Label ID="lb" runat="server" Visible="false"></asp:Label>
                            <div class="col-md-12">
                                <div class="table table-responsive" style="overflow: scroll; height: 420px">
                            <asp:GridView ID="GridBank" AutoGenerateColumns="false" CssClass="table table-respnsive table-striped" GridLines="None" runat="server"  Font-Bold="true" DataKeyNames="id" >
                                <HeaderStyle CssClass="GridViewHeaderStyle" HorizontalAlign="Center"  />
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="SNO" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="bankname" HeaderText="Bank Name" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="holdername" HeaderText="Account Holder" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="accountnumber" HeaderText="Account Number" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="branch" HeaderText="Branch" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />


                                    <asp:BoundField DataField="ifsc" HeaderText="Ifsc Code" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                                   

                                    <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btnEdit" runat="server" CssClass="btn btn-primary btn-xs" Text="Edit" CommandArgument='<%# Eval("Id") %>' CausesValidation="false" OnClick="btnEdit_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Button ID="btndelete"  runat="server" CssClass="btn btn-danger btn-xs" Text="Delete"  OnClientClick="if ( !confirm('Are you sure you want to delete this Login?')) return false;" CommandArgument='<%# Eval("Id") %>'  CausesValidation="false" OnClick="btndelete_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                </div></div>

                            <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" CancelControlID="btncancel" TargetControlID="HiddenField1"></cc1:ModalPopupExtender>
                            <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#cecece" BorderStyle="Solid" BorderWidth="1px"  Height="600px" Width="650px" ForeColor="#ffffff" style="background-color: rgba(0,0,0,0.8)" >
                                  <h3 class="text-center">Edit Bank Details</h3>
                                <hr />
                                <div class="col-md-6">
                                <div class="form-group">
                            <b>Bank Name</b> <asp:TextBox ID="txtebankname" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                <div class="form-group">
                           <b>AccountHolder Name</b> <asp:TextBox ID="txteholder" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                <div class="form-group">
                            <b>Account Number </b><asp:TextBox ID="txteaccountnbr" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                             
                            
                             <div class="col-md-6">
                                <div class="form-group">
                           <b> Branch</b><asp:TextBox ID="txtebranch" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                             <div class="col-md-6">
                                <div class="form-group">
                            <b>IFSC Code</b> <asp:TextBox ID="txteifsccode" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                 </div>
                               
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:Button ID="btnupdate" CssClass="btn btn-success" runat="server" Text="Update" OnClick="btnupdate_Click"/>
                                    </div>
                                    </div>
                                <div class="col-md-3" style="padding-top:20px">
                                <div class="form-group">
                                 <asp:Button ID="btncancel" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                                    </div>
                                    </div>
                            </asp:Panel>
                            <asp:Label ID="lblid" runat="server" Text="" Visible="false"></asp:Label>
                            </div>
                    </div>
                        </div>
                    </div>
                
            </div>
        </div>
</asp:Content>
