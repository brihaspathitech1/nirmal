﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Nirmal.DataAccessLayer;
using System.Configuration;


namespace Nirmal.prasad
{
    public partial class Customerdetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind_Distributors();
            }
            lbldslctname.Visible = false;
            lblloc.Visible = false;
            lbldlocation.Visible = false;

        }
        protected void bind_Distributors()
        {
            DataSet ds = DataQueries.SelectCommon("select id,name from DistributorLogin where role='distributor'");
            ddlAdmins.DataTextField = "name";
            ddlAdmins.DataValueField = "id";
            ddlAdmins.DataSource = ds;
            ddlAdmins.DataBind();
            //ddlAdmins.Items.Insert(0, "-Select Distributor-");
        }

       

      
          
       protected void btnsubmit_Click(object sender, EventArgs e)
        {
           
        }

        protected void ddlAdmins_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbldslctname.Visible = true;
            lblloc.Visible = true;
            lbldlocation.Visible = true;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select c.name,c.mobile,c.Address,d.name from customer c inner join DistributorLogin d on d.id=c.Did where c.Did='" + ddlAdmins.SelectedItem.Value + "'", conn44);
            SqlCommand cmd1 = new SqlCommand("select name,loc from DistributorLogin where name='" + ddlAdmins.SelectedItem.Text + "' and role='distributor'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            da.Fill(ds);
            da1.Fill(ds1);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0 || ds1.Tables[0].Rows.Count > 0)
            {
                lbldslctname.Text = ds1.Tables[0].Rows[0]["name"].ToString();
                lbldlocation.Text = ds1.Tables[0].Rows[0]["loc"].ToString();
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();

            }
            else
            {


                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                int columncount = grdcustdetails.Rows[0].Cells.Count;
                grdcustdetails.Rows[0].Cells.Clear();
                grdcustdetails.Rows[0].Cells.Add(new TableCell());
                grdcustdetails.Rows[0].Cells[0].ColumnSpan = columncount;
                grdcustdetails.Rows[0].Cells[0].Text = "No Records Found";
            }

        }
       
    }
}