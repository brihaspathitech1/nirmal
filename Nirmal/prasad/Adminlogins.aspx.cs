﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Nirmal.DataAccessLayer;
using System.IO;

namespace Nirmal.prasad
{
    public partial class Adminlogins : System.Web.UI.Page
   {
        SqlConnection connstr = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }

       protected void btnsubmit_Click(object sender, EventArgs e)
        {
            connstr.Open();
            string serverpath = string.Empty;
            string message = "";
            
            if (droprole.SelectedValue == "1")
            {
                foreach (System.Web.UI.WebControls.ListItem item in list1.Items)
                {
                    if (item.Selected)
                    {
                        message += item.Text + ",";

                    }
                }
                string sub = message.Substring(0, message.Length - 1);
                if (FileUpload1.HasFile)

                {
                    string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string path = Server.MapPath("~/images/") + filename;
                    FileUpload1.PostedFile.SaveAs(path);
                    serverpath = @"http://www.gsrnirmal.com/images/" + filename;
                    SqlCommand cmd = new SqlCommand("Insert into DistributorLogin(name,username,password,mobile,email,role,address,dexpiry,status,image,bankname,acctno,branch,ifsc,loc,aadhar,shopname,village) values('" + txtadminname.Text + "','" + txtusername.Text + "','" + txtpwd.Text + "','" + txtmobile.Text + "','" + txtemail.Text + "','" + droprole.SelectedItem.Text + "','" + txtadress.Text + "','" + txtdexpiry.Text + "','1','" + serverpath + "','" + bank.Text + "','" + acctno.Text + "','" + branch.Text + "','" + ifsc.Text + "','" + loc.Text + "','" + aadhar.Text + "','" + sub + "','" + village.Text + "')", connstr);

                    cmd.ExecuteNonQuery();

                }



                else
                {
                    SqlCommand cmd = new SqlCommand("Insert into DistributorLogin(name,username,password,mobile,email,role,address,dexpiry,status,bankname,acctno,branch,ifsc,loc,aadhar,shopname,village) values('" + txtadminname.Text + "','" + txtusername.Text + "','" + txtpwd.Text + "','" + txtmobile.Text + "','" + txtemail.Text + "','" + droprole.SelectedItem.Text + "','" + txtadress.Text + "','" + txtdexpiry.Text + "','1','" + bank.Text + "','" + acctno.Text + "','" + branch.Text + "','" + ifsc.Text + "','" + loc.Text + "','" + aadhar.Text + "','" + sub + "','" + village.Text + "')", connstr);

                    cmd.ExecuteNonQuery();

                }
            }

            else
            {
                if (FileUpload1.HasFile)

                {
                    string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    string path = Server.MapPath("~/images/") + filename;
                    FileUpload1.PostedFile.SaveAs(path);
                    serverpath = @"http://www.gsrnirmal.com/images/" + filename;
                    SqlCommand cmd = new SqlCommand("Insert into DistributorLogin(name,username,password,mobile,email,role,address,dexpiry,status,image,bankname,acctno,branch,ifsc,loc,aadhar,village) values('" + txtadminname.Text + "','" + txtusername.Text + "','" + txtpwd.Text + "','" + txtmobile.Text + "','" + txtemail.Text + "','" + droprole.SelectedItem.Text + "','" + txtadress.Text + "','" + txtdexpiry.Text + "','1','" + serverpath + "','" + bank.Text + "','" + acctno.Text + "','" + branch.Text + "','" + ifsc.Text + "','" + loc.Text + "','" + aadhar.Text + "','" + village.Text + "')", connstr);

                    cmd.ExecuteNonQuery();

                }
                else
                {
                    SqlCommand cmd = new SqlCommand("Insert into DistributorLogin(name,username,password,mobile,email,role,address,dexpiry,status,bankname,acctno,branch,ifsc,loc,aadhar,village) values('" + txtadminname.Text + "','" + txtusername.Text + "','" + txtpwd.Text + "','" + txtmobile.Text + "','" + txtemail.Text + "','" + droprole.SelectedItem.Text + "','" + txtadress.Text + "','" + txtdexpiry.Text + "','1','" + bank.Text + "','" + acctno.Text + "','" + branch.Text + "','" + ifsc.Text + "','" + loc.Text + "','" + aadhar.Text + "','" + village.Text + "')", connstr);

                    cmd.ExecuteNonQuery();

                }
            }
            connstr.Close();

            txtadminname.Text = null;
            txtpwd.Text = null;
            txtusername.Text = null;
            txtmobile.Text = null;
            txtemail.Text = null;
            txtdexpiry.Text = null;
            txtadress.Text = bank.Text=acctno.Text=branch.Text=ifsc.Text=null;
            BindGrid();
        }
        protected void BindGrid()
        {
            string s = DateTime.Now.ToString("yyyy-MM-dd");
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmdd = new SqlCommand("select substring(dexpiry,0,11) as dexpiry from DistributorLogin where dexpiry='" + DateTime.Now.ToString("yyyy-MM-dd") + "' and status=1", conn44);
            SqlDataAdapter daa = new SqlDataAdapter(cmdd);
            DataSet dss = new DataSet();
            daa.Fill(dss);
            conn44.Close();
            if (dss.Tables[0].Rows.Count > 0)
            {
                lb.Text = dss.Tables[0].Rows[0]["dexpiry"].ToString();

            }
            if (s == lb.Text)
            {
                SqlConnection conw = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);

                conw.Open();
                SqlCommand cmded = new SqlCommand("update DistributorLogin set status='0' where dexpiry='" + lb.Text + "'", conw);
                cmded.ExecuteNonQuery();
                conw.Close();
                SqlCommand cmd = new SqlCommand("select * from DistributorLogin where status='1' order by id desc", conn44);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn44.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridLogin.DataSource = ds;

                    GridLogin.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridLogin.DataSource = ds;
                    GridLogin.DataBind();
                    int columncount = GridLogin.Rows[0].Cells.Count;
                    GridLogin.Rows[0].Cells.Clear();
                    GridLogin.Rows[0].Cells.Add(new TableCell());
                    GridLogin.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridLogin.Rows[0].Cells[0].Text = "No Records Found";

                }
            }
            else
            {
                SqlCommand cmd = new SqlCommand("select * from DistributorLogin where status='1' order by id desc ", conn44);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                conn44.Close();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GridLogin.DataSource = ds;

                    GridLogin.DataBind();

                }
                else
                {
                    ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                    GridLogin.DataSource = ds;
                    GridLogin.DataBind();
                    int columncount = GridLogin.Rows[0].Cells.Count;
                    GridLogin.Rows[0].Cells.Clear();
                    GridLogin.Rows[0].Cells.Add(new TableCell());
                    GridLogin.Rows[0].Cells[0].ColumnSpan = columncount;
                    GridLogin.Rows[0].Cells[0].Text = "No Records Found";
                }
            }
        }

        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Delete from DistributorLogin where id='" + id + "'", connstr);
            connstr.Open();
            cmd.ExecuteNonQuery();
            connstr.Close();
            BindGrid();
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            lblid.Text = btn.CommandArgument;
            SqlCommand cmd = new SqlCommand("Select * from DistributorLogin where id='" + lblid.Text + "'", connstr);
            connstr.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txtname.Text = dr["name"].ToString();
                txtuid.Text = dr["username"].ToString();
                txtpaswd.Text = dr["password"].ToString();
                txtmbile.Text = dr["mobile"].ToString();
                txtmail.Text = dr["email"].ToString();
                txtexpiry.Text = dr["dexpiry"].ToString();
                updateacctno.Text = dr["acctno"].ToString();
                updatebank.Text = dr["bankname"].ToString();
                updatebranch.Text = dr["branch"].ToString();
                updateifsc.Text = dr["ifsc"].ToString();
                TextBox1.Text = dr["aadhar"].ToString();
                address.Text = dr["address"].ToString();
                shpname.Text = dr["shopname"].ToString();
                
            }
            connstr.Close();
            ModalPopupExtender1.Show();
        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            connstr.Open();
            string serverpath = string.Empty;
            if (Imgprev.HasFile)
            {
                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                Imgprev.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
                SqlCommand cmd = new SqlCommand("Update DistributorLogin set name='" + txtname.Text + "',username='" + txtuid.Text + "',password='" + txtpaswd.Text + "',mobile='" + txtmbile.Text + "', email = '" + txtmail.Text + "',dexpiry='" + txtexpiry.Text + "',image='"+serverpath+ "',acctno='" + updateacctno.Text + "',branch='" + updatebranch.Text + "',bankname='" + updatebank.Text + "',ifsc='" + updateifsc.Text + "',aadhar='"+TextBox1.Text+"',address='"+address.Text+"',shopname='"+shpname.Text+"' where id='" + lblid.Text + "'", connstr);

                cmd.ExecuteNonQuery();
            }
            else
            {
                SqlCommand cmd = new SqlCommand("Update DistributorLogin set name='" + txtname.Text + "',username='" + txtuid.Text + "',password='" + txtpaswd.Text + "',mobile='" + txtmbile.Text + "', email = '" + txtmail.Text + "',dexpiry='" + txtexpiry.Text + "',acctno='"+updateacctno.Text+"',branch='"+updatebranch.Text+"',bankname='"+updatebank.Text+"',ifsc='"+updateifsc.Text+ "',aadhar='" + TextBox1.Text + "',address='" + address.Text + "',shopname='" + shpname.Text + "' where id='" + lblid.Text + "'", connstr);

                cmd.ExecuteNonQuery();
            }
            connstr.Close();
            BindGrid();
        }

        protected void loc_TextChanged(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Select Id,location from locations where location='" + loc.Text + "'", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                loc.Text = dr["location"].ToString();
                
            }


            string strcon = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection connection = new SqlConnection(strcon);
            connection.Open();
            SqlCommand cmdd = new SqlCommand("select * from Distributorlogin where loc='"+loc.Text+ "' and shopname is not null and role='merchant' ", connection);
            SqlDataReader drr = cmdd.ExecuteReader();
            list1.DataSource = drr;
            list1.DataValueField = "id";
            list1.DataTextField = "shopname";
            list1.DataBind();
            connection.Close();
            list1.Items.Insert(0, new System.Web.UI.WebControls.ListItem("", "0"));
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> getlocation(string prefixText)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["Nirmal"].ToString());
            SqlDataAdapter adp = new SqlDataAdapter(" SELECT location FROM locations where location like'" + prefixText + "%'", con);


            DataTable dt = new DataTable();
            adp.Fill(dt);
            List<string> location = new List<string>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                location.Add(dt.Rows[i]["location"].ToString());
            }
            return location;
        }

        protected void droprole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(droprole.SelectedValue=="1")
            {
                sh.Visible = true;
            }
            else
            {
                sh.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DataQueries.InsertCommon("insert into locations (location) values ('"+locate.Text+"')");
        }
    }
}

