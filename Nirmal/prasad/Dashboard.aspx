﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Nirmal.prasad.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
<style>
    .p {
        text-align:center;        
    }

</style>
    <script src="Chart.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
    <div class="row">
        <div class="col-md-12">
          <div class="box">
        <div class="box-header with-border">
              <h3 class="box-title">DashBoard</h3>
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                </button>
               
              </div>
            </div>
             
              <div class="col-sm-6 col-lg-3" >
                  
                                <div class="overview-item overview-item--c1">
                                     <a href="Distributorslist.aspx">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px">
                                            <div class="icon">
                                               <%-- <i class="zmdi zmdi-account-o"></i>--%>
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                            </div>
                                             <div class="text" style="text-align:center">
                                                <span><b style="color:black">Distributors</b></span>
                                                <h2><b><asp:Label ID="lbldistributor" runat="server" Text="0" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                       
                                    </div></a>
                                </div> 
                            </div>
                 
		
		 <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                  <a href="AdminCategorylist.aspx">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px">
                                           
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                                
                                            </div>
                                            <div class="text" style="text-align:center">
                                                <span><b style="color:black">Categories</b></span>
                                                <h2><b><asp:Label ID="lblcategory" runat="server" Text="0" Font-Size="Large" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                       
                                    </div></a>
                                </div>
             </div>
            
              
        <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <a href="Dealerproducts.aspx">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px" >
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </div>
                                             <div class="text" style="text-align:center">
                                                <span><b style="color:black">Products</b></span>
                                                 <h2><b><asp:Label ID="lblproduc" runat="server" Text="0" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                       
                                    </div> </a>
                                </div>
                            </div>
      
	
		
		

     
        <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c4">
                                    <a href="Orders.aspx">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px">
                                            <div class="icon">
                                                <%--<i class="zmdi zmdi-money"></i>--%>
                                                <i class="zmdi zmdi-shopping-cart" aria-hidden="true"></i>
                                            </div>
                                            <div class="text" style="text-align:center">
                                                <span><b style="color:black">Orders</b></span>
                                                <h2><b><asp:Label ID="lblorders" runat="server" Text="0" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                </div>   </a>
                                        
                                    </div>
                            </div>
 
              <div class="clearfix"></div>   
        <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c4">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px">
                                            <div class="icon">
                                                <%--<i class="zmdi zmdi-money"></i>--%>
                                                <i class="zmdi zmdi-shopping-cart" aria-hidden="true"></i>
                                            </div>
                                            <div class="text" style="text-align:center">
                                                <span><b style="color:black">Pending Orders</b></span>
                                                <h2><b><asp:Label ID="lblpendingorders" runat="server" Text="0" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
              		
              <div class="col-sm-6 col-lg-3" >
                                <div class="overview-item overview-item--c1">
                                    <a href="Dealerlist.aspx">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px">
                                            <div class="icon">
                                               <%-- <i class="zmdi zmdi-account-o"></i>--%>
                                                <i class="zmdi zmdi-calendar-note"" aria-hidden="true"></i>
                                            </div>
                                             <div class="text" style="text-align:center">
                                                <span><b style="color:black">Dealers</b></span>
                                                <h2><b><asp:Label ID="lbldealers" runat="server" Text="0" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                       
                                    </div>   </a>
                                </div>
                            </div>
                       
              
               <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c2">
                                  <a href="Merchentlist.aspx">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px">
                                           
                                            <div class="icon">
                                                <i class="fa fa-users"></i>
                                                
                                            </div>
                                            <div class="text" style="text-align:center">
                                                <span><b style="color:black">Merchants</b></span>
                                                <h2><b><asp:Label ID="lblmerchants" runat="server" Text="0" Font-Size="Large" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                       
                                    </div> </a>
                                </div>
             </div>
                 
                 <div class="col-sm-6 col-lg-3">
                                <div class="overview-item overview-item--c3">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix" style="height:80px" >
                                            <div class="icon">
                                                <i class="fa fa-users"></i>
                                            </div>
                                             <div class="text" style="text-align:center">
                                                <span><b style="color:black">Customers</b></span>
                                                 <h2><b><asp:Label ID="lblcustomers" runat="server" Text="0" ForeColor="Black"></asp:Label></b></h2>
                                            </div>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
       
       

        </div>
            </div>
        </div>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>

        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    </script>
    
   <%-- <asp:Literal ID="ltch" runat="server"></asp:Literal>--%>
</asp:Content>



       
	
		
		

     
     