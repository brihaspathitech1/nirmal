﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
namespace Nirmal.prasad
{
    public partial class Ordersprofit: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                gridbind();
            }
        }
        protected void gridbind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd1 = new SqlCommand("with cte as (select o2.id,o2.date,d.name,d.address,d.shopname,isnull(o2.amount,0) as amount,isnull(o2.actualamount,0) as actualamount,isnull(o2.travelcharges,0) as travelcharges,(((cast(isnull(o2.amount,0) as decimal) -cast(isnull(o2.actualamount,0) as decimal)))-cast(isnull(o2.travelcharges,0) as decimal))as updatedamount from tblorder2 o2 inner join DistributorLogin d on o2.mid=d.id)select c.*, case when b.oid is null then 0 else 1 end status   from cte c full join bills b on b.oid=c.id", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                calsum();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";
            }

        }


        protected void btnitems_Click(object sender, EventArgs e)
        {

            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = grdProduct_Details.DataKeys[grow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("with cte as (select o.id,o.oid, o.date,d.name,d.address,o.amount,o.status,d.mobile,d.loc,(c.name) as CustomerName from  tblorderitem2 o inner join DistributorLogin d on o.Distributor_id=d.id inner join customer c on c.id=o.customer_id where o.status = 'Approved' and o.ordid='" + lblId.Text + "')  select *, substring(cte.date, 0, 11) as dt from cte");
            grid1.DataSource = ds;
            grid1.DataBind();
            modal1.Show();



        }
        protected void calsum()
        {
            double NetAmt = 0;


            foreach (GridViewRow g1 in grdProduct_Details.Rows)
            {

                NetAmt += Convert.ToDouble(g1.Cells[8].Text);

            }
            this.lblTotal.Text = NetAmt.ToString();



            DataSet d = DataQueries.SelectCommon("select sum(cast(amount as decimal(10,2))) as billamount from bills");
            if (d.Tables[0].Rows.Count > 0)
            {
                lblbillamount.Text = d.Tables[0].Rows[0]["billamount"].ToString();
            }
            if (lblbillamount.Text == "")
            {
                lblbillamount.Text = "0";
            }
            lblpending.Text = (decimal.Parse(lblTotal.Text) - decimal.Parse(lblbillamount.Text)).ToString();
        }

        protected void grdProduct_Details_RowDataBound(object sender, GridViewRowEventArgs e)
        {
           
                try
                {
                    GridViewRow grv = e.Row;
                    if (grv.Cells[9].Text.Equals("1"))
                    {
                        e.Row.ForeColor = System.Drawing.Color.Green;
                    }
                    if (grv.Cells[9].Text.Equals("0"))
                    {
                        e.Row.ForeColor = System.Drawing.Color.Black;
                    }
                grv.Cells[9].Visible = false;


                }
                catch (Exception ex)
                {

                }

            
          
        }
    }
}