﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        string cookiee = string.Empty;
        string cookiee1 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie nameCookie = Request.Cookies["fname"];
            HttpCookie nameCookie1 = Request.Cookies["Role"];

            //If Cookie exists fetch its value.
            cookiee = nameCookie != null ? nameCookie.Value.Split('=')[1] : "undefined";
            cookiee1 = nameCookie1 != null ? nameCookie1.Value.Split('=')[1] : "undefined";
            lbluname.Text = "Welcome:" + cookiee;

            if (cookiee1 == "Default")
            {
                admin_profit.Visible = false;
                or1.Visible = false;
                or2.Visible = true;
            }
            else if(cookiee1=="dealer")
            {
                l4.Visible = false;
                l5.Visible = false;
                l6.Visible = false;
                l7.Visible = false;
                l8.Visible = false;
                l10.Visible = false;                
                l13.Visible = false;
                l14.Visible = false;
            }
            else if (cookiee1 == "merchant")
            {
                l4.Visible = false;
                l5.Visible = false;
                l6.Visible = false;
                l7.Visible = false;
                l8.Visible = false;
                l10.Visible = false;             
                l13.Visible = false;
                l14.Visible = false;
            }

        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
           
            Response.Redirect("Default.aspx");
        }
    }
    }
