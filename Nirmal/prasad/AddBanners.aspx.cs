﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nirmal.DataAccessLayer;


namespace Nirmal.prasad
{
    public partial class AddBanners : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Bindgrid();
            }

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            string serverpath = string.Empty;
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn1 = new SqlConnection(connstrg);
            if (Imgprev.HasFile)
            {
                string filename = Path.GetFileName(Imgprev.PostedFile.FileName);
                string path = Server.MapPath("~/images/") + filename;
                Imgprev.PostedFile.SaveAs(path);
                serverpath = @"http://www.gsrnirmal.com/images/" + filename;
            }
            String insert = "insert into tblBanner(content)values('" + serverpath + "')";
            SqlCommand comm1 = new SqlCommand(insert, conn1);

            conn1.Open();
            comm1.ExecuteNonQuery();
            conn1.Close();
            Imgprev.Dispose();
            Bindgrid();

        }
        protected void Bindgrid()
        {

            DataSet ds = DataQueries.SelectCommon("select * from tblBanner order by id desc");
           
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridLogin.DataSource = ds;

                GridLogin.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                GridLogin.DataSource = ds;
                GridLogin.DataBind();
                int columncount = GridLogin.Rows[0].Cells.Count;
                GridLogin.Rows[0].Cells.Clear();
                GridLogin.Rows[0].Cells.Add(new TableCell());
                GridLogin.Rows[0].Cells[0].ColumnSpan = columncount;
                GridLogin.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void btndelete_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            string id = btn.CommandArgument;
            DataQueries.InsertCommon("Delete from tblBanner where id='" + id + "'");

            Bindgrid();
        }
    }
}