﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;

namespace Nirmal.prasad
{
    public partial class Orderspagefordefault : System.Web.UI.Page
    {
        string cookiee1 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            gridbind();
            HttpCookie nameCookie1 = Request.Cookies["Role"];
            cookiee1 = nameCookie1 != null ? nameCookie1.Value.Split('=')[1] : "undefined";
            // calsum();


        }
        protected void gridbind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd1 = new SqlCommand(" with cte as (select o.id, o.date,d.name,d.address,o.amount,o.status,isnull(o.updatedamount,0)as updatedamount,d.mobile,d.loc,(c.name) as CustomerName from  tblorder o inner join DistributorLogin d on o.Distributor_id=d.id inner join customer c on c.id=o.customer_id where o.status = 'Approved')  select *, substring(cte.date, 0, 11) as dt from cte", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (cookiee1 == "Default")
                {
                    this.grdProduct_Details.Columns[5].Visible = false;
                    grdProduct_Details.DataSource = ds;
                    grdProduct_Details.DataBind();
                }
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
        protected void Ordersbind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd1 = new SqlCommand("with cte as (select o.id, o.date,d.name,d.address,o.amount,o.status,o.updatedamount,d.mobile,d.loc from  tblorder o inner join DistributorLogin d on o.Distributor_id=d.id ) select *, substring(cte.date, 0, 11) as dt from cte where cast(date as date) between '" + TAN.Text.Trim() + "'and '" + TextBox1.Text.Trim() + "'", conn44);
            if (TAN.Text.ToString() == "")
            {
                cmd1.Parameters.Add("@date2", SqlDbType.VarChar).Value = DBNull.Value;
            }
            else
            {
                cmd1.Parameters.Add("@date2", SqlDbType.VarChar).Value = TextBox1.Text;

            }

            if (TextBox1.Text.ToString() == "")
            {
                cmd1.Parameters.Add("@date3", SqlDbType.VarChar).Value = DBNull.Value;
            }
            else
            {
                cmd1.Parameters.Add("@date3", SqlDbType.VarChar).Value = TextBox1.Text;

            }
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void btnitems_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = grdProduct_Details.DataKeys[grow.RowIndex].Value.ToString();
            //lblUser.Text = grdProduct_Details.DataKeys[grow.RowIndex].Values["user_id"].ToString();
            //lblqua.Text = grdOrders.DataKeys[grow.RowIndex].Values["quantity"].ToString();

            //name.Text = grow.Cells[0].Text.Replace("&nbsp;", "");
            DataSet ds = DataQueries.SelectCommon("select a.productname,a.units,a.mrp,oi.count,convert(float,a.mrp)*oi.count as  totalcost from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id  where o.id = '" + lblId.Text + "'");


            GridView1.DataSource = ds;
            GridView1.DataBind();

            //DataSet d = DataQueries.SelectCommon(" select tpr.name as pname,q.quantity, o.[id],[date],[address],o.[status],[amount],[user_id],[paymentmode],[coupon_id],(select coupon_code from tfood.tblcoupon where id = o.coupon_id) as coupon,(select name from[tfood].[tfood].[tbluser] where id = o.user_id)as name,(select mobile from[tfood].[tfood].[tbluser] where id = o.user_id)as mobile,(select name from tblcity where id=(select city_id  from tblAdmins where Admin_id=o.admin_id)) as city from[tfood].[tfood].[tblorder] o inner join tblorderitem tbr on tbr.order_id=o.id inner join tblproduct tpr on tbr.product_id=tpr.id inner join tblQuantity q on q.id=tbr.quantity_id where o.id='" + lblId.Text + "'");
            //if (d.Tables[0].Rows.Count > 0)
            //{
            //    lblname.Text = d.Tables[0].Rows[0]["name"].ToString();
            //    lbladdress.Text = d.Tables[0].Rows[0]["address"].ToString();
            //    lblamont.Text = d.Tables[0].Rows[0]["amount"].ToString();
            //    lbldate.Text = d.Tables[0].Rows[0]["date"].ToString();
            //    lblmobl.Text = d.Tables[0].Rows[0]["mobile"].ToString();
            //    lblcoupan.Text = d.Tables[0].Rows[0]["coupon"].ToString();
            //    lblpmntmod.Text = d.Tables[0].Rows[0]["paymentmode"].ToString();
            //    lblordrno.Text = d.Tables[0].Rows[0]["id"].ToString();
            //}
            DataSet d = DataQueries.SelectCommon("select o.id,o.date,d.name,d.mobile,(c.name) as custname,(c.address) as custaddress,o.amount,o.status,(c.mobile) as custmobile,(c.pincode) as pincode from  tblorder o inner join customer c  on o.Customer_id=c.id   inner join DistributorLogin d on o.Distributor_id=d.id where o.id='" + lblId.Text + "'");
            if (d.Tables[0].Rows.Count > 0)
            {
                lblpname.Text = d.Tables[0].Rows[0]["date"].ToString();
                lblunits.Text = d.Tables[0].Rows[0]["name"].ToString();
                lblprice.Text = d.Tables[0].Rows[0]["status"].ToString();
                lblcount.Text = d.Tables[0].Rows[0]["amount"].ToString();
                moble.Text = d.Tables[0].Rows[0]["mobile"].ToString();
                lblcustname.Text = d.Tables[0].Rows[0]["custname"].ToString();
                lblcustmobile.Text = d.Tables[0].Rows[0]["custmobile"].ToString();
                lblcustaddress.Text = d.Tables[0].Rows[0]["custaddress"].ToString();
                lblpincode.Text = d.Tables[0].Rows[0]["pincode"].ToString();
                lblorderid.Text = d.Tables[0].Rows[0]["id"].ToString();

            }

            ModalPopupExtender2.Show();
        }
        protected void calsum()
        {
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //base.VerifyRenderingInServerForm(control);
        }


        protected void pdf_click_Click(object sender, EventArgs e)
        {


            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=OrdersReport.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            lbname.RenderControl(hw);
            lblpname.RenderControl(hw);
            lbunits.RenderControl(hw);
            lblunits.RenderControl(hw);
            lblcname.RenderControl(hw);
            lblcustname.RenderControl(hw);
            lbprice.RenderControl(hw);
            lblprice.RenderControl(hw);
            lblcmobile.RenderControl(hw);
            lblcustmobile.RenderControl(hw);
            lblcaddress.RenderControl(hw);
            lblcustaddress.RenderControl(hw);
            lbcount.RenderControl(hw);
            lblcount.RenderControl(hw);
            mobi.RenderControl(hw);
            moble.RenderControl(hw);
            GridView1.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
            GridView1.AllowPaging = true;
            GridView1.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Ordersbind();
        }

        protected void btndetails_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow grow = (GridViewRow)btn.NamingContainer;
            lblId.Text = grdProduct_Details.DataKeys[grow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("select amount,actualamount,adminprof from tblorder where id = '" + lblId.Text + "'");
            GridView2.DataSource = ds;
            GridView2.DataBind();
            ModalPopupExtender1.Show();


        }
    }
}