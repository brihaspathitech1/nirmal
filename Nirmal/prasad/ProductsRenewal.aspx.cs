﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class ProductsRenewal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                productsbind();
            }
        }




        protected void productsbind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Addproduct where status='0'", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdRenewal.DataSource = ds;
                grdRenewal.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdRenewal.DataSource = ds;
                grdRenewal.DataBind();
                int columncount = grdRenewal.Rows[0].Cells.Count;
                grdRenewal.Rows[0].Cells.Clear();
                grdRenewal.Rows[0].Cells.Add(new TableCell());
                grdRenewal.Rows[0].Cells[0].ColumnSpan = columncount;
                grdRenewal.Rows[0].Cells[0].Text = "No Records Found";
            }
        }



        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblId.Text = grdRenewal.DataKeys[gRow.RowIndex].Value.ToString();

            
            SqlCommand cmd = new SqlCommand("Select * from Addproduct where id='" + lblId.Text + "'", conn44);
            conn44.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                txtexpiry.Text = dr["expirydate"].ToString();
                txtcharge.Text = dr["maintenance"].ToString();
                txtsuplcost.Text = dr["supproductcost"].ToString();
                
            }
            conn44.Close();
           
            this.ModalPopupExtender2.Show();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            DataSet ds = DataQueries.SelectCommon("select productname from Addproduct where Id='" + lblId.Text + "'");
            
            String Update = "update  [Addproduct] set expirydate='" + txtexpiry.Text + "',maintenance='" + txtcharge.Text + "',supproductcost='" +txtsuplcost.Text + "',status='1'  where Id='" + lblId.Text + "'";
            SqlCommand comm = new SqlCommand(Update, conn);
            //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            //SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            //comm1.ExecuteNonQuery();
            conn.Close();
            //Response.Redirect("ProductsRenewal.aspx");
            productsbind();
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            LinkButton btnsubmit = sender as LinkButton;
            GridViewRow gRow = (GridViewRow)btnsubmit.NamingContainer;
            lblId.Text = grdRenewal.DataKeys[gRow.RowIndex].Value.ToString();
            DataSet ds = DataQueries.SelectCommon("select productname from Addproduct where Id='" + lblId.Text + "'");
            string connstrg = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            SqlConnection conn = new SqlConnection(connstrg);
            String Delete = "delete from [Addproduct]  where Id='" + lblId.Text + "'";
            SqlCommand comm = new SqlCommand(Delete, conn);
            //String Update1 = "update [Visitors] set outtime='" + DateTime.Now.ToString("MM/dd/yyy hh:mm:ss") + "' where Id='" + Label1.Text + "'";
            //SqlCommand comm1 = new SqlCommand(Update1, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            //comm1.ExecuteNonQuery();
            conn.Close();
            Response.Redirect("ProductsRenewal.aspx");
        }
    }
}