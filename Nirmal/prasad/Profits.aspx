﻿<%@ Page Title="" Language="C#" MasterPageFile="~/prasad/Admin.Master" AutoEventWireup="true" CodeBehind="Profits.aspx.cs" Inherits="Nirmal.prasad.Profits" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
       .GridViewHeaderStyle th {   text-align: center; }
   </style>
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
   
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><b>Profits Details</b></h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>
               
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">

                                    <div class="col-md-3">
                                        <label>From Date</label>
                                       <asp:TextBox ID="txtFrom" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server"
                                             TargetControlID="txtFrom" Format="yyyy-MM-dd" />
                                    </div>
                            <div class="col-md-3">
                                        <label>To Date</label>
                                         <asp:TextBox ID="txtTo" class="form-control" runat="server" AutoComplete="off"></asp:TextBox>
                                        <cc2:CalendarExtender ID="CalendarExtender1" runat="server"
                                             TargetControlID="txtTo" Format="yyyy-MM-dd" />
                                    </div>                                
                                    <div class="col-md-2" style="padding-top:25px">                                
                                        <asp:Button ID="btnsubmit" class="btn btn-success" runat="server" Text="Submit" OnClick="btnsubmit_Click" />                                     
                                    </div>

                                    </div>
                               
                              </div>
                                <br />
                    <div class="row" runat="server" id="admindiv">
                        <div class="cl-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="lbladminproftext" runat="server"><h4>Total AdminProfit:</h4></asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lbladminprof" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="lblmonthadminproftext" runat="server"><h4>This Month AdminProfit:</h4></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="lblmonthadminprof" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <br />
                     <div class="row"  id="ordersdiv" runat="server">
                        <div class="cl-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="lbltotordersamounttext" runat="server"><h4>Total OrdersAmount:</h4></asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="lbltotordersamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="lblmonthordersamounttext" runat="server"><h4>This Month OrdersAmount:</h4></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="lblmonthordersamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                        </div>
                    </div> <br />
                    <br />
                                <br />
                     <div class="row" id="dealersdiv" runat="server">
                        <div class="cl-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="totdealprofitamounttext" runat="server"><h4>Total Dealers ProfitAmount:</h4></asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="totdealprofitamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="thismonthdealprofitamounttext" runat="server"><h4>This Month Dealers ProfitAmount:</h4></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="thismonthdealprofitamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                        </div>
                    </div>
                            <br /> <br />
                    <br />
                     <div class="row" id="distdiv" runat="server">
                        <div class="cl-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="totdistprofitamounttext" runat="server"><h4>Total Distributors ProfitAmount:</h4></asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="totdistprofitamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="thismonthdistprofitamounttext" runat="server"><h4>This Month Distributors ProfitAmount:</h4></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="thismonthdistprofitamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                        </div>
                    </div>
                              <br /> <br />
                    <br />
                     <div class="row" id="merchantsdiv" runat="server">
                        <div class="cl-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="totmerchamounttext" runat="server"><h4>Total Merchants Amount:</h4></asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="totmerchamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="tismonthmerchamounttext" runat="server"><h4>This Month Merchants Amount:</h4></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="tismonthmerchamount" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <br /> <br />
                    <br />
                     <div class="row" id="traveldiv" runat="server">
                        <div class="cl-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="tottravelchargestext" runat="server"><h4>Total TravelChareges:</h4></asp:Label>
                            </div>
                            <div class="col-md-2">
                                <asp:Label ID="tottravelcharges" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="thismonthtravelchargetext" runat="server"><h4>This Month TravelCharges:</h4></asp:Label>
                            </div>
                            <div class="col-md-3">
                                <asp:Label ID="thismonthtravelcharge" runat="server" ForeColor="Blue" Font-Size="Large"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                     <div class="row">
                         <div class="col-md-4">
                                <asp:Label ID="lbldateadminprofittext" runat="server" Visible="false"><h4>Date Wise AdminProfit:</h4></asp:Label>
                              </div>
                         <div class="col-md-2">
                         <asp:Label ID="lbldateadminprofit" runat="server" ForeColor="Blue" Font-Size="Large" Visible="false"></asp:Label>
                   </div> 
                     <div class="col-md-4">
                     <asp:Label ID="lbldateordersamounttext" runat="server" Visible="false"><h4>Date Wise OrdersAmount:</h4></asp:Label>
                               </div>
                    <div class="col-md-2"><asp:Label ID="lbldateordersamount" runat="server" ForeColor="Blue" Font-Size="Large" Visible="false"></asp:Label>
                   </div>
                         </div>
                        </div><br />
                    <br />
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                     <asp:Label ID="lbldatedealerprofittext" runat="server" Visible="false"><h4>Date Wise DealerProfit:</h4></asp:Label>
                               </div><div class="col-md-2"> <asp:Label ID="lbldatedealerprofit" runat="server" ForeColor="Blue" Font-Size="Large" Visible="false"></asp:Label>
                    </div> <div class="col-md-4">
                     <asp:Label ID="lbldatedistprofittext" runat="server" Visible="false"><h4>Date Wise DistributorProfit:</h4></asp:Label>
                             </div><div class="col-md-2">    <asp:Label ID="lbldatedistprofit" runat="server" ForeColor="Blue" Font-Size="Large" Visible="false"></asp:Label>
                     
                   </div> </div></div><br />
                    <br />
                      <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                     <asp:Label ID="lbldatemerchantsaletext" runat="server" Visible="false"><h4>Date Wise MerchantSale:</h4></asp:Label>
                             </div><div class="col-md-2">   <asp:Label ID="lbldatemerchantsale" runat="server" ForeColor="Blue" Font-Size="Large" Visible="false"></asp:Label>
                    </div>
                            <div class="col-md-4">
                     <asp:Label ID="lbldatetravelchargestext" runat="server" Visible="false"><h4>Date Wise Travelcharges:</h4></asp:Label>
                        </div><div class="col-md-2">       <asp:Label ID="lbldatetravelcharges" runat="server" ForeColor="Blue" Font-Size="Large" Visible="false"></asp:Label>
                        </div> </div></div></div>
                    </div>
                  
            <asp:Label ID="o2total" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="atotal" runat="server" Visible="false"></asp:Label>
             <asp:Label ID="o2thismonthtotal" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="athismonthtotal" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="o2datetotal" runat="server" Visible="false"></asp:Label>
            <asp:Label ID="adatetotal" runat="server" Visible="false"></asp:Label>
               
                </div>
            </div>
        
</asp:Content>