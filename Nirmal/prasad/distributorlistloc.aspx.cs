﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Nirmal.DataAccessLayer;
using System.Configuration;
namespace Nirmal.prasad
{
    public partial class distributorlistloc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bind_Distributors();
            }
            lbldslctname.Visible = false;
            lblloc.Visible = false;
            lbldlocation.Visible = false;

        }
        protected void bind_Distributors()
        {
            DataSet ds = DataQueries.SelectCommon("select id,name from DistributorLogin where role='dealer'");
            ddlAdmins.DataTextField = "name";
            ddlAdmins.DataValueField = "id";
            ddlAdmins.DataSource = ds;
            ddlAdmins.DataBind();
            //ddlAdmins.Items.Insert(0, "-Select Distributor-");
        }





        protected void btnsubmit_Click(object sender, EventArgs e)
        {

        }

        protected void ddlAdmins_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbldslctname.Visible = true;
            lblloc.Visible = true;
            lbldlocation.Visible = true;
            string loc = string.Empty;
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            DataSet d = DataQueries.SelectCommon("select name,loc from DistributorLogin where name='" + ddlAdmins.SelectedItem.Text + "' and role='dealer'");
            if (d.Tables[0].Rows.Count > 0)
            {
                loc = d.Tables[0].Rows[0]["loc"].ToString();
            }
            lbldlocation.Text = loc;
            grid(loc);
            
        }

        public void grid(string loc)
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            
            SqlCommand cmd = new SqlCommand("select * from DistributorLogin where loc='"+loc+"' and role='distributor' order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdcustdetails.DataSource = ds;

                grdcustdetails.DataBind();

            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdcustdetails.DataSource = ds;
                grdcustdetails.DataBind();
                int columncount = grdcustdetails.Rows[0].Cells.Count;
                grdcustdetails.Rows[0].Cells.Clear();
                grdcustdetails.Rows[0].Cells.Add(new TableCell());
                grdcustdetails.Rows[0].Cells[0].ColumnSpan = columncount;
                grdcustdetails.Rows[0].Cells[0].Text = "No Records Found";

            }
        }

    }
}