﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Nirmal.DataAccessLayer;

namespace Nirmal.prasad
{

    public partial class Admintomerchant : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            merchantlist();
        }
        protected void merchantlist()
        {
            DataSet ds=DataQueries.SelectCommon("select * from DistributorLogin where role='merchant'");
            gridmerchants.DataSource = ds;
            gridmerchants.DataBind();
        }

        protected void gridmerchants_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gridmerchants, "Select$" + e.Row.RowIndex);
                e.Row.Attributes["style"] = "cursor:pointer";
            }

        }

        protected void gridmerchants_SelectedIndexChanged(object sender, EventArgs e)
        {
            merclist.Visible = false;
            distlist.Visible = true; 
            search.Visible = false;
            search1.Visible = true;
            string lid = gridmerchants.SelectedRow.Cells[1].Text;
            DataSet ds = DataQueries.SelectCommon("select * from DistributorLogin where role='distributor' and shopname='" + lid + "'");
            griddistributors.DataSource = ds;
            griddistributors.DataBind();
        }
    }
}