﻿using Nirmal.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal.prasad
{
    public partial class Categorylist : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bind();

        }
        protected void bind()
        {
            SqlConnection conn44 = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
            conn44.Open();
            SqlCommand cmd = new SqlCommand("select * from Category order by id desc", conn44);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            conn44.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                grdProduct_Details.DataSource = ds;
                grdProduct_Details.DataBind();
                int columncount = grdProduct_Details.Rows[0].Cells.Count;
                grdProduct_Details.Rows[0].Cells.Clear();
                grdProduct_Details.Rows[0].Cells.Add(new TableCell());
                grdProduct_Details.Rows[0].Cells[0].ColumnSpan = columncount;
                grdProduct_Details.Rows[0].Cells[0].Text = "No Records Found";
            }
        }
    }
}