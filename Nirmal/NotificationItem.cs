﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nirmal
{
    public class NotificationItem
    {
        public string title { get; set; }

        public string body { get; set; }

        public string image { get; set; }


        public int code { get; set; }
    }
}