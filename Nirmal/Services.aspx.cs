﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nirmal.DataAccessLayer;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;
namespace Nirmal
{
    public partial class Services : System.Web.UI.Page
    {
        string constr = "Data Source=103.233.79.22;Initial Catalog=Nirmal;User ID=Nirmal;Password=Brihaspathi@123";
        protected void Page_Load(object sender, EventArgs e)

        {


            if (Request.HttpMethod == "POST")
            {
                try
                {
                    string request = Request.Params["request"].ToString();
                    switch (request)
                    {

                        case "login":
                            login();
                            break;
                        case "cat_list":
                            cat();
                            break;
                        case "banner":
                            banner();
                            break;
                        case "productlist":
                            productlist();
                            break;
                        case "plist":
                            plist();
                            break;
                        case "addcart":
                            addcart();
                            break;

                        case "cart_status":
                            cart_status();
                            break;

                        case "decrementcart":
                            decrementcart();
                            break;
                        case "cart":
                            cart();
                            break;
                        case "cartsubtotal":
                            cartsubtotal();
                            break;
                        case "removecart":
                            removecart();
                            break;
                        case "addcustomer":
                            Addcustomer();
                            break;
                        case "order":
                            order();
                            break;
                        case "distributors_list":
                            distributors_list();
                            break;
                        case "btwndate_distributors_list":
                            btwndate_distributors_list();
                            break;
                        case "date_distributors_list":
                            date_distributors_list();
                            break;
                        case "order_items":
                            order_items();
                            break;
                        case "orders_list":
                            orders_list();
                            break;
                        case "btwndate_orders_list":
                            btwndate_orders_list();
                            break;
                        case "date_orders_list":
                            date_orders_list();
                            break;
                        case "Distributorlist":
                            Distributorlist();
                            break;
                        case "Distributer_orders_list":
                            Distributer_orders_list();
                            break;
                        case "distributer_order_items":
                            distributer_order_items();
                            break;
                        case "order_rejected":
                            order_rejected();
                            break;
                        case "order_approved":
                            order_approved();
                            break;
                        case "Bill_order_items":
                            Bill_order_items();
                            break;
                        case "items_count":
                            items_count();
                            break;
                        case "Bill_Customer_Details":
                            Bill_Customer_Details();
                            break;
                        case "customerdetails":
                            customerdetails();
                            break;
                        case "Broadcast_messages":
                            broadcast();
                            break;
                        case "token":
                            token();
                            break;
                        case "contacts":
                            contacts();
                            break;
                        case "productsearch":
                            productsearch();
                            break;
                        case "custcontacts":
                            custcontacts();
                            break;
                        case "binddetails":
                            binddetails();
                            break;
                        case "merchantreg":
                            dealeraddsmerchant();
                            break;
                        case "shpname":
                            shopnamesbind();
                            break;
                        case "addproducts":
                            addingproductsthroughapp();
                            break;
                        case "suggestion":
                            suggestion();
                            break;
                        case "suggdisplay":
                            suggdisplay();
                            break;
                        case "list":
                            approvelist();
                            break;
                        case "active":
                            inactive();
                            break;
                        case "activelogins":
                            activelogins();
                            break;
                        case "categorybind":
                            categorybind();
                            break;
                        case "mandals":
                            mandals();
                            break;
                        case "approvedmandal":
                            approvemandal();
                            break;
                        case "mandaldistributors":
                            mandaldistributors();
                            break;
                        case "mandaldistributorsorders":
                            mandalorders();
                            break;
                        case "sendingpproveditem":
                            sendingapproved();
                            break;
                        case "distributorslisttomerchant":
                            displaydistributorstomerchant();
                            break;
                        case "orderslisttomerchant":
                            displayorderslisttomerchant();
                            break;
                        case "profitdisplay":
                            profitdisplay();
                            break;
                        case "travelcharges":
                            includetravelcharges();
                            break;

                        case "merchantorderitems":
                            merchant_order_items();
                            break;
                        case "sellingprofittoadmin":
                            sendtoadmin();
                            break;
                        case "distributorprofitloc":
                            distributorprofitloc();
                            break;
                        case "distributorprofitnames":
                            distributorprofitnames();
                            break;
                        case "distributorprofitorders":
                            distributorprofitordersids();
                            break;
                        case "merchanttoadminprofit":
                            orderitemslist();
                            break;
                        case "amountdisplaytoadmin":
                            profitamounttoadmin();
                            break;
                        case "sendproductslist":
                            sendproductslist();
                            break;


                        case "sendingprofittodistributor":
                            clear();
                            break;
                        case "dealeraddproducts":
                            dealeraddproducts();
                            break;
                        case "distributororderlist":
                            distributororderlist();
                            break;
                        case "distributorprofitamount":
                            distributorprofiamount();
                            break;
                        case "distributorprofit":
                            distributorprofit();
                            break;
                        case "sendbills":
                            sendbills();
                            break;
                        case "displaybills":
                            displaybills();
                            break;
                        case "totaldistprofit":
                            totaldistprofit();
                            break;
                        case "distaddress":
                            distaddress();
                            break;
                        case "fapprovelist":
                            final1();
                            break;
                        case "merchantaddsorderids":
                            inserttblorder2();
                            break;
                        case "merchanttoadmin":
                            merchanttoadmin();
                            break;
                        case "distributorsprofitorderslist":
                            distributorsprofitorders();
                            break;
                        case "distaddedcustlist":
                            distaddedcustlist();
                            break;
                        case "merchantdetailstodealer":
                            merchantdetailstodealer();
                            break;
                        case "distdetailstodealer":
                            distdetailstodealer();
                            break;
                        case "distordercount":
                            distordercount();
                            break;
                        case "dealerprofit":
                            dealerprofit();
                            break;
                        case "totaldealerprofit":
                            totaldealerprofit();
                            break;
                        case "distsubprofit":
                            distsubprofit();
                            break;

                        case "ordercompleted":
                            ordercompleted();
                            break;
                        case "orderidtomerchantforbill":
                            orderidtomerchantforbill();
                            break;
                        case "selectshopname":
                            shopnames();
                            break;
                        case "delivery":
                            delivery();
                            break;
                        case "itemslisttomerchant":
                            itemslist();
                            break;
                        case "deliverylist":
                            deliverylist();
                            break;
                        case "deliveryorderdetails":
                            deliveryorderdetails();
                            break;
                        case "distributortodealer":
                            distributortodealer();
                            break;

                        case "dealerappprovedorderslist":
                            dealerappprovedorderslist();
                            break;
                        case "dealerorderitems":
                            dealerorderitems();
                            break;
                        case "displaydealerorders":
                            displaydealerorders();
                            break;
                        case "custdetailstodealer":
                            custdetailstodealer();
                            break;

                        // ****------------->NFDB Services<------------------**** //

                        case "RecentNews":
                            Recentnews();
                            break;
                        case "Gallery":
                            Gallery();
                            break;
                        case "sendimage":
                            sendimage();
                            break;
                        case "tenderspdf":
                            tenderspdf();
                            break;
                        case "URL":
                            URL();
                            break;
                        case "electionlogin":
                            electionlogin();
                            break;
                        case "electionlogout":
                            electionlogout();
                            break;
                        case "electiondisplay":
                            electiondisplay();
                            break;
                        case "countlogins":
                            countlogins();
                            break;
                        case "Aplog":
                            Aplog();
                            break;
                        case "Aplogout":
                            Aplogout();
                            break;
                        case "Apdisplay":
                            Apdisplay();
                            break;
                        case "Apcount":
                            Apcount();
                            break;
                        case "testcat":
                            testcat();
                            break;
                        case "testprod":
                            testprod();
                            break;
                        case "productslist":
                            descrip();
                            break;
                        case "testimage":
                            testimage();
                            break;

                        case "testt":
                            testt();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Response.Write("{\"error\": true,\"result\":\"Server Error=" + ex.Message.ToString() + "\"}");
                }
            }
        }
        private void token()
        {
            string user_id = Request.Params["user_id"].ToString().Trim();
            string token = Request.Params["token"].ToString().Trim();
            DataQueries.UpdateCommon("update DistributorLogin set token='" + token + "' where id='" + user_id + "'");
            Response.Write("{\"error\": false ,\"message\":\"success\"}");

        }
        protected void broadcast()
        {
            string title = Request.Params["title"].ToString();
            string description = Request.Params["description"].ToString();
            NotificationItem notification = new NotificationItem();

            notification.title = title;
            notification.body = description;
            notification.image = "";
            notification.code = 0;
            NotificationOperation msg = new NotificationOperation();
            msg.sendTopicNotification("news", notification);
            Response.Write("{\"error\": true ,\"message\":\"Send successfully.\"}");

        }
        protected void login()
        {
            string uname = Request.Params["username"].ToString().Trim();
            string password = Request.Params["password"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select * from DistributorLogin where username='" + uname + "' and password='" + password + "' and status=1");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["id"].ToString().Trim();
                string name = ds.Tables[0].Rows[0]["name"].ToString().Trim();
                string mobile = ds.Tables[0].Rows[0]["mobile"].ToString().Trim();
                string email = ds.Tables[0].Rows[0]["email"].ToString().Trim();
                string role = ds.Tables[0].Rows[0]["role"].ToString().Trim();
                string location = ds.Tables[0].Rows[0]["loc"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"id\":\"" + id + "\",\"mobile\":\"" + mobile + "\",\"email\":\"" + email + "\",\"role\":\"" + role + "\",\"loc\":\"" + location + "\"}");
            }
        }



        protected void shopnames()
        {
            string did = Request.Params["distid"].ToString();
            string shp = string.Empty;
            string shp1 = string.Empty;
            DataSet dss = DataQueries.SelectCommon("WITH tmp( id,shopname,String) AS(SELECT id,LEFT(shopname, CHARINDEX(',', shopname + ',') - 1),STUFF(shopname, 1, CHARINDEX(',', shopname + ','), '') FROM distributorlogin where id='" + did + "' UNION all SELECT id,LEFT(String, CHARINDEX(',', String + ',') - 1),STUFF(String, 1, CHARINDEX(',', String + ','), '') FROM tmp WHERE String > '') SELECT t.shopname,d.image,d.id as shopid FROM tmp t inner join DistributorLogin d on d.shopname=t.shopname where d.role='merchant'");
            DataTable table = dss.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }
        protected void cat()
        {
            string did = Request.Params["distid"].ToString();
            string sh = Request.Params["shopname"].ToString();
            string lblpname = string.Empty;
            DataSet d = DataQueries.SelectCommon("select did from active");
            if (d.Tables[0].Rows.Count > 0)
            {
                lblpname = d.Tables[0].Rows[0]["did"].ToString();
            }

            if (did == lblpname)
            {
                DataQueries.UpdateCommon("update active set status='1' where did='" + did + "'");
            }
            else
            {
                DataQueries.InsertCommon("insert into active(did,status) values('" + did + "','1')");
            }
            // DataSet ds = DataQueries.SelectCommon("select id,categoryname,image from category ");
            DataSet ds = DataQueries.SelectCommon("select distinct c.id,c.categoryname,c.image from category c inner join addproduct a on c.id=a.categoryid where shopname='" + sh + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }
        public void addcart()
        {
            string pid = Request.Params["product_id"].ToString();
            string uid = Request.Params["user_id"].ToString();
            string shopid = Request.Params["shopid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select id,count from tblcart where product_id='" + pid + "' and user_id='" + uid + "'");
            if (ds.Tables[0].Rows.Count == 0)
            {
                DataQueries.InsertCommon("insert into tblcart(product_id,count,user_id,shopid) values ('" + pid + "','1','" + uid + "','" + shopid + "')");
                Response.Write("{\"message\":\"Success\",\"count\":1}");

            }

            else
            {

                string count = ds.Tables[0].Rows[0]["count"].ToString();
                int value = int.Parse(count.ToString());

                int increment = value + 1;

                count = increment.ToString();
                DataQueries.UpdateCommon("update tblcart set count='" + count + "' where product_id='" + pid + "' and user_id='" + uid + "'");
                Response.Write("{\"message\":\"Success\",\"count\":" + count.Trim().ToString() + "}");
            }
        }
        public void decrementcart()
        {
            string pid = Request.Params["product_id"].ToString();
            string uid = Request.Params["user_id"].ToString();
            string count = string.Empty;
            SqlConnection conn = new SqlConnection(constr);
            conn.Open();
            SqlCommand cmd = new SqlCommand("select * from tblcart where product_id='" + pid + "' and user_id='" + uid + "'", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                count = dr["count"].ToString();

            }
            conn.Close();
            int value = int.Parse(count.ToString());
            int increment = value - 1;
            count = increment.ToString();
            if (count != "0")
            {
                DataQueries.UpdateCommon("update tblcart set count='" + count + "' where product_id='" + pid + "' and user_id='" + uid + "' ");
                Response.Write("{\"message\":\"Success\",\"count\":\"" + count.Trim().ToString() + "\"}");
            }
            else
            {
                DataQueries.DeleteCommon("delete from tblCart where product_id='" + pid + "' and user_id='" + uid + "'");
                Response.Write("{\"message\":\"your cart is deleted \",\"count\":\"" + count.Trim().ToString() + "\"}");
            }
        }
        public void items_count()
        {
            string pid = Request.Params["product_id"].ToString();
            string uid = Request.Params["user_id"].ToString();
            string count = Request.Params["count"].ToString();
            string avaiable = string.Empty;
            string cartcnt = string.Empty;
            DataSet ds1 = DataQueries.SelectCommon("select price,(select COUNT(*) from tblcart where product_id='" + pid + "' and user_id='" + uid + "') as cartcount from Addproduct where id='" + pid + "' ");
            if (ds1.Tables[0].Rows.Count > 0)
            {
                // avaiable = ds1.Tables[0].Rows[0]["available"].ToString().Trim();
                cartcnt = ds1.Tables[0].Rows[0]["cartcount"].ToString().Trim();
            }
            // int cnt = int.Parse(count);
            // int avl = int.Parse(avaiable);
            // if (cnt > avl)
            // {
            //      DataQueries.DeleteCommon("delete from tblCart where product_id='" + pid + "' and user_id='" + uid + "'");
            //    Response.Write("{\"error\":true,\"message\":\"count is greater then avilable quantity\",\"count\":\"" + count.Trim().ToString() + "\",\"Cartcount\":\"" + cartcnt.Trim().ToString() + "\",\"itemsavailable\":\"" + avaiable.Trim().ToString() + "\"}");
            // }

            DataSet ds = DataQueries.SelectCommon("select id,count from tblcart where product_id='" + pid + "' and user_id='" + uid + "'");
            if (ds.Tables[0].Rows.Count == 0)
            {
                DataQueries.InsertCommon("insert into tblcart(product_id,count,user_id) values ('" + pid + "','" + count + "','" + uid + "')");
                Response.Write("{\"error\":false,\"count\":\"" + count.Trim().ToString() + "\",\"Cartcount\":\"" + cartcnt.Trim().ToString() + "\"}");
            }

            else
            {
                DataQueries.UpdateCommon("update tblcart set count='" + count + "' where product_id='" + pid + "' and user_id='" + uid + "'");
                Response.Write("{\"error\":false,\"count\":\"" + count.Trim().ToString() + "\",\"Cartcount\":\"" + cartcnt.Trim().ToString() + "\"}");
            }

        }
        public void cart_status()
        {
            string user_id = Request.Params["user_id"].ToString();

            string command = string.Empty;

            command = "select sum(convert(int,p.price))  as total,(select COUNT(c.id) from tblcart c inner join Addproduct p on p.id=c.product_id where c.user_id='" + user_id + "') as count from tblcart c inner join Addproduct p on c.product_id=p.id where c.user_id='" + user_id + "'";
            DataSet ds = DataQueries.SelectCommon(command);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string total = ds.Tables[0].Rows[0]["total"].ToString();
                string count = ds.Tables[0].Rows[0]["count"].ToString();
                Response.Write("{\"error\":false,\"count\":" + count + "}");
            }
            else
            {
                Response.Write("{\"error\":true,\"count\":" + 0 + "}");
            }
        }
        public void cartsubtotal()
        {
            string user_id = Request.Params["user_id"].ToString();
            string shopid = Request.Params["shopid"].ToString();
            string command = string.Empty;

            command = "select sum(cast(a.mrp as float)*cast(c.count as float)) as ctotal,sum(cast(a.price as float)*cast(c.count as float)) as actualamount from tblcart c inner join Addproduct a on c.product_id=a.id where c.user_id='" + user_id + "' and c.shopid='" + shopid + "'";
            DataSet ds = DataQueries.SelectCommon(command);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string total = ds.Tables[0].Rows[0]["ctotal"].ToString();
                string amt = ds.Tables[0].Rows[0]["actualamount"].ToString();

                Response.Write("{\"error\":false,\"ctotal\":" + total + ",\"actualamount\":" + amt + "}");
            }
            else
            {
                Response.Write("{\"error\":true,\"ctotal\":" + 0 + ",\"actualamount\":" + 0 + "}");
            }
        }
        protected void banner()
        {
            //DataSet ds = DataQueries.SelectCommon("with cte  as (SELECT top 100000 COUNT(o.Distributor_id)  as id,i.image as content,DENSE_RANK() OVER(order BY  COUNT(o.distributor_id) ) AS top3 FROM tblorder o inner join Distributorlogin i on i.id=o.Distributor_id where Date >= DATEADD(day, 1 - DATEPART(dw, GETDATE()), CONVERT(DATE, GETDATE())) AND Date < DATEADD(day, 8 - DATEPART(dw, GETDATE()), CONVERT(DATE, GETDATE())) GROUP BY o.Distributor_id, i.image ORDER BY COUNT(o.id) desc) select * from cte");
            DataSet ds = DataQueries.SelectCommon("select id,content from tblbanner");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void productlist()
        {
            string id = Request.Params["category_id"].ToString();
            string location = Request.Params["distributor_location"].ToString();
            string userid = Request.Params["user_id"].ToString();

            string shp = Request.Params["shopname"].ToString();
            string shp1 = string.Empty;
            //DataSet dss = DataQueries.SelectCommon("select * from distributorlogin where id='" + userid + "'");
            //if (dss.Tables[0].Rows.Count > 0)
            //{
            //    shp = dss.Tables[0].Rows[0]["shopname"].ToString();
            //}
            //string[] words = shp.Split(',');
            //foreach (string s in words)
            //{
            //    shp1 += s +"','";
            //}
            //shp = "'"+shp1.Substring(0,shp1.Length-3)+"'";

            DataSet ds = DataQueries.SelectCommon("select distinct a.id , a.productname,a.categoryid,a.units,a.mrp,a.image from Addproduct a inner join Category c on c.id = a.categoryid inner join DistributorLogin d on a.dealerlocation=d.loc where  c.id ='" + id + "' and dealerlocation='" + location + "' and a.status=1 and role='distributor' and a.shopname ='" + shp + "' ");

            DataTable table = ds.Tables[0];
            List<Product> productList = new List<Product>();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                Product product = new Product();
                int id1 = Convert.ToInt32(table.Rows[i]["id"].ToString());
                product.id = id1;
                product.productname = table.Rows[i]["productname"].ToString().Trim();
                product.categoryid = table.Rows[i]["categoryid"].ToString().Trim();
                product.units = table.Rows[i]["units"].ToString().Trim();
                product.price = table.Rows[i]["mrp"].ToString().Trim();
                product.image = table.Rows[i]["image"].ToString().Trim();

                product.quantity_list = get_quantitylist(userid, id1);

                if (product.quantity_list.Count > 0)
                    productList.Add(product);

            }
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(productList);
            Response.Write(json);
        }
        protected void plist()
        {
            //string id = Request.Params["category_id"].ToString();

            //string userid = Request.Params["user_id"].ToString();

            //DataSet ds = DataQueries.SelectCommon("select a.* from Addproduct a inner join Category c on c.id = a.categoryid where c.id ='" + id + "' and status=1");
            //DataTable table = ds.Tables[0];
            //List<Product> productList = new List<Product>();
            //for (int i = 0; i < table.Rows.Count; i++)
            //{
            //    Product product = new Product();

            //    int id1 = Convert.ToInt32(table.Rows[i]["id"].ToString());
            //    product.id = id1;
            //    product.productname = table.Rows[i]["productname"].ToString().Trim();
            //    //product.categoryid = table.Rows[i]["categoryid"].ToString().Trim();
            //    product.units = table.Rows[i]["units"].ToString().Trim();
            //    product.price = table.Rows[i]["mrp"].ToString().Trim();
            //    //product.available = table.Rows[i]["available"].ToString().Trim();
            //    product.image = table.Rows[i]["image"].ToString().Trim();

            //    //product.quantity_list = get_quantitylist(userid, id1);

            //    //if (product.quantity_list.Count > 0)
            //    productList.Add(product);

            //}

            //var jsonSerialiser = new JavaScriptSerializer();
            //var json = jsonSerialiser.Serialize(productList);
            //Response.Write(json);
        }
        protected void cart()
        {
            string id = Request.Params["user_id"].ToString();
            string shopid = Request.Params["shopid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select p.id,p.image,p.productname,p.units,p.mrp as price,p.price as actualamount,c.count from Addproduct p inner join tblcart c on p.id=c.product_id where user_id='" + id + "' and shopid='" + shopid + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        public void removecart()
        {
            string pid = Request.Params["product_id"].ToString();
            string uid = Request.Params["user_id"].ToString();
            DataQueries.DeleteCommon("delete from tblCart where product_id='" + pid + "' and user_id='" + uid + "' ");
            Response.Write("{\"message\":\"Success\"}");
        }
        protected void Addcustomer()
        {
            string name = Request.Params["customername"].ToString();
            string mobile = Request.Params["customerphone"].ToString();
            string address = Request.Params["customeraddress"].ToString();
            string pincode = Request.Params["customerpincode"].ToString();
            string distributor_id = Request.Params["distributor_id"].ToString();

            DataQueries.InsertCommon("insert into customer(name,mobile,Address,pincode,Did) values('" + name + "','" + mobile + "','" + address + "','" + pincode + "','" + distributor_id + "')");
            DataSet ds = DataQueries.SelectCommon("select id from customer where name='" + name + "' and mobile='" + mobile + "' and Address='" + address + "' and pincode='" + pincode + "' and Did='" + distributor_id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string id = ds.Tables[0].Rows[0]["id"].ToString();
                Response.Write("{\"error\":false,\"customerid\":" + id + "}");

            }

           

        }
        public List<ProductQuantity> get_quantitylist(string user_id, int product_id)
        {
            List<ProductQuantity> quantitylist = new List<ProductQuantity>();

            DataSet ds = DataQueries.SelectCommon("select isnull(sum(count),0) as count from tblcart c where user_id='" + user_id + "' and product_id='" + product_id + "'");
            DataTable dt = ds.Tables[0];
            for (int j = 0; j < dt.Rows.Count; j++)
            {

                ProductQuantity quantity = new ProductQuantity();

                quantity.count = Convert.ToInt32(dt.Rows[j]["count"].ToString());

                quantitylist.Add(quantity);
            }

            return quantitylist;
        }


        public string DataTableToJsonWithJsonNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = Newtonsoft.Json.JsonConvert.SerializeObject(table);
            return JSONString;
        }

        public string date()
        {
            return (DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
        }
        protected void order()

        {
            string customer_id = Request.Params["customer_id"].ToString();
            string distributor_id = Request.Params["distributor_id"].ToString();
            string ammount = Request.Params["ammount"].ToString();
            string actualamount = Request.Params["actualamount"].ToString();
            string shopname = Request.Params["shopname"].ToString();
            string order_date = date();
            string pid = string.Empty;
            string count = string.Empty;
            string oid = string.Empty;
            string location = string.Empty;
            string id = string.Empty;
            int tot, idd;
            string orderid = string.Empty;
            string mid = string.Empty;
            //in order to generate order id based on location
            DataSet t = DataQueries.SelectCommon("select id from distributorlogin where shopname='" + shopname + "' and role='merchant'");
            if (t.Tables[0].Rows.Count > 0)
            {
                mid = t.Tables[0].Rows[0]["id"].ToString();
            }
            DataSet dss = DataQueries.SelectCommon("select loc from distributorlogin where id='" + distributor_id + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                location = dss.Tables[0].Rows[0]["loc"].ToString().Trim();
            }
            DataSet dsss = DataQueries.SelectCommon("select count(id) as id from tblorder where Distributor_id='" + distributor_id + "'");
            if (dsss.Tables[0].Rows.Count > 0)
            {
                id = dsss.Tables[0].Rows[0]["id"].ToString().Trim();

            }
            tot = int.Parse(id);
            idd = tot + 1;
            double d = Convert.ToDouble(ammount);
            double ac = Convert.ToDouble(actualamount);
            double prof = d - ac;
            string profit = Convert.ToString(prof);

            DataQueries.InsertCommon("insert into tblorder(date,status,amount,actualamount,Distributor_id,Customer_id,orderid,status1,final,adminprof,final1,location,appstatus,shopname,delivery) values('" + order_date + "','Pending','" + ammount + "','" + actualamount + "','" + distributor_id + "','" + customer_id + "','" + location + idd + "','0','0','" + profit + "','0','" + location + "','0','" + mid + "','0')");

            DataSet ds = DataQueries.SelectCommon("select id,orderid from tblorder where Distributor_id='" + distributor_id + "' and amount='" + ammount + "' and Customer_id='" + customer_id + "' and date='" + order_date + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                oid = ds.Tables[0].Rows[0]["id"].ToString().Trim();

                orderid = ds.Tables[0].Rows[0]["orderid"].ToString().Trim();
            }

            DataSet cartdata = DataQueries.SelectCommon("select product_id,count from tblcart where user_id='" + distributor_id + "'");

            int count1 = cartdata.Tables[0].Rows.Count;
            for (int i = 0; i < count1; i++)
            {

                string product_id = cartdata.Tables[0].Rows[i]["product_id"].ToString().Trim();
                string product_count = cartdata.Tables[0].Rows[i]["count"].ToString().Trim();

                SqlConnection conn22 = new SqlConnection(constr);
                conn22.Open();
                // string strcommand22 = "select ISNULL(available,0) from Addproduct where id='" + product_id + "'";
                // SqlCommand comm22 = new SqlCommand(strcommand22, conn22);
                //int ProductQuantity = Convert.ToInt32(comm22.ExecuteScalar());
                DataQueries.InsertCommon("insert into tblorderitem(order_id,product_id,count) values ('" + oid + "','" + product_id + "','" + product_count + "')");
                conn22.Close();
            }
            DataQueries.DeleteCommon("delete from tblcart where user_id='" + distributor_id + "'");

            DataSet cust = DataQueries.SelectCommon("select * from customer where id=(select Customer_id from tblorder where id='" + oid + "')");
            if (cust.Tables[0].Rows.Count > 0)
            {
                string name = cust.Tables[0].Rows[0]["name"].ToString();
                string mobile = cust.Tables[0].Rows[0]["mobile"].ToString();
                string result = "";
                WebRequest request = null;
                HttpWebResponse response = null;
                String userid = "gsrnirmal";
                String passwd = "527927";
                StringBuilder sp = new StringBuilder();
                sp.Append("Dear Mr/Mrs." + name + ",thank you for shopping with prasad home products.Your Bill no is " + oid + " of amount " + ammount + ".You will receive your ordered products within 2 or 3 days home delivery.");
                String url = "http://smsc.essms.com/smpp/?username=" + userid + "&password=" + passwd + "&from=NIRMAL&to=91" + mobile + "&text=" + sp.ToString() + ""; request = WebRequest.Create(url);
                response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                Encoding ec = Encoding.GetEncoding("utf-8");
                StreamReader red = new System.IO.StreamReader(stream, ec);
                result = red.ReadToEnd();
                Console.WriteLine(result);
                red.Close();
                stream.Close();
            }

            Response.Write("{\"message\":\"Success\",\"order_id\":" + oid.Trim().ToString() + "}");

        }
        protected void distributors_list()
        {
            DataSet ds = DataQueries.SelectCommon("select Distributor_id,(select name from DistributorLogin where id=Distributor_id) as name from tblorder where REPLACE( (convert(varchar(10),date,110)),'-','/')= convert(varchar(10),GETDATE(),111) group by Distributor_id");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void btwndate_distributors_list()
        {
            string fromdate = Request.Params["fromdate"].ToString().Trim();
            string todate = Request.Params["Todate"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select  Distributor_id,(select name from DistributorLogin where id=Distributor_id) as name from tblorder where (convert(varchar(10),date,110)) between '" + fromdate + "' and '" + todate + "'  group by Distributor_id");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void date_distributors_list()
        {
            string date = Request.Params["date"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select  Distributor_id,(select name from DistributorLogin where id=Distributor_id) as name from tblorder where (convert(varchar(10),date,110)) ='" + date + "' group by Distributor_id");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void orders_list()
        {
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select id  as orderid,status from tblorder where REPLACE( (convert(varchar(10),date,110)),'-','/')= convert(varchar(10),GETDATE(),111) and Distributor_id='" + distributor_id + "' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void btwndate_orders_list()
        {
            string fromdate = Request.Params["fromdate"].ToString().Trim();
            string todate = Request.Params["Todate"].ToString().Trim();
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select id  as orderid,status from tblorder where (convert(varchar(10),date,110)) between '" + fromdate + "' and '" + todate + "' and Distributor_id='" + distributor_id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void date_orders_list()
        {
            string date = Request.Params["date"].ToString().Trim();
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select id  as orderid,status from tblorder where (convert(varchar(10),date,110))='" + date + "' and Distributor_id='" + distributor_id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void order_items()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select c.name,c.mobile,c.Address,a.image,a.productname,a.units,a.mrp as price,oi.count,(select amount from tblorder where id='" + order_id + "') as totalprice,o.actualamount,a.price as normal from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + order_id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void Distributorlist()
        {
            DataSet ds = DataQueries.SelectCommon("select name,mobile,email from DistributorLogin where role='distributor'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }
        protected void Distributer_orders_list()
        {

            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select id  as order_id,status,orderid,convert(varchar(10),date,108) as date,delivery from tblorder where  Distributor_id='" + distributor_id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }
        protected void distributer_order_items()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select c.name,c.mobile,c.Address,a.image,a.productname,a.units,a.mrp as price,oi.count,(select amount from tblorder where id='" + order_id + "') as totalprice from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + order_id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void order_rejected()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            DataQueries.UpdateCommon("update tblorder set status='Rejected' where id='" + order_id + "'");

            DataSet ds = DataQueries.SelectCommon("select status from tblorder where id='" + order_id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string status = ds.Tables[0].Rows[0]["status"].ToString();
                Response.Write("{\"error\": false,\"Status\":" + status + "}");
            }
        }
        protected void order_approved()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select a.id,a.productname,a.units,a.mrp as price,oi.count,(select amount from tblorder where id='" + order_id + "') as totalprice from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + order_id + "'");
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string product_id = dt.Rows[i]["id"].ToString().Trim();
                double count = Convert.ToDouble(dt.Rows[i]["count"].ToString().Trim());
                //double available = Convert.ToDouble(dt.Rows[i]["available"].ToString().Trim());

                //double new_quantity = available - count;

                //DataQueries.UpdateCommon("update Addproduct set available='" + new_quantity + "' where id='" + product_id + "'");

                DataQueries.UpdateCommon("update tblorder set status='Approved' where id='" + order_id + "'");

                DataSet ds1 = DataQueries.SelectCommon("select status from tblorder where id='" + order_id + "'");
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    string status = ds1.Tables[0].Rows[0]["status"].ToString();
                    Response.Write("{\"error\": false,\"Status\":" + status + "}");
                }
            }

            //SMS

        }

        protected void Bill_order_items()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("with cte as (select RANK() OVER(ORDER BY a.id ) AS sno ,c.name,c.mobile,o.status,c.Address,a.productname,a.units,a.price,oi.count,CAST(a.mrp as decimal) as GSTprice,(select amount from tblorder where id='" + order_id + "') as totalprice from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + order_id + "')select *,cte.GSTprice*cte.count as Eachproducttotal from cte");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void Bill_Customer_Details()
        {

            string order_id = Request.Params["order_id"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("with cte as(select c.name as Name,c.mobile as Mobile,c.Address as Address,d.name as dname,convert(varchar(10),o.date,108) as date,d.mobile as dmobile,d.address as daddress,o.status as Status,(select amount from tblorder where id='" + order_id + "') as Totalprice from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id inner join DistributorLogin d on c.Did=d.id where o.id = '" + order_id + "') select * from cte group by Mobile,Name,Totalprice,Address,Status,dname,dmobile,daddress,date");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["Name"].ToString();
                string mobile = ds.Tables[0].Rows[0]["Mobile"].ToString();
                string address = ds.Tables[0].Rows[0]["Address"].ToString();
                string total = ds.Tables[0].Rows[0]["Totalprice"].ToString();
                string status = ds.Tables[0].Rows[0]["Status"].ToString();
                string daddress = ds.Tables[0].Rows[0]["daddress"].ToString();
                string dname = ds.Tables[0].Rows[0]["dname"].ToString();
                string dmobile = ds.Tables[0].Rows[0]["dmobile"].ToString();
                string date = ds.Tables[0].Rows[0]["date"].ToString();
                Response.Write("{\"error\": false ,\"Name\":\"" + name + "\",\"Mobile\":\"" + mobile + "\",\"Address\":\"" + address + "\",\"Status\":\"" + status + "\",\"dname\":\"" + dname + "\",\"dmobile\":\"" + dmobile + "\",\"daddress\":\"" + daddress + "\",\"date\":\"" + date + "\",\"Totalprice\":\"" + total + "\"}");

            }
        }
        protected void customerdetails()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();

            DataSet ds = DataQueries.SelectCommon("select name,mobile,Address,(select name from DistributorLogin where id=(select Distributor_id from tblorder where id='" + order_id + "') ) as Distributorname from customer where id=(select Customer_id from tblorder where id='" + order_id + "')");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString();
                string mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                string Address = ds.Tables[0].Rows[0]["Address"].ToString();
                string Distributorname = ds.Tables[0].Rows[0]["Distributorname"].ToString();
                Response.Write("{\"error\":false,\"name\":\"" + name + "\",\"mobile\":\"" + mobile + "\",\"Address\":\"" + Address + "\",\"Distributorname\":\"" + Distributorname + "\"}");
            }
        }
        protected void contacts()
        {
            EmployeeOperation operation = new EmployeeOperation();
            var jsonSerialiser = new JavaScriptSerializer();
            //Response.Write(jsonSerialiser.Serialize(operation.getEmployeeList()));
            //Response.Write("{\"error\":false,\"message\":Success}");
            Response.Write("{\"error\":false,\"message\":Success,\"data\": " + jsonSerialiser.Serialize(operation.getEmployeeList()) + "}");

        }
        protected void productsearch()
        {
            string id = Request.Params["category_id"].ToString();

            string userid = Request.Params["user_id"].ToString();
            string locat = Request.Params["location"].ToString();

            DataSet ds = DataQueries.SelectCommon("select a.* from Addproduct a inner join Category c on c.id = a.categoryid where c.id ='" + id + "' and status=1 and a.dealerlocation='" + locat + "'");
            DataTable table = ds.Tables[0];
            List<Product> productList = new List<Product>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                Product product = new Product();

                int id1 = Convert.ToInt32(table.Rows[i]["id"].ToString());
                product.id = id1;
                product.productname = table.Rows[i]["productname"].ToString().Trim();
                product.categoryid = table.Rows[i]["categoryid"].ToString().Trim();
                product.units = table.Rows[i]["units"].ToString().Trim();
                product.price = table.Rows[i]["mrp"].ToString().Trim();
                //product.actualcost = table.Rows[i]["price"].ToString().Trim();
                product.image = table.Rows[i]["image"].ToString().Trim();

                product.quantity_list = get_quantitylist(userid, id1);

                if (product.quantity_list.Count > 0)
                    productList.Add(product);

            }

            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(productList);
            Response.Write("{\"error\":false,\"message\":\"Success\",\"data\": " + json + "}");
        }
        protected void custcontacts()
        {
            string id = Request.Params["term"].ToString();
            DataSet ds = DataQueries.SelectCommon("select mobile from customer where mobile like '%" + id + "%' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"data\": " + JSONresult + "}");
        }
        protected void binddetails()
        {
            string id = Request.Params["number"].ToString();
            DataSet ds = DataQueries.SelectCommon("select name,mobile,address,pincode from customer where mobile='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString();
                string mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                string address = ds.Tables[0].Rows[0]["address"].ToString();
                string pincode = ds.Tables[0].Rows[0]["pincode"].ToString();
                Response.Write("{\"error\": false ,\"name\":\"" + name + "\",\"mobile\":\"" + mobile + "\",\"address\":\"" + address + "\",\"pincode\":\"" + pincode + "\"}");

            }
        }
        protected void suggestion()
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string sname = Request.Params["sname"].ToString();
            string smob = Request.Params["smobile"].ToString();
            string saddress = Request.Params["saddress"].ToString();
            string sugg = Request.Params["ssug"].ToString();

            DataQueries.InsertCommon("insert into suggestions(date,sname,smob,sadd,sugg) values('" + date + "','" + sname + "','" + smob + "','" + saddress + "','" + sugg + "')");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        protected void suggdisplay()
        {

            DataSet ds = DataQueries.SelectCommon("select * from suggestions");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void approvelist()
        {
            DataSet ds = DataQueries.SelectCommon(" select cast(id as varchar(max)) as id,amount,status from tblorder where status='approved'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void inactive()
        {
            string key = Request.Params["key"].ToString();
            DataQueries.UpdateCommon("update active set status='0' where did='" + key + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        protected void activelogins()
        {
            DataSet ds = DataQueries.SelectCommon("select d.name from DistributorLogin d inner join active a on a.did=d.id where a.status=1 ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        //according to latest requirement services 
        protected void shopnamesbind()
        {
            string loc = Request.Params["shoplocation"].ToString();
            DataSet ds = DataQueries.SelectCommon("select shopname from distributorlogin where shopname is not null and loc='" + loc + "' and role='merchant' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"Shopname\": " + JSONresult + "}";
            Response.Write(result);

        }

        //dealer registers shop details and creates a login for shop owner(merchant) etc..
        protected void dealeraddsmerchant()
        {
            string shopname = Request.Params["shopname"].ToString();
            string dname = Request.Params["name"].ToString();
            string dmobile = Request.Params["mobile"].ToString();
            string aadhar = Request.Params["aadhar_number"].ToString();
            string daddress = Request.Params["address"].ToString();
            string loc = Request.Params["location"].ToString();
            string password = Request.Params["password"].ToString();
            string image = Request.Params["image"].ToString();
            string did = Request.Params["did"].ToString();
            string village = Request.Params["village"].ToString();
            string imagebase64 = image;
            string imname = dname + dmobile;
            byte[] bytes1 = Convert.FromBase64String(imagebase64);
            File.WriteAllBytes(Server.MapPath("~/images/") + imname + ".jpg", bytes1);
            string path = "http://www.gsrnirmal.com/images/" + imname + ".jpg";
            DataQueries.InsertCommon("insert into Distributorlogin(shopname,name,username,mobile,address,loc,password,role,status,image,did,aadhar,village) values('" + shopname + "','" + dname + "','" + dmobile + "','" + dmobile + "','" + daddress + "','" + loc + "','" + password + "','merchant','1','" + path + "','" + did + "','" + aadhar + "','" + village + "')");

            Response.Write("{\"error\": false ,\"message\":\"Success\"}");

        }
        protected void categorybind()
        {
            DataSet ds = DataQueries.SelectCommon("select categoryname from category");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            string result = "{\"Categoryname\": " + JSONresult + "}";
            Response.Write(result);
        }
        //dealer adds products 

        protected void addingproductsthroughapp()
        {
            //string id = string.Empty;
            //string category = Request.Params["categoryname"].ToString();
            //string shopname = Request.Params["shname"].ToString();
            //string pname = Request.Params["pname"].ToString();
            //string pquant = Request.Params["pquant"].ToString();
            //string pprice = Request.Params["pprice"].ToString();
            //string mrp = Request.Params["mrp"].ToString();
            //string location = Request.Params["location"].ToString();
            //string image = Request.Params["image"].ToString();
            //string did = Request.Params["did"].ToString();
            //string imagebase64 = image;
            //byte[] bytes1 = Convert.FromBase64String(imagebase64);
            //File.WriteAllBytes(Server.MapPath("~/images/") + pname + ".jpg", bytes1);
            //string path = "http://www.gsrnirmal.com/images/" + pname + ".jpg";
            //DataSet d = DataQueries.SelectCommon("select id from category where categoryname='" + category + "'");
            //if (d.Tables[0].Rows.Count > 0)
            //{
            //    id = d.Tables[0].Rows[0]["id"].ToString();
            //}
            //DataQueries.InsertCommon("insert into Addproduct(productname,categoryid,units,price,mrp,shopname,status,dealerlocation,image,did) values('" + pname + "','" + id + "','" + pquant + "','" + pprice + "','" + mrp + "','" + shopname + "','0','" + location + "','" + path + "','" + did + "')");
            //Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        //binding locations
        protected void mandals()
        {
            string mandal = Request.Params["mandal"].ToString();
            DataSet ds = DataQueries.SelectCommon("select location from locations where location like '%" + mandal + "%' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"data\": " + JSONresult + "}");
        }
        //to display locations for admin to approve orders
        protected void approvemandal()
        {
            DataSet ds = DataQueries.SelectCommon(" select distinct d.loc from DistributorLogin d inner join tblorder o on o.Distributor_id=d.id where d.loc is not null and o.status='Approved' and o.status1='0'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        //to display distributors list based on location to admin
        protected void mandaldistributors()
        {
            string loc = Request.Params["location"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct d.id,d.name from DistributorLogin d inner join tblorder o on o.Distributor_id=d.id  where o.status='Approved' and d.loc='" + loc + "' and o.status1='0'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        //to display orders list based on distributors to admin
        protected void mandalorders()
        {
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select cast(o.id as varchar) as id,o.status from tblorder o inner join DistributorLogin d on o.Distributor_id=d.id where o.status='Approved' and o.Distributor_id='" + distributor_id + "' and o.status1='0' and appstatus='1' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void sendingapproved()
        {
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataQueries.UpdateCommon("update tblorder set status1='1' , appstatus='2' where Distributor_id='" + distributor_id + "' and status='Approved'");
            Response.Write("{\"message\": success\"}");
        }
        protected void displaydistributorstomerchant()
        {
            string loc = Request.Params["location"].ToString();
            string mid = Request.Params["mid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct cast(d.id as varchar) as id,d.name from DistributorLogin d inner join tblorder o on o.Distributor_id=d.id  where o.status='Approved' and d.loc='" + loc + "' and o.status1='1' and o.final='0' and o.shopname='" + mid + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }
        protected void displayorderslisttomerchant()
        {
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select cast(o.id as varchar) as id,o.status from tblorder o inner join DistributorLogin d on o.Distributor_id=d.id where o.status='Approved' and o.Distributor_id='" + distributor_id + "' and o.status1='1' and o.final='0' order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);


        }
        protected void profitdisplay()
        {
            DataSet ds = DataQueries.SelectCommon("select amount,actualamount,(convert (float,amount)-convert (float,actualamount)) as profit from tblorder");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void includetravelcharges()
        {
            string orderid = Request.Params["orderid"].ToString().Trim();
            string travelcharges = Request.Params["charges"].ToString().Trim();
            string actualamount = Request.Params["amount"].ToString().Trim();
            string mid = Request.Params["mid"].ToString();
            string adminprof = string.Empty;
            string id = string.Empty;
            DataSet das = DataQueries.SelectCommon("select adminprof from tblorder2 where id='" + orderid + "'");
            if (das.Tables[0].Rows.Count > 0)
            {
                adminprof = das.Tables[0].Rows[0]["adminprof"].ToString();
            }

            double d;
            double t = Convert.ToDouble(travelcharges);
            double a = Convert.ToDouble(adminprof);
            d = a - t;
            string amt = Convert.ToString(d);
            DataQueries.UpdateCommon("update tblorder2 set travelcharges='" + travelcharges + "',updatedamount='" + amt + "' where id='" + orderid + "' and status='Approved'");

            DataSet ds = DataQueries.SelectCommon("select updatedamount from tblorder2 where id='" + orderid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                id = ds.Tables[0].Rows[0]["updatedamount"].ToString();
            }


            Response.Write("{\"error\": false ,\"message\":\"Success\",\"updatedamount\":\"" + id + "\",\"adminprof\":\"" + adminprof + "\"}");
        }
        protected void merchanttoadmin()
        {
            string mid = Request.Params["mid"].ToString();
            string id = Request.Params["id"].ToString();
            string did = Request.Params["did"].ToString();
            DataQueries.DeleteCommon("delete from  merchantsendsapprovelist where mid='" + mid + "' and distributor_id='"+did+"'");
            DataQueries.UpdateCommon("update tblorder2 set final='1' where id='" + id + "'");
            Response.Write("{\"error\": false ,\"message\": success\"}");
        }

        protected void merchant_order_items()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select c.name,c.mobile,c.Address,a.image,a.productname,a.units,a.mrp as price,oi.count,(select amount from tblorder where id='" + order_id + "') as totalprice,o.actualamount,a.price as normal from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + order_id + "' and o.final='0'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void sendtoadmin()
        {

            string orderid = Request.Params["orderid"].ToString().Trim();
            string date = string.Empty;
            string status = string.Empty;
            string amount = string.Empty;
            string Distributor_id = string.Empty;
            string Customer_id = string.Empty;
            string ordername = string.Empty;
            string actualamount = string.Empty;
            string mid = Request.Params["mid"].ToString();
            DataQueries.UpdateCommon("update tblorder set final='1' where id='" + orderid + "' and status='Approved'");
            DataSet ds = DataQueries.SelectCommon("select * from tblorder where id='" + orderid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                date = ds.Tables[0].Rows[0]["date"].ToString();
                status = ds.Tables[0].Rows[0]["status"].ToString();
                amount = ds.Tables[0].Rows[0]["amount"].ToString();
                Distributor_id = ds.Tables[0].Rows[0]["Distributor_id"].ToString();
                Customer_id = ds.Tables[0].Rows[0]["Customer_id"].ToString();
                ordername = ds.Tables[0].Rows[0]["orderid"].ToString();
                actualamount = ds.Tables[0].Rows[0]["actualamount"].ToString();
                //  DataQueries.InsertCommon("insert into merchantsendsapprovelist(oid,date,status,amount,actualamount,Distributor_id,Customer_id,orderid) values('" + orderid+"','" + date + "','"+status+"','" + amount + "','" + actualamount + "','" + Distributor_id + "','" + Customer_id + "','" + ordername +"')");
            }

            DataQueries.InsertCommon("insert into merchantsendsapprovelist(oid,date,status,amount,actualamount,Distributor_id,Customer_id,orderid,mid) values('" + orderid + "','" + date + "','" + status + "','" + amount + "','" + actualamount + "','" + Distributor_id + "','" + Customer_id + "','" + ordername + "','" + mid + "')");
            Response.Write("{\"error\": false ,\"message\": success\"}");

        }

        protected void distaddress()
        {
            string id = Request.Params["id"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct d.id,d.address from distributorlogin d inner join [merchantsendsapprovelist] m on d.id=m.distributor_id where mid='" + id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void final1()
        {
            string id = Request.Params["id"].ToString();
            string did = Request.Params["did"].ToString();
            DataSet ds = DataQueries.SelectCommon("select oid,amount,distributor_id from [merchantsendsapprovelist] where mid='" + id + "' and Distributor_id='" + did + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }


        protected void inserttblorder2()
        {
            string mid = Request.Params["mid"].ToString();
            string did = Request.Params["did"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string id = string.Empty;
            string amt = string.Empty;
            string actualamt = string.Empty;
            string d = string.Empty;
            string amountt = string.Empty;
            string c = string.Empty;
            string orderid1 = string.Empty;
            string orderid2 = string.Empty;
            DataSet dss = DataQueries.SelectCommon("select orderid from merchantsendsapprovelist where mid='" + mid + "' and distributor_id='" + did + "' order by id desc");
            if (dss.Tables[0].Rows.Count > 0)
            {
                orderid1 = dss.Tables[0].Rows[0]["orderid"].ToString().Trim();
            }
            DataSet dsts = DataQueries.SelectCommon("select orderid from tblorderitem2 where mid='" + mid + "' and distributor_id='" + did + "' order by id desc");
            if (dsts.Tables[0].Rows.Count > 0)
            {
                orderid2 = dsts.Tables[0].Rows[0]["orderid"].ToString().Trim();
            }
            if (orderid1 == orderid2)
            {
                DataSet ds = DataQueries.SelectCommon("select id,amount from tblorder2 where mid='" + mid + "' and distributor_id='" + did + "' order by id desc");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    id = ds.Tables[0].Rows[0]["id"].ToString().Trim();
                    amountt = ds.Tables[0].Rows[0]["amount"].ToString().Trim();
                }
                Response.Write("{\"error\": false,\"message\":\"Success\",\"id\":\"" + id + "\",\"amount\":\"" + amountt + "\"}");
            }
            else
            {
                DataSet dssq = DataQueries.SelectCommon("select sum(cast(amount as float)) as amount,sum(cast(actualamount as float)) as actualamount from merchantsendsapprovelist where mid='" + mid + "' and distributor_id='" + did + "'");
                if (dssq.Tables[0].Rows.Count > 0)
                {
                    amt = dssq.Tables[0].Rows[0]["amount"].ToString().Trim();
                    actualamt = dssq.Tables[0].Rows[0]["actualamount"].ToString().Trim();
                    //d = dssq.Tables[0].Rows[0]["Distributor_id"].ToString().Trim();
                    // c = dssq.Tables[0].Rows[0]["Customer_id"].ToString().Trim();

                }
                double a = Convert.ToDouble(amt);
                double at = Convert.ToDouble(actualamt);
                double prof = a - at;
                string profitamt = Convert.ToString(prof);
                DataQueries.InsertCommon("insert into tblorder2(date,status,mid,amount,actualamount,adminprof,final,distributor_id) values('" + date + "','Approved','" + mid + "','" + amt + "','" + actualamt + "','" + profitamt + "','0','" + did + "')");

                DataSet ds = DataQueries.SelectCommon("select id,amount from tblorder2 where mid='" + mid + "' and distributor_id='" + did + "'  order by id desc");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    id = ds.Tables[0].Rows[0]["id"].ToString().Trim();
                    amountt = ds.Tables[0].Rows[0]["amount"].ToString().Trim();
                }
                DataSet cartdata = DataQueries.SelectCommon("select * from merchantsendsapprovelist where mid='" + mid + "' and distributor_id='" + did + "'");

                int count1 = cartdata.Tables[0].Rows.Count;
                for (int i = 0; i < count1; i++)
                {

                    string oid = cartdata.Tables[0].Rows[i]["oid"].ToString().Trim();
                    string datee = cartdata.Tables[0].Rows[i]["date"].ToString().Trim();
                    string status = cartdata.Tables[0].Rows[i]["status"].ToString().Trim();
                    string amount = cartdata.Tables[0].Rows[i]["amount"].ToString().Trim();
                    string Distributor_id = cartdata.Tables[0].Rows[i]["Distributor_id"].ToString().Trim();
                    string Customer_id = cartdata.Tables[0].Rows[i]["Customer_id"].ToString().Trim();
                    string orderid = cartdata.Tables[0].Rows[i]["orderid"].ToString().Trim();
                    string actualamount = cartdata.Tables[0].Rows[i]["actualamount"].ToString().Trim();

                    SqlConnection conn22 = new SqlConnection(constr);
                    conn22.Open();

                    DataQueries.InsertCommon("insert into tblorderitem2(ordid,[oid],[date],[status],[amount],[Distributor_id],[Customer_id],[orderid],[actualamount],[mid]) values ('" + id + "','" + oid + "','" + datee + "','" + status + "','" + amount + "','" + Distributor_id + "','" + Customer_id + "','" + orderid + "','" + actualamount + "','" + mid + "')");
                    conn22.Close();
                }
                Response.Write("{\"error\": false,\"message\":\"Success\",\"id\":\"" + id + "\",\"amount\":\"" + amountt + "\"}");
            }
        }

        protected void distributorprofitloc()
        {
            DataSet ds = DataQueries.SelectCommon("select distinct d.loc from DistributorLogin d inner join tblorder2 o on o.Distributor_id=d.id where d.loc is not null and o.status='Approved' and o.final='1' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void distributorprofitnames()
        {
            string loc = Request.Params["location"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct d.id,d.name from DistributorLogin d inner join tblorder2 o on o.Distributor_id=d.id  where o.status='Approved' and d.loc='" + loc + "' and o.final='1' ");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void distributorprofitordersids()
        {
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select o.id,o.status,o.amount,o.actualamount,o.adminprof,o.travelcharges,o.updatedamount from tblorder2 o inner join DistributorLogin d on o.Distributor_id=d.id where o.status='Approved' and o.Distributor_id='" + distributor_id + "' and o.final='1'  order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void distributorsprofitorders()
        {
            string id = Request.Params["id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select o.id,o.status,o.orderid from tblorderitem2 o inner join DistributorLogin d on o.Distributor_id=d.id where o.status='Approved' and o.ordid='" + id + "'   order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void dealerprofit()
        {
            string id = Request.Params["id"].ToString();
            string oid = string.Empty;
            string dam = string.Empty;
            string amt = string.Empty;
            string dname = string.Empty;
            string did = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select o.id as oid,sum(cast(a.dealamount as float)*i.count) as damt,o.amount,d.name  from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id  where a.did ='" + id + "' group by o.id,o.amount,d.name");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }

        protected void orderitemslist()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            string oid = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select oid from tblorderitem2 where id='" + order_id + "' order by id desc");
            if (ds.Tables[0].Rows.Count > 0)
            {
                oid = ds.Tables[0].Rows[0]["oid"].ToString().Trim();
            }
            DataSet dss = DataQueries.SelectCommon("select c.name,c.mobile,c.Address,a.image,a.productname,a.units,a.mrp as price,oi.count,(select amount from tblorder where id='" + oid + "') as totalprice,o.actualamount,o.travelcharges,o.updatedamount,o.adminprof from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + oid + "'");
            DataTable table = dss.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        protected void profitamounttoadmin()
        {

            string order_id = Request.Params["order_id"].ToString().Trim();
            //DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as distprofit,o.amount,o.adminprof,o.travelcharges,o.updatedamount from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id where i.order_id='" + order_id + "' group by amount,actualamount,adminprof,travelcharges,updatedamount ");
            //DataTable table = ds.Tables[0];
            //string JSONresult = DataTableToJsonWithJsonNet(table);
            //Response.Write(JSONresult);
            string distprofit = string.Empty;
            string amount = string.Empty;
            string adminprof = string.Empty;
            string charges = string.Empty;
            string update = string.Empty;
            string oid = string.Empty;

            DataSet dss = DataQueries.SelectCommon("select oid from tblorderitem2 where id='" + order_id + "' order by id desc");
            if (dss.Tables[0].Rows.Count > 0)
            {
                oid = dss.Tables[0].Rows[0]["oid"].ToString().Trim();

            }
            DataSet dsss = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as distprofit from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id where i.order_id='" + oid + "'");
            if (dsss.Tables[0].Rows.Count > 0)
            {
                distprofit = dsss.Tables[0].Rows[0]["distprofit"].ToString().Trim();

            }


            DataSet ds = DataQueries.SelectCommon("select amount,actualamount,(convert (float,amount)-convert (float,actualamount)) as profit from tblorderitem2 where oid='" + oid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {

                amount = ds.Tables[0].Rows[0]["amount"].ToString();
                adminprof = ds.Tables[0].Rows[0]["actualamount"].ToString();

                update = ds.Tables[0].Rows[0]["profit"].ToString();
            }
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"amount\":\"" + amount + "\",\"distprofit\":\"" + distprofit + "\"}");

        }
        protected void sendproductslist()
        {
            string id = Request.Params["id"].ToString().Trim();
            string dealid = Request.Params["did"].ToString();
            string shop = Request.Params["shpname"].ToString();
            string id1 = string.Empty;
            string image = string.Empty;
            string pname = string.Empty;
            //DataSet dss = DataQueries.SelectCommon("with cte as(select p.*,a.productname from productslist p left join (select * from Addproduct where did=174 ) a on p.pname=a.productname )select pname, image, catname,case when productname is null then 0 else 1 end status  from cte ");
            DataSet ds = DataQueries.SelectCommon("with cte as(select p.*,a.productname from productslist p left join (select * from Addproduct where did='" + dealid + "' and shopname='" + shop + "') a on p.pname=a.productname where catname='" + id + "' )select id,pname, image, catname,case when productname is null then 0 else 1 end count  from cte order by id desc");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void dealeraddproducts()
        {
            string id = string.Empty;
            string cid = Request.Params["cid"].ToString();
            string category = Request.Params["categoryname"].ToString();
            string shopname = Request.Params["shname"].ToString();
            string pname = string.Empty;
            string pquant = Request.Params["pquant"].ToString();
            string pprice = Request.Params["pprice"].ToString();
            string mrp = Request.Params["mrp"].ToString();
            string location = Request.Params["location"].ToString();
            string did = Request.Params["did"].ToString();
            string image = string.Empty;

            DataSet d = DataQueries.SelectCommon("select id from category where categoryname='" + category + "'");
            if (d.Tables[0].Rows.Count > 0)
            {
                id = d.Tables[0].Rows[0]["id"].ToString();
            }
            DataSet d1 = DataQueries.SelectCommon("select pname,image from productslist where id='" + cid + "'");
            if (d1.Tables[0].Rows.Count > 0)
            {
                pname = d1.Tables[0].Rows[0]["pname"].ToString();
                image = d1.Tables[0].Rows[0]["image"].ToString();
            }

            DataQueries.InsertCommon("insert into Addproduct(categoryid,units,price,mrp,shopname,status,dealerlocation,productname,image,did) values('" + id + "','" + pquant + "','" + pprice + "','" + mrp + "','" + shopname + "','0','" + location + "','" + pname + "','" + image + "','" + did + "')");
            Response.Write("{\"error\": false ,\"count\":\"1\"}");
        }

        protected void clear()
        {
            string orderid = Request.Params["orderid"].ToString().Trim();
            DataQueries.UpdateCommon("update tblorder2 set final1='1' where id='" + orderid + "' and status='Approved'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }

        protected void distributororderlist()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            string oid = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select c.name,c.mobile,c.Address,a.image,a.productname,a.units,a.mrp as price,oi.count,(select amount from tblorder where id='" + order_id + "') as totalprice,o.actualamount,o.travelcharges,o.updatedamount,o.adminprof from Addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join customer c on c.id=o.Customer_id where o.id = '" + order_id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void distributorprofiamount()
        {
            string order_id = Request.Params["order_id"].ToString().Trim();
            //DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as distprofit,o.amount,o.adminprof,o.travelcharges,o.updatedamount from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id where i.order_id='" + order_id + "' group by amount,actualamount,adminprof,travelcharges,updatedamount ");
            //DataTable table = ds.Tables[0];
            //string JSONresult = DataTableToJsonWithJsonNet(table);
            //Response.Write(JSONresult);
            string distprofit = string.Empty;
            string amount = string.Empty;
            string adminprof = string.Empty;
            string charges = string.Empty;
            string update = string.Empty;
            string oid = string.Empty;
            DataSet dss = DataQueries.SelectCommon("select oid from tblorderitem2 where id='" + order_id + "' order by id desc");
            if (dss.Tables[0].Rows.Count > 0)
            {
                oid = dss.Tables[0].Rows[0]["oid"].ToString().Trim();

            }
            DataSet dsss = DataQueries.SelectCommon("select sum((cast(a.distamount as float)*i.count)) as distprofit from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id where i.order_id='" + oid + "'");
            if (dsss.Tables[0].Rows.Count > 0)
            {
                distprofit = dsss.Tables[0].Rows[0]["distprofit"].ToString().Trim();

            }
            //DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as distprofit,o.amount,o.adminprof,o.travelcharges,o.updatedamount from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id where i.order_id='" + order_id + "' and o.final1='1' group by amount,actualamount,adminprof,travelcharges,updatedamount ");
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    distprofit = ds.Tables[0].Rows[0]["distprofit"].ToString();
            //    amount = ds.Tables[0].Rows[0]["amount"].ToString();
            //    adminprof = ds.Tables[0].Rows[0]["adminprof"].ToString();
            //    charges = ds.Tables[0].Rows[0]["travelcharges"].ToString();
            //    update = ds.Tables[0].Rows[0]["updatedamount"].ToString();
            //}
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"distprofit\":\"" + distprofit + "\"}");

        }

        protected void distributorprofit()
        {
            string distributor_id = Request.Params["distributor_id"].ToString().Trim();
            // DataSet ds = DataQueries.SelectCommon("select o.id as orderid,o.status from tblorderitem2 o inner join DistributorLogin d on o.Distributor_id=d.id where o.status='Approved' and d.id='" + distributor_id + "' order by o.id desc");
            DataSet ds = DataQueries.SelectCommon("select o.id as orderid,sum((cast(a.distamount as float)*i.count)) as profit  from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id where d.id ='" + distributor_id + "' group by o.id");

            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void sendbills()
        {
            string date = DateTime.Now.ToString("yyyy-MM-dd");
            string img = Request.Params["image"].ToString().Trim();
            string text = Request.Params["text"].ToString().Trim();
            string loc = Request.Params["loc"].ToString().Trim();
            string mname = Request.Params["mname"].ToString().Trim();
            string mnumber = Request.Params["mnumber"].ToString().Trim();
            string amount = Request.Params["amount"].ToString().Trim();
            string oid = Request.Params["id"].ToString();
            string imagebase64 = img;
            string id = string.Empty;
            string shopname = string.Empty;
            string mid = string.Empty;
            int tot, idd;
            DataSet dsss = DataQueries.SelectCommon("select count(id) as id from bills ");
            if (dsss.Tables[0].Rows.Count > 0)
            {
                id = dsss.Tables[0].Rows[0]["id"].ToString().Trim();

            }
            tot = int.Parse(id);
            idd = tot + 1;
            byte[] bytes1 = Convert.FromBase64String(imagebase64);
            File.WriteAllBytes(Server.MapPath("~/images/") + idd + ".jpg", bytes1);
            string path = "http://www.gsrnirmal.com/images/" + idd + ".jpg";

            DataSet dss = DataQueries.SelectCommon("select id,shopname from distributorlogin where mobile='" + mnumber + "' ");
            if (dss.Tables[0].Rows.Count > 0)
            {
                shopname = dss.Tables[0].Rows[0]["shopname"].ToString().Trim();
                mid = dss.Tables[0].Rows[0]["id"].ToString().Trim();
            }

            DataQueries.InsertCommon("insert into bills(image,text,loc,mname,mnumber,shopname,date,amount,oid) values('" + path + "','" + text + "','" + loc + "','" + mname + "','" + mnumber + "','" + shopname + "','" + date + "','" + amount + "','" + oid + "')");
            DataQueries.UpdateCommon("update tblorder2 set stat='1' where id='" + oid + "'");
            Response.Write("{\"error\": false,\"message\":\"Success\"}");
        }

        protected void displaybills()
        {
            DataSet ds = DataQueries.SelectCommon("select * from bills");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void totaldistprofit()
        {
            string did = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.distamount as float)) as distprofit from addproduct a inner join tblorderitem oi on a.id=oi.product_id inner join tblorder o on oi.order_id=o.id inner join DistributorLogin d on o.Distributor_id=d.id where d.id='" + did + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string profit = ds.Tables[0].Rows[0]["distprofit"].ToString();
                Response.Write("{\"error\":false,\"distprofit\":\"" + profit + "\"}");
            }
        }
        protected void distaddedcustlist()
        {
            string id = Request.Params["did"].ToString();
            DataSet ds = DataQueries.SelectCommon("select name,mobile,address from customer where did='" + id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }
        protected void merchantdetailstodealer()
        {
            string loc = Request.Params["loc"].ToString();
            DataSet ds = DataQueries.SelectCommon("select name,mobile,village as address from DistributorLogin where loc='" + loc + "' and role='merchant'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }
        protected void distdetailstodealer()
        {
            string loc = Request.Params["loc"].ToString();
            DataSet ds = DataQueries.SelectCommon("select name,mobile,village as address from DistributorLogin where loc='" + loc + "' and role='distributor'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");
        }
        protected void distordercount()
        {
            string loc = Request.Params["loc"].ToString();
            string ct = string.Empty;
            string address = string.Empty;
            string name = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select d.name,count(Distributor_id) as count,d.address from tblorder t inner join DistributorLogin d on d.id=t.Distributor_id where location='" + loc + "'  group by Distributor_id,name,address");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write("{\"error\": false ,\"message\":\"Success\",\"data\":" + JSONresult + "}");

        }
        protected void ordercompleted()
        {
            string did = Request.Params["did"].ToString();
            string s = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select appstatus from tblorder  where Distributor_id='" + did + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                s = ds.Tables[0].Rows[0]["appstatus"].ToString();
            }

            if (s == "1")
            {
                Response.Write("{\"error\": true ,\"message\":\"Success\"}");
            }
            else
            {
                DataQueries.UpdateCommon("update tblorder set appstatus='1' where Distributor_id='" + did + "' and appstatus='0'");
                Response.Write("{\"error\": false ,\"message\":\"Success\"}");
            }
        }
        protected void orderidtomerchantforbill()
        {
            string mid = Request.Params["mid"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("SELECT id,updatedamount FROM [Nirmal].[Nirmal].[tblorder2] where final='1' and final1 is null and stat is null and mid='" + mid + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }

        //NFDB Services
        protected void Recentnews()
        {
            DataSet ds = DataQueries.SelectCommon("select id,news,image,Convert(varchar(16), GETDATE(),121) as date,Title from NFDBLATESTNEWS");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void Gallery()
        {
            DataSet ds = DataQueries.SelectCommon("select id,image from Gallery");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);

        }
        protected void sendimage()
        {
            string id = Request.Params["id"].ToString();
            DataSet ds = DataQueries.SelectCommon("select image from NFDBLATESTNEWS where id='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string image = ds.Tables[0].Rows[0]["image"].ToString().Trim();
                Response.Write("{\"error\":false,\"image\":" + image + "}");
            }

        }
        protected void tenderspdf()
        {
            DataSet ds = DataQueries.SelectCommon("select * from NFDBPDF");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void URL()
        {
            Uri myUri = new Uri("http://www.fb.com", UriKind.Absolute);
            Response.Write("{\"error\":false,\"url\":" + myUri + "}");
        }
        //election services
        protected void electionlogin()
        {
            string name = string.Empty;
            string electionid = Request.Params["electionid"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataSet ds = DataQueries.SelectCommon("select loginid from electionlogin where loginid='" + electionid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                name = ds.Tables[0].Rows[0]["loginid"].ToString();
            }
            if (name == electionid)
            {
                DataQueries.UpdateCommon("update electionlogin set status='1',datte='" + date + "' where loginid='" + electionid + "'");
            }
            else
            {
                DataQueries.InsertCommon("insert into electionlogin(loginid,datte,status) values('" + electionid + "','" + date + "','1')");
            }
            Response.Write("{\"error\": false\"}");

        }
        protected void Aplog()
        {
            string name = string.Empty;
            string electionid = Request.Params["electionid"].ToString();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            DataSet ds = DataQueries.SelectCommon("select loginid from Applogin where loginid='" + electionid + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                name = ds.Tables[0].Rows[0]["loginid"].ToString();
            }
            if (name == electionid)
            {
                DataQueries.UpdateCommon("update Applogin set status='1',datte='" + date + "' where loginid='" + electionid + "'");
            }
            else
            {
                DataQueries.InsertCommon("insert into Applogin(loginid,datte,status) values('" + electionid + "','" + date + "','1')");
            }
            Response.Write("{\"error\": false\"}");

        }

        protected void electionlogout()
        {
            string electionlogout = Request.Params["electionidlogout"].ToString();
            DataQueries.InsertCommon("update electionlogin set status='0' where loginid='" + electionlogout + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void Aplogout()
        {

            string electionlogout = Request.Params["electionidlogout"].ToString();
            DataQueries.InsertCommon("update Applogin set status='0' where loginid='" + electionlogout + "'");
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }
        protected void electiondisplay()
        {
            DataSet ds = DataQueries.SelectCommon("select loginid,datte from electionlogin where status='1'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void Apdisplay()
        {
            DataSet ds = DataQueries.SelectCommon("select loginid,datte from Applogin where status='1'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void countlogins()
        {
            DataSet ds = DataQueries.SelectCommon("select distinct count(loginid) as total from electionlogin where status='1'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string count = ds.Tables[0].Rows[0]["total"].ToString();
                Response.Write("{\"error\":false,\"total\":" + count + "}");
            }
            else
            {
                Response.Write("{\"total\":" + 0 + "}");
            }
        }
        protected void Apcount()
        {
            DataSet ds = DataQueries.SelectCommon("select count(loginid) as total from Applogin where status='1'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string count = ds.Tables[0].Rows[0]["total"].ToString();
                Response.Write("{\"error\":false,\"total\":" + count + "}");
            }
            else
            {
                Response.Write("{\"total\":" + 0 + "}");
            }
        }

        //testing

        protected void testcat()
        {
            DataSet ds = queries.SelectCommon("select id,categoryname,image from Category");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void testprod()
        {
            string id = Request.Params["id"].ToString();
            DataSet ds = queries.SelectCommon("select id,name,image from products where catid='" + id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void descrip()
        {
            string id = Request.Params["id"].ToString();
            DataSet ds = queries.SelectCommon("select id,image,price,description from productslist where pid='" + id + "'");
            DataTable table = ds.Tables[0];
            string JSONresult = DataTableToJsonWithJsonNet(table);
            Response.Write(JSONresult);
        }
        protected void totaldealerprofit()
        {
            string dealid = Request.Params["id"].ToString();
            DataSet ds = DataQueries.SelectCommon("select sum(cast(a.dealamount as float)*i.count)-(select isnull(sum(cast(damount as float)),0) from payamount where  role='dealer' and did='" + dealid + "') as subtot from tblorderitem i  inner join tblorder o on i.order_id = o.id inner join Addproduct a on a.id = i.product_id inner join DistributorLogin d on o.Distributor_id = d.id where a.did = '" + dealid + "' and status1='1'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string deprofit = ds.Tables[0].Rows[0]["subtot"].ToString().Trim();
                Response.Write("{\"error\": false ,\"message\":\"Success\",\"subtotal\":\"" + deprofit + "\"}");
            }
        }



        protected void distsubprofit()
        {
            string did = Request.Params["distributor_id"].ToString().Trim();
            DataSet ds = DataQueries.SelectCommon("select sum((cast(a.distamount as float)*i.count))-(select isnull(sum(cast(damount as float)),0) from payamount where  role='distributor' and did='" + did + "') as distsub from tblorderitem i  inner join tblorder o on i.order_id=o.id inner join Addproduct a on a.id=i.product_id inner join DistributorLogin d on o.Distributor_id=d.id where d.id ='" + did + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string profit = ds.Tables[0].Rows[0]["distsub"].ToString();
                Response.Write("{\"error\":false,\"distsubtotal\":\"" + profit + "\"}");
            }
        }
        protected void delivery()
        {
            string id = Request.Params["orderid"].ToString();
            DataQueries.UpdateCommon("update tblorder set delivery='1' where id='" + id + "'");
            string mobile = string.Empty;
            string cust = string.Empty;
            string merchantno = string.Empty;
            string dname = string.Empty;
            DataSet ds = DataQueries.SelectCommon("select c.mobile,c.name,d.mobile as mb,d.name as mn from customer c inner join tblorder o on c.id=o.Customer_id inner join DistributorLogin d on d.id=o.shopname where o.id='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                mobile = ds.Tables[0].Rows[0]["mobile"].ToString();
                cust = ds.Tables[0].Rows[0]["name"].ToString();
                merchantno = ds.Tables[0].Rows[0]["mb"].ToString();
                dname = ds.Tables[0].Rows[0]["mn"].ToString();

            }
            WebRequest requestt = null;
            HttpWebResponse responsee = null;
            String user = "BTRAK";
            String pass = "841090";
            string sms = "Dear " + cust + ",your order with order id " + id + " has been delivered.If you havent received then please contact  merchant name " + dname + " mobile  " + merchantno + " ";
            String urrl = "http://smsc.essms.com/smpp/?username=" + user + "&password=" + pass + "&from=NIRMAL&to=91" + mobile + "&text=" + sms + "";
            requestt = WebRequest.Create(urrl);
            responsee = (HttpWebResponse)requestt.GetResponse();
            Response.Write("{\"error\": false ,\"message\":\"Success\"}");
        }

        protected void itemslist()
        {
            string mid = Request.Params["mid"].ToString();
            string shp = string.Empty;
            DataSet dss = DataQueries.SelectCommon("select * from distributorlogin where id='" + mid + "'");
            if (dss.Tables[0].Rows.Count > 0)
            {
                shp = dss.Tables[0].Rows[0]["shopname"].ToString();
            }

            DataSet ds = DataQueries.SelectCommon("select image,productname,mrp,units,price from addproduct where shopname='" + shp + "' and status=1");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);


        }

        protected void deliverylist()
        {
            string mid = Request.Params["mid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select cast(id as varchar) as id from tblorder where shopname='" + mid + "' and delivery='1'");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);

        }
        protected void deliveryorderdetails()
        {
            string id = Request.Params["oid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select o.amount,d.name as dname,d.mobile,d.village,c.name as cname,c.mobile as cmobile from tblorder o inner join DistributorLogin d on d.id=o.Distributor_id inner join customer c on c.id=o.Customer_id where o.id='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string amount = ds.Tables[0].Rows[0]["amount"].ToString();
                string dname = ds.Tables[0].Rows[0]["dname"].ToString();
                string mob = ds.Tables[0].Rows[0]["mobile"].ToString();
                string vill = ds.Tables[0].Rows[0]["village"].ToString();
                string cname = ds.Tables[0].Rows[0]["cname"].ToString();
                string cmob = ds.Tables[0].Rows[0]["cmobile"].ToString();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"amount\":\"" + amount + "\",\"dname\":\"" + dname + "\",\"mobile\":\"" + mob + "\",\"village\":\"" + vill + "\",\"cname\":\"" + cname + "\",\"cmobile\":\"" + cmob + "\"}");
            }
        }

        protected void distributortodealer()
        {
            string deid = Request.Params["deid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct cast(d.id as varchar) as id,name +' village '+village from tblorder o inner join tblorderitem oi on o.id=oi.order_id inner join Addproduct a on a.id=oi.product_id INNER JOIN DISTRIBUTORLOGIN D On o.distributor_id=d.id where a.did='" + deid + "' and o.status='Approved'  ");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);
        }
        protected void dealerappprovedorderslist()
        {
            string deid = Request.Params["deid"].ToString();
            string did = Request.Params["did"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct cast(o.id as varchar) as id,o.amount,delivery from tblorder o inner join tblorderitem oi on o.id=oi.order_id inner join Addproduct a on a.id=oi.product_id INNER JOIN DISTRIBUTORLOGIN D On o.distributor_id=d.id where a.did='" + deid + "' and d.id='" + did + "'  and o.status='Approved' ");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);
        }
        protected void dealerorderitems()
        {
            string oid = Request.Params["oid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select a.productname,oi.count as units,cast(oi.count as float)*cast(a.mrp as float) as mrp from addproduct a inner join tblorderitem oi on a.id=oi.product_id  where oi.order_id='" + oid + "' ");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);
        }
        protected void displaydealerorders()
        {
            string deid = Request.Params["deid"].ToString();
            DataSet ds = DataQueries.SelectCommon("select distinct o.id,o.amount from tblorder o inner join tblorderitem oi on o.id=oi.order_id inner join Addproduct a on a.id=oi.product_id where a.did='" + deid + "' and o.delivery='1' ");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);
                                                  
        }
        protected void custdetailstodealer()
        {
            string id = Request.Params["id"].ToString();
            DataSet ds = DataQueries.SelectCommon("select c.name,c.mobile,c.address from customer c inner join tblorder o on o.Customer_id=c.id where o.id='" + id + "'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string name = ds.Tables[0].Rows[0]["name"].ToString();
                string address = ds.Tables[0].Rows[0]["address"].ToString();
                string mob = ds.Tables[0].Rows[0]["mobile"].ToString();

                Response.Write("{\"error\": false ,\"message\":\"Success\",\"name\":\"" + name + "\",\"mobile\":\"" + mob + "\",\"address\":\"" + address + "\"}");

            }

        }
        protected void testimage()
        {

            DataSet ds = DataQueries.SelectCommon("select baseimage from productslist where id=3");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);
        }

        protected void testt()
        {
            DataSet ds = DataQueries.SelectCommon("select * from testt");
            DataTable table = ds.Tables[0];
            string jsonr = DataTableToJsonWithJsonNet(table);
            Response.Write(jsonr);
        }

    }
}