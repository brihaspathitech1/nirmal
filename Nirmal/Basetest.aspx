﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Basetest.aspx.cs" Inherits="Nirmal.Basetest" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title> 
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="script" runat="server"></asp:ScriptManager>
        <div>

<hr />
<asp:GridView ID="gvImages" runat="server" AutoGenerateColumns="false">
    <Columns>
       <asp:BoundField DataField="pname" />
        <asp:BoundField DataField="image" />
     <asp:TemplateField HeaderText="Image"  ItemStyle-Height = "50" ItemStyle-Width="50" HeaderStyle-BackColor="#1a94d8" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"  >
        <ItemTemplate>
            <asp:Image ID="Image1" runat="server"  ImageUrl = '<%# Eval("image")%>'  ControlStyle-Height="100" ControlStyle-Width="100" />
        </ItemTemplate>
    </asp:TemplateField>
       <asp:TemplateField ItemStyle-Height = "50" ItemStyle-Width="50">
    <ItemTemplate>
        <img src="data:image/jpeg;base64,<%# Eval("baseimage") %>" height="50" width="50" />
    </ItemTemplate>
</asp:TemplateField>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="test" runat="server" OnClick="test_Click" Text="edit" CommandArgument='<%#Eval("id") %>'></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<div id="dialog" style="display: none">
</div>
        </div>
        <asp:ModalPopupExtender ID="modelpopup1" runat="server" CancelControlID="btncancel" TargetControlID="hf1" PopupControlID="panel1"></asp:ModalPopupExtender>
        <asp:Panel runat="server" ID="panel1" style="background:#E8E4C3;border:groove;border-color:cornflowerblue" Height="180px" Width="700px" CssClass="animated bounceInDown" >
           <div class="col-md-12 col-md-offset-11">
            <asp:LinkButton runat="server" ID="btncanceledit" CssClass="fa fa-times"></asp:LinkButton>
               </div>
            <div class="col-md-12">
               
              <asp:FileUpload ID="FileUpload1" runat="server" />
                   
                    <div class="col-md-3">
                        <div class="form-group">
                            <br />
                            <asp:Button runat="server" ID="btnupdate" Text="Update" CssClass="btn btn-info" OnClick="btnupdate_Click" />
                        </div>
                    </div>

                <div class="col-md-3" style="padding-top: 20px">
                                <div class="form-group">
                                    <asp:Button ID="btncancel" CssClass="btn btn-danger" runat="server" Text="Cancel" />
                                </div>
                            </div>
                </div>
              
            <asp:HiddenField runat="server" ID="hf1" />
            
        </asp:Panel>
        <asp:Label ID="ee" runat="server"></asp:Label>
    </form>
</body>
</html>
