﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Nirmal
{
    public partial class Basetest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                string constr = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(constr))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM productslist where  baseimage is null and catname in ('MENS FOOT WERAE ','FANCY and GENERAL STORE')", conn))
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        gvImages.DataSource = dt;
                        gvImages.DataBind();
                    }
                }
            }
        }

        protected void test_Click(object sender, EventArgs e)
        {
            LinkButton btn=sender as LinkButton;
            ee.Text = btn.CommandArgument;
            this.modelpopup1.Show();
        }

        protected void btnupdate_Click(object sender, EventArgs e)
        {
            byte[] bytes;
            BinaryReader br = new BinaryReader(FileUpload1.PostedFile.InputStream);

            bytes = br.ReadBytes(FileUpload1.PostedFile.ContentLength);
            string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            string constr = ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                string sql = "update productslist set baseimage=@test where id='" +ee.Text + "'";
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    //cmd.Parameters.AddWithValue("@Data", bytes);
                    cmd.Parameters.AddWithValue("@test", base64String);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }

            Response.Redirect("Basetest.aspx");
        }
    }
}