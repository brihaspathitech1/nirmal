﻿using System.Configuration;
using System.Data.SqlClient;

namespace Nirmal.DataConnection
{
    class dataconnection
    {
        public dataconnection()
        { }
        public static SqlConnection GetConnection()
        {
            SqlConnection connection;
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["nirmal"].ConnectionString);
           return connection;
        }
    }
}
