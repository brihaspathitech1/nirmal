﻿using System.Configuration;
using System.Data.SqlClient;

namespace Nirmal.DataAccessLayer
{
    public class connect
    {
        public connect()
        { }
        public static SqlConnection GetConnection()
        {
            SqlConnection connection;
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["vms"].ConnectionString);
            return connection;
        }
    }
}