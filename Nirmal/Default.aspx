﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Nirmal.Default1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <%--<link href='https://fonts.googleapis.com/css?family=Roboto:500,900,100,300,700,400' rel='stylesheet' type='text/css'>
    <link href="script/bootstrap-glyphicons.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>--%>
    <link href="fontawesome-free-5.12.0-web/css/all.min.css" rel="stylesheet" />
    <link href="script/bootstrapcss.min.css" rel="stylesheet" />
    <link href="script/bootstrap-glyphicons.css" rel="stylesheet" />
    <link href="script/fontss.css" rel="stylesheet" />
    <link href="script/css.css" rel="stylesheet" />
    <link href="fontawesome-free-5.12.0-web/css/fontawesome.min.css" rel="stylesheet" />

    <link href="script/Gsrcss.css" rel="stylesheet" />
    <script src="script/jquery.min.js"></script>
    <script src="script/bootstrap.min.js"></script>
    <script src="fontawesome-free-5.12.0-web/js/all.min.js"></script>
    <script src="fontawesome-free-5.12.0-web/js/fontawesome.min.js"></script>
    <script>
        var a = 0;
        $(window).scroll(function () {

            var oTop = $('#counter').offset().top - window.innerHeight;
            if (a == 0 && $(window).scrollTop() > oTop) {
                $('.counter-value').each(function () {
                    var $this = $(this),
                        countTo = $this.attr('data-count');
                    $({
                        countNum: $this.text()
                    }).animate({
                        countNum: countTo
                    },

                        {

                            duration: 7000,
                            easing: 'swing',
                            step: function () {
                                $this.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                $this.text(this.countNum);
                                //alert('finished');
                            }

                        });
                });
                a = 1;
            }

        });
    </script>
    <style>
        .navbar-brand {
            max-width: 50%;
            height: 67px;
            padding: 0px;
        }
        /* Circle */
        nav.circle ul li a {
            position: relative;
            overflow: hidden;
            z-index: 1;
        }

        .navbar-default .navbar-nav > li > a {
            color: #F3731F;
            font-weight: 700
        }


        nav.circle ul li a:after {
            display: block;
            position: absolute;
            margin: 0;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            /*content: '.';*/
            color: transparent;
            width: 1px;
            height: 1px;
            border-radius: 50%;
            background: #F3731F;
        }

        .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > li > a:hover {
            color: #eee;
            background-color: #dc691c;
        }

        nav.circle ul li a:hover:after {
            -webkit-animation: circle 1.5s ease-in forwards;
            color: #F3731F;
        }

        nav.fill ul li a {
            transition: all 2s;
        }

            nav.fill ul li a:after {
                text-align: left;
                content: '.';
                margin: 0;
                opacity: 0;
            }

            nav.fill ul li a:hover {
                color: #F3731F;
                z-index: 1;
            }

                nav.fill ul li a:hover:after {
                    z-index: -10;
                    animation: fill 1s forwards;
                    -webkit-animation: fill 1s forwards;
                    -moz-animation: fill 1s forwards;
                    opacity: 1;
                }

        /*@-webkit-keyframes fill {
            0% {
                width: 0%;
                height: 1px;
            }

            50% {
                width: 100%;
                height: 1px;
            }

            100% {
                width: 100%;
                height: 100%;
                background: #333;
            }
        }*/



        /*@-webkit-keyframes circle {
            0% {
                width: 1px;
                top: 0;
                left: 0;
                bottom: 0;
                right: 0;
                margin: auto;
                height: 1px;
                z-index: -1;
                background: #eee;
                border-radius: 100%;
            }

            100% {
                background: #aaa;
                height: 5000%;
                width: 5000%;
                z-index: -1;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                margin: auto;
                border-radius: 0;
            }
        }*/

        .navbar-default .navbar-nav > li > a {
            color: #F3731F;
            font-weight: 700;
        }

        /*.carousel-inner > .item > img {
            margin: 0 auto;
            padding-top: 50px;
        }*/


        body {
            font: 400 15px Lato, sans-serif;
            line-height: 1.8;
            color: #818181;
        }

        h2 {
            font-size: 24px;
            text-transform: uppercase;
            color: #303030;
            font-weight: 600;
            margin-bottom: 30px;
        }

        h4 {
            font-size: 19px;
            line-height: 1.375em;
            color: #303030;
            font-weight: 400;
            margin-bottom: 30px;
        }

        .jumbotron {
            background-color: #f4511e;
            color: #fff;
            padding: 100px 25px;
            font-family: Montserrat, sans-serif;
        }

        .container-fluid {
            padding: 60px 50px;
        }

        .bg-grey {
            background-color: #f6f6f6;
        }


        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }

            .thumbnail img {
                width: 100%;
                height: 100%;
                margin-bottom: 10px;
            }

        /*.carousel-control.right, .carousel-control.left {
            background-image: none;
            color: #f4511e;
        }

        .carousel-indicators li {
            border-color: #f4511e;
        }*/

        /*.carousel-indicators li.active {
                background-color: #f4511e;
            }*/

        .item h4 {
            font-size: 19px;
            line-height: 1.375em;
            font-weight: 400;
            font-style: italic;
            margin: 70px 0;
        }

        .item span {
            font-style: normal;
        }

        .panel {
            border: 1px solid #f4511e;
            border-radius: 0 !important;
            transition: box-shadow 0.5s;
        }

            .panel:hover {
                box-shadow: 5px 0px 40px rgba(0,0,0, .2);
            }

        .panel-footer .btn:hover {
            border: 1px solid #f4511e;
            background-color: #fff !important;
            color: #f4511e;
        }

        .panel-heading {
            color: #fff !important;
            background-color: #f4511e !important;
            padding: 25px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 0px;
            border-top-right-radius: 0px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .panel-footer {
            background-color: white !important;
        }

            .panel-footer h3 {
                font-size: 32px;
            }

            .panel-footer h4 {
                color: #aaa;
                font-size: 14px;
            }

            .panel-footer .btn {
                margin: 15px 0;
                background-color: #f4511e;
                color: #fff;
            }
        /*.navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }*/
        /*.navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }*/
        /*.navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }*/
        footer .glyphicon {
            font-size: 20px;
            margin-bottom: 20px;
            color: #f4511e;
        }

        body {
            font: 400 15px Lato, sans-serif;
            line-height: 1.8;
            color: #818181;
        }

        h2 {
            font-size: 24px;
            text-transform: uppercase;
            color: #303030;
            font-weight: 600;
            margin-bottom: 30px;
        }

        h4 {
            font-size: 19px;
            line-height: 1.375em;
            color: #303030;
            font-weight: 400;
            margin-bottom: 30px;
        }

        .jumbotron {
            background-color: #f4511e;
            color: #fff;
            padding: 100px 25px;
            font-family: Montserrat, sans-serif;
        }

        .container-fluid {
            padding: 60px 50px;
        }

        .bg-grey {
            background-color: #f6f6f6;
        }

        .logo-small {
            color: #f4511e;
            font-size: 50px;
        }

        .logo {
            color: #f4511e;
            font-size: 200px;
        }

        .thumbnail {
            padding: 0 0 15px 0;
            border: none;
            border-radius: 0;
        }

            .thumbnail img {
                width: 100%;
                height: 100%;
                margin-bottom: 10px;
            }

        /*.slideanim {
                visibility: hidden;
            }

            .slide {
                animation-name: slide;
                -webkit-animation-name: slide;
                animation-duration: 1s;
                -webkit-animation-duration: 1s;
                visibility: visible;
            }*/

        @keyframes slide {
            0% {
                opacity: 0;
                transform: translateY(70%);
            }

            100% {
                opacity: 1;
                transform: translateY(0%);
            }
        }

        @-webkit-keyframes slide {
            0% {
                opacity: 0;
                -webkit-transform: translateY(70%);
            }

            100% {
                opacity: 1;
                -webkit-transform: translateY(0%);
            }
        }

        @media screen and (max-width: 768px) {
            .col-sm-4 {
                text-align: center;
                margin: 25px 0;
            }

            .btn-lg {
                width: 100%;
                margin-bottom: 35px;
            }
        }

        @media screen and (max-width: 480px) {
            .logo {
                font-size: 150px;
            }
        }

        .glyphicon:hover {
            color: gold
        }

        .c-no {
            height: 150px;
        }

        .grey-bg {
            background: #ebebeb;
        }

        .counter-Txt {
            text-align: center;
            font-size: 20px;
            font-weight: bold;
            text-transform: uppercase;
            margin-top: 20px;
        }

            .counter-Txt span {
                display: block;
                font-size: 36px
            }

        @media(min-width:320px) and (max-width:767px) {
            .c-no {
                height: 100%;
            }

            .counter-Txt {
                margin-top: 35px;
            }

            .margin-bot-35 {
                margin-bottom: 35px;
            }
        }
        /*h4{
            color: #5D93C0;
        }*/
        .c-no {
            height: 242px;
        }

        .grey-bg {
            background: #ebebeb;
        }

        .counter-Txt {
            text-align: center;
            font-size: 20px;
            font-weight: bold;
            text-transform: uppercase;
            margin-top: 20px;
        }

            .counter-Txt span {
                display: block;
                font-size: 36px
            }

        @media(min-width:320px) and (max-width:767px) {
            .c-no {
                height: 100%;
            }

            .counter-Txt {
                margin-top: 35px;
            }

            .margin-bot-35 {
                margin-bottom: 35px;
            }
        }

        .size {
            margin: 5px;
            margin-left: 9px !important;
            color: black;
        }

        .contact {
            color: #EB741E;
            margin-right: 5px;
        }

        h4 {
            color: #055267;
        }
    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <form id="form1" runat="server">
    <div style="background-color: #F3731F; padding: 5px">
    </div>
    <section style="background: #F3731F; color: #FFFFFF">
        <nav class="navbar navbar-default navbar-fixed-top " style="border-top-color: #F3731F; border-top-width: 5px;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#myPage">
                        <img src="nlogo.png" id="logo" class="img-responsive" style="max-width: 100px; margin-top: -16px;" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right  ">
                        <li><a href="#myCarousel">HOME</a></li>
                        <li><a href="#about">ABOUT</a></li>
                      <%--  <li><a href="#portfolio">CAREERS</a></li>--%>
                        <li><a href="#services">PRODUCTS</a></li>
                        <li><a href="#contact">CONTACT</a></li>
                      <%--  <li><a href="#contact">EMPLOYEES</a></li>--%>
                        <li><a href="Reg.aspx">REGISTRATION</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </section>

    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="position: relative; margin-top: 55px;">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="img/gsr.jpg" alt="Los Angeles" style="width: 100%;">
            </div>
           <%-- <div class="item">
                <img src="img/gsr2.jpg" alt="Chicago" style="width: 100%;">
            </div>--%>

            <div class="item">
                <img src="img/gsr.jpg" alt="New york" style="width: 100%;">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Container (Services Section) -->
    <div id="about" class="container-fluid text-center">
        <h2 style="color: #43597B">SERVICES</h2>
        <h4>What we offer</h4>
        <br>
        <div class="row slideanim">
            <div class="col-sm-4">
                <span class="glyphicon  glyphicon-shopping-cart logo-small"></span>
                <h4>Quality Products</h4>
                <p>We Deliver Quality Products To Customers</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-thumbs-up logo-small"></span>
                <h4>Trust Worthy Employees</h4>
                <p>We have trust Worthy & Experianced Staff</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-dashboard logo-small"></span>
                <h4>Fast Delivery</h4>
                <p>We Deliver Products With in Customer asserted Time</p>
            </div>
        </div>
        <br>
    </div>
    <div class="grey-bg c-no container-fluid">
        <div class="container">
            <div class="row" id="counter">
                <div class="col-sm-3 counter-Txt"><span class="glyphicon glyphicon-user" style="color: #F3DE00"></span><span class="counter-value" data-count="10000">0</span> Happy Customers </div>
                <div class="col-sm-3 counter-Txt"><span class="glyphicon glyphicon-home" style="color: #0088D3"></span><span class="counter-value" data-count="350">0</span> SHOPS</div>
                <div class="col-sm-3 counter-Txt margin-bot-35"><span class="glyphicon glyphicon-heart-empty" style="color: red"></span><span class="counter-value" data-count="500">0</span> Employees</div>
                <div class="col-sm-3 counter-Txt margin-bot-35"><span class="glyphicon glyphicon-time" style="color: #00C5D3"></span><span class="counter-value" data-count="24">0</span> Hours Working </div>

            </div>
        </div>
    </div>



    <div id="services" class="container-fluid text-center ">
          <div class="row">
        <h2 class="text-center text-capitalize">Category List</h2>
        <div class="col-md-3">
            <div class="form-group">
                <asp:DataList ID="dlcat" runat="server" RepeatColumns="4">
                   
                    <ItemTemplate>
                            <asp:Image ID="image1" runat="server" Imageurl='<%# Eval("image") %>' height="50" Width="30"/><br />
                 
                        <asp:LinkButton ID="b" runat="server" Text='<%# Eval("categoryname") %>' CommandArgument='<%# Eval("Id") %>' CssClass="btn btn-link" /><br />
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
    </div>
    </div>

 
    <div id="portfolio" class="container-fluid text-center bg-grey">

        <h2>What our customers say</h2>
        <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <h4>"This company is the best. I am so happy with the result!"<br>
                        <span>Michael Roe, Vice President, Comment Box</span></h4>
                </div>
                <div class="item">
                    <h4>"One word... WOW!!"<br>
                        <span>John Doe, Salesman, Rep Inc</span></h4>
                </div>
                <div class="item">
                    <h4>"Could I... BE any more happy with this company?"<br>
                        <span>Chandler Bing, Actor, FriendsAlot</span></h4>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="container text-center">
        <h3 style="color: #43597B"><strong>TEAM</strong></h3>
        <p style="color: #FF3F3C"><em>" Meet OR Contact Us For</em></p>
        <p style="color: #FF3F3C">Any Information About Our Products and Dealings "</p>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <%--<img src="img/1.jpg" class="img-circle person" alt="Random Name" width="255" height="255">--%>
                <h4 class="text-center"><strong>GKYPrasad Babu</strong> </h4>
                <h5 class="text-center" style="color: #43597B">Owner and Admin Of GSR Nirmal Home Products</h5>
                <p>Contact No:9848505284</p>
            </div>
            <div class="col-sm-3">
               <%-- <img src="img/3.jpg" class="img-circle person" alt="Random Name" width="255" height="255">--%>
                <h4 class="text-center"><strong>Venkat</strong> </h4>
                <h5 style="color: #43597B">Poddutur Dealer</h5>
                <p>Contact No:9550762304</p>
            </div>
            <div class="col-sm-3">
              <%--  <img src="img/2.jpg" class="img-circle person" alt="Random Name" width="255" height="255">--%>
                <h4 class="text-center"><strong>Narasayya</strong> </h4>
                <h5 class="text-center" style="color: #43597B">Chagalamarri Dealer</h5>
                <p>Contact No:9618601597</p>
            </div>
            <div class="col-sm-3">
               <%-- <img src="img/bandmember.jpg" class="img-circle person" alt="Random Name" width="255" height="255">--%>
                <h4 class="text-center"><strong>Srinu</strong> </h4>
                <h5 class="text-center" style="color: #43597B">Nandyala Dealer</h5>
                <p>Contact No:9848071062</p>
            </div>
        </div>
    </div>
    <!-- Footer -->
    <footer class="page-footer font-small blue-grey lighten-5">

    <div style="background-color: #F2771F;">
            <div class="container">

                <!-- Grid row-->
                <div class="row py-4 d-flex align-items-center">

                    <!-- Grid column -->
                    <div class="col-md-6 col-lg-5 text-left text-md-left mb-4 mb-md-0">
                        <h5 class="mb-0" style="font-size: larger; color: white">Get connected with us !</h5>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                   <%-- <div class="col-md-6 col-lg-7 text-right text-md-right" style="float: left">

                        <!-- Facebook -->
                        <a class="fb-ic ">
                            <i class="fab fa-facebook-f fa-2x white-text mr-4 size"></i>
                        </a>
                        <!-- Twitter -->
                        <a class="tw-ic ">
                            <i class="fab fa-twitter fa-2x white-text mr-4 size"></i>
                        </a>
                        <!-- Google +-->
                        <a class="gplus-ic ">
                            <i class="fab fa-google-plus-g fa-2x white-text mr-4 size"></i>
                        </a>
                        <!--Linkedin -->
                        <a class="li-ic ">
                            <i class="fab fa-linkedin-in fa-2x white-text mr-4 size"></i>
                        </a>
                        <!--Instagram-->
                        <a class="ins-ic ">
                            <i class="fab fa-instagram fa-2x white-text size"></i>
                        </a>

                    </div>--%>
                    <!-- Grid column -->

                </div>
                <!-- Grid row-->

            </div>
        </div>

        <!-- Footer Links -->
        <div class="container-fluid text-center text-md-left mt-5 img-responsive" style="background-color: #ECEFF1; background-image: url(contact.jpg);">

            <!-- Grid row -->
            <div class="row mt-3" style="vertical-align: middle">

                <!-- Grid column -->
                <div class="col-md-3 col-lg-4 col-xl-3 mb-4">
                    <%--<img src="" />--%>
                    <!-- Content -->
                    <h4 class="text-uppercase font-weight-bold  size" style="">Location</h4>
                    <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p>
                        <i class="fas fa-home mr-3 fa-2x contact" style="color: #EB741E; margin-right: 5px"></i>Vivek Nagar
                        <br>
                        Kallur(md),Kurnool
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                    <!-- Links -->
                    <h4 class="text-uppercase font-weight-bold size">Contact Mobile</h4>
                    <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

                    <p>
                        <i class="fas fa-phone fa-2x mr-3 contact"></i>+91 9848505284<br />

                        +91 9550762304
                    </p>


                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                    <!-- Links -->
                    <h4 class="text-uppercase font-weight-bold  size">E-mail</h4>
                    <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p>
                        <i class="fas fa-envelope fa-2x mr-3 contact"></i>gkyprasadbabu@gmail.com
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                    <!-- Links -->
                    <h4 class="text-uppercase font-weight-bold size">Website</h4>
                    <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                    <p>
                        <i class="fas fa-globe-asia fa-2x mr-3 contact"></i><a href="http://www.gsrnirmal.com/">www.gsrnirmal.com</a>
                    </p>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright text-center text-black-50 py-3" style="background-color: #BDBFC1;">
            Copyright ©2019 All rights reserved &nbsp;&nbsp;  | &nbsp;&nbsp;
    <a href="https://Brihaspathi.com" style="line-height: 45px; color: #43597B"><strong>Brihaspathi</strong>   </a>
        </div>
        <!-- Copyright -->

    </footer>
    <!-- Footer -->



    <!-- Footer -->
    </form>
</body>

</html>