﻿using System.Collections.Generic;

namespace Nirmal
{
    public class Product
    {
        public int id { get; set; }
        public string productname { get; set; }
        public string categoryid { get; set; }
        public string units { get; set; }
        public string price { get; set; }
       // public string available { get; set; }
        public string image { get; set; }
        public int count { get; set; }
        //public string actualcost { get; set; }
        public List<ProductQuantity> quantity_list { get; set; }
        
    }
}