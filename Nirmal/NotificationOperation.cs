﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nirmal.DataAccessLayer;
using System.Data;
using System.Net;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Drawing;
using System.ComponentModel;

namespace Nirmal
{
    public class NotificationOperation
    {
        public void sendSingleNotification(string deviceId, NotificationItem noti)
        {

            var jsonData = new
            {
                to = deviceId,
                data = new
                {
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendTopicNotification(string topic, NotificationItem noti)
        {

            var jsonData = new
            {
                to = "/topics/" + topic,
                data = new
                {
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image

                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);

        }

        public void sendMultipleNotification(string[] registrationIds, NotificationItem noti)
        {

            var jsonData = new
            {
                registration_ids = registrationIds,
                data = new
                {
                    title = noti.title,
                    body = noti.body,
                    code = noti.code,
                    image = noti.image
                }
            };

            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(jsonData);

            PushNotification(json);
        }

        public void PushNotification(string json)
        {
            try
            {
                var applicationID = "AAAALiKefVk:APA91bHM3cS2TqBX9L6hwr3s7t1WzhsDWJTsLDxgD6qRsluFHkouY8RjXmRNIOwSvaePzL2D5RJj0qlHhSFXV9wmofM0Bxd_CVRAGHlueNcrRmmrCVEbCYmoKrBg9aWU1dLhBHEyGeg0";

                var senderId = "198149307737";

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                tRequest.Method = "post";

                tRequest.ContentType = "application/json";

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }

            catch (Exception ex)
            {
                string str = ex.Message;
            }

        }
    }
}